package lg.dplakosh.lgvoipdemo.backend.lg.dplakosh.lgvoipdemo.backend.signaling.testmock;

import lg.dplakosh.lgvoipdemo.backend.net.IVoiceService;

public class VoiceServiceMock implements IVoiceService {
    @Override
    public void actionMakeCall() {

    }

    @Override
    public void setCodec(String codec) {

    }

    @Override
    public int getLocalPort() {
        return 0;
    }

    @Override
    public int getLocalPortRecv() {
        return 0;
    }

    @Override
    public void endCall() {

    }

    @Override
    public void setStreams(String remoteIpAddress, int remotePort, int remotePortRecv) {

    }
}
