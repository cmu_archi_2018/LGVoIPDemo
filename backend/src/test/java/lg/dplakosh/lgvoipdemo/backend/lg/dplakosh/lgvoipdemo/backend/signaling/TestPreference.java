package lg.dplakosh.lgvoipdemo.backend.lg.dplakosh.lgvoipdemo.backend.signaling;

import android.content.SharedPreferences;
import android.support.annotation.Nullable;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class TestPreference implements SharedPreferences {

    private HashMap<String, String> mKeyValueMap;

    public void setMap(HashMap<String, String> map) {
        mKeyValueMap = map;
    }

    @Override
    public Map<String, ?> getAll() {
        return null;
    }

    @Nullable
    @Override
    public String getString(String key, @Nullable String defValue) {
        return mKeyValueMap.getOrDefault(key, defValue);
    }

    @Nullable
    @Override
    public Set<String> getStringSet(String key, @Nullable Set<String> defValues) {
        return null;
    }

    @Override
    public int getInt(String key, int defValue) {
        String value = mKeyValueMap.getOrDefault(key, String.valueOf(defValue));
        return Integer.parseInt(value);
    }

    @Override
    public long getLong(String key, long defValue) {
        String value = mKeyValueMap.getOrDefault(key, String.valueOf(defValue));
        return Long.parseLong(value);
    }

    @Override
    public float getFloat(String key, float defValue) {
        String value = mKeyValueMap.getOrDefault(key, String.valueOf(defValue));
        return Float.parseFloat(value);
    }

    @Override
    public boolean getBoolean(String key, boolean defValue) {
        String value = mKeyValueMap.getOrDefault(key, defValue ? "true" : "false");
        return value.equals("true");
    }

    @Override
    public boolean contains(String key) {
        return mKeyValueMap.containsKey(key);
    }

    @Override
    public Editor edit() {
        return null;
    }

    @Override
    public void registerOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener listener) {
    }

    @Override
    public void unregisterOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener listener) {
    }
}
