package lg.dplakosh.lgvoipdemo.backend.lg.dplakosh.lgvoipdemo.backend.signaling;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import org.junit.BeforeClass;
import org.junit.Test;

import java.util.HashMap;

import lg.dplakosh.lgvoipdemo.backend.signaling.ProtocolMessages;
import lg.dplakosh.lgvoipdemo.backend.utils.Helper;
import lg.dplakosh.lgvoipdemo.backend.utils.Logger;
import lg.dplakosh.lgvoipdemo.backend.utils.Preference;

import static org.junit.Assert.*;

public class ProtocolMessagesTest {

    private static TestPreference mSharedPref;

    @BeforeClass
    public static void before() {
        Logger.DONT_PRINT_LOG = true;
        mSharedPref = new TestPreference();
        Preference.init(mSharedPref);
    }

    @Test
    public void makeInviteMessage() {
        /*{
           “msg” : “invite”,
           “seqNo” : 1,
           “cid” : “guid”,
           “to”: 1004,
           “from”:1001,
           “protocol”:”…”
           “codec”:”…”
           "token":
        }*/
        // Precondition ----------------------------------------------------------------------------
        HashMap<String, String> pref = new HashMap<>();
        pref.put("accessToken", "accessToken1234");
        pref.put("my_number", "98765");
        pref.put("my_data_protocol", "rtp");
        pref.put("my_codec", "gsm");
        mSharedPref.setMap(pref);

        String peerNumber = "1234567";
        String cid = java.util.UUID.randomUUID().toString();

        // Testing ---------------------------------------------------------------------------------
        String jsonString = ProtocolMessages.makeInviteMessage(cid, peerNumber);

        // Check Result ----------------------------------------------------------------------------
        checkBasicCondition(jsonString);

        JsonParser parser = new JsonParser();
        JsonElement element = parser.parse(jsonString);

        assertEquals("invite", element.getAsJsonObject().get("msg").getAsString());
        assertTrue(element.getAsJsonObject().has("seqNo"));
        assertEquals(cid, element.getAsJsonObject().get("cid").getAsString());
        assertEquals(peerNumber, element.getAsJsonObject().get("to").getAsString());
        assertEquals(mSharedPref.getString("my_number", ""), element.getAsJsonObject().get("from").getAsString());
        assertEquals(mSharedPref.getString("my_data_protocol", ""), element.getAsJsonObject().get("protocol").getAsString());
        assertEquals(mSharedPref.getString("my_codec", ""), element.getAsJsonObject().get("codec").getAsString());
        assertEquals("accessToken1234", element.getAsJsonObject().get("token").getAsString());
    }

    @Test
    public void makeRingingMessage() {
        /*{
            msg : ringing,
            seqNo : 11,
            cid : guid
            token:
        }*/
        // Precondition ----------------------------------------------------------------------------
        HashMap<String, String> pref = new HashMap<>();
        pref.put("accessToken", "accessToken1234");
        mSharedPref.setMap(pref);

        String cid = java.util.UUID.randomUUID().toString();

        // Testing ---------------------------------------------------------------------------------
        String jsonString = ProtocolMessages.makeRingingMessage(cid);

        // Check Result ----------------------------------------------------------------------------
        checkBasicCondition(jsonString);

        JsonParser parser = new JsonParser();
        JsonElement element = parser.parse(jsonString);

        assertEquals("ringing", element.getAsJsonObject().get("msg").getAsString());
        assertTrue(element.getAsJsonObject().has("seqNo"));
        assertEquals(cid, element.getAsJsonObject().get("cid").getAsString());
        assertEquals("accessToken1234", element.getAsJsonObject().get("token").getAsString());
    }

    @Test
    public void makeAcceptMessage() {
        /*{
            msg : accept,
            seqNo : 12,
            cid : guid,
            rtp ip : 192.168.0.11
            rtp port : 50000
            rtp_port_recv
            token:
        }*/
        // Precondition ----------------------------------------------------------------------------
        HashMap<String, String> pref = new HashMap<>();
        pref.put("accessToken", "accessToken1234");
        pref.put("rtp_ip", "192.0.0.1");
        mSharedPref.setMap(pref);

        String cid = java.util.UUID.randomUUID().toString();
        int localPort = 5510;
        int localPortRecv = 5520;

        // Testing ---------------------------------------------------------------------------------
        String jsonString = ProtocolMessages.makeAcceptMessage(cid, localPort, localPortRecv);

        // Check Result ----------------------------------------------------------------------------
        checkBasicCondition(jsonString);

        JsonParser parser = new JsonParser();
        JsonElement element = parser.parse(jsonString);

        assertEquals("accept", element.getAsJsonObject().get("msg").getAsString());
        assertTrue(element.getAsJsonObject().has("seqNo"));
        assertEquals(cid, element.getAsJsonObject().get("cid").getAsString());
        assertEquals(Helper.getmStrMyIpAddress(), element.getAsJsonObject().get("rtp_ip").getAsString());
        assertEquals(localPort, element.getAsJsonObject().get("rtp_port").getAsInt());
        assertEquals(localPortRecv, element.getAsJsonObject().get("rtp_port_recv").getAsInt());
        assertEquals("accessToken1234", element.getAsJsonObject().get("token").getAsString());
    }

    @Test
    public void makeAckMessage() {
        /*{
            msg : ack,
            seqNo : 2,
            cid : guid
            rtp ip : 192.168.0.12
            rtp port : 50010
            rtp_port_recv
            token:
        }*/
        // Precondition ----------------------------------------------------------------------------
        HashMap<String, String> pref = new HashMap<>();
        pref.put("accessToken", "accessToken1234");
        mSharedPref.setMap(pref);

        String cid = java.util.UUID.randomUUID().toString();
        int localPort = 5510;
        int localPortRecv = 5520;

        // Testing ---------------------------------------------------------------------------------
        String jsonString = ProtocolMessages.makeAckMessage(cid, localPort, localPortRecv);

        // Check Result ----------------------------------------------------------------------------
        checkBasicCondition(jsonString);

        JsonParser parser = new JsonParser();
        JsonElement element = parser.parse(jsonString);

        assertEquals("ack", element.getAsJsonObject().get("msg").getAsString());
        assertTrue(element.getAsJsonObject().has("seqNo"));
        assertEquals(cid, element.getAsJsonObject().get("cid").getAsString());
        assertEquals(Helper.getmStrMyIpAddress(), element.getAsJsonObject().get("rtp_ip").getAsString());
        assertEquals(localPort, element.getAsJsonObject().get("rtp_port").getAsInt());
        assertEquals(localPortRecv, element.getAsJsonObject().get("rtp_port_recv").getAsInt());
        assertEquals("accessToken1234", element.getAsJsonObject().get("token").getAsString());
    }

    @Test
    public void makeRejectMessage() {
        /*{
            msg : reject,
            seqNo : 13,
            cid : guid,
            reason : 100
            token:
        }*/
        // Precondition ----------------------------------------------------------------------------
        HashMap<String, String> pref = new HashMap<>();
        pref.put("accessToken", "accessToken1234");
        mSharedPref.setMap(pref);

        String cid = java.util.UUID.randomUUID().toString();
        int reason = 123;

        // Testing ---------------------------------------------------------------------------------
        String jsonString = ProtocolMessages.makeRejectMessage(cid, reason);

        // Check Result ----------------------------------------------------------------------------
        checkBasicCondition(jsonString);

        JsonParser parser = new JsonParser();
        JsonElement element = parser.parse(jsonString);

        assertEquals("reject", element.getAsJsonObject().get("msg").getAsString());
        assertTrue(element.getAsJsonObject().has("seqNo"));
        assertEquals(cid, element.getAsJsonObject().get("cid").getAsString());
        assertEquals(reason, element.getAsJsonObject().get("reason").getAsInt());
        assertEquals("accessToken1234", element.getAsJsonObject().get("token").getAsString());
    }

    @Test
    public void makeByeMessage() {
        /*{
            msg : bye,
            seqNo : 6,
            cid : guid,
            reason :
            token:
        }*/
        // Precondition ----------------------------------------------------------------------------
        HashMap<String, String> pref = new HashMap<>();
        pref.put("accessToken", "accessToken1234");
        mSharedPref.setMap(pref);

        String cid = java.util.UUID.randomUUID().toString();
        int reason = 123;

        // Testing ---------------------------------------------------------------------------------
        String jsonString = ProtocolMessages.makeByeMessage(cid, reason);

        // Check Result ----------------------------------------------------------------------------
        checkBasicCondition(jsonString);

        JsonParser parser = new JsonParser();
        JsonElement element = parser.parse(jsonString);

        assertEquals("bye", element.getAsJsonObject().get("msg").getAsString());
        assertTrue(element.getAsJsonObject().has("seqNo"));
        assertEquals(cid, element.getAsJsonObject().get("cid").getAsString());
        assertEquals(reason, element.getAsJsonObject().get("reason").getAsInt());
        assertEquals("accessToken1234", element.getAsJsonObject().get("token").getAsString());
    }

    @Test
    public void makeCallKeepAliveMessage() {
        /*{
            msg : keepalive,
            status : calling,
            from : 111-222-3333,
            to : 111-222-4444,
            cid :
            token :
        }*/
        // Precondition ----------------------------------------------------------------------------
        HashMap<String, String> pref = new HashMap<>();
        pref.put("accessToken", "accessToken1234");
        pref.put("my_number", "98765");
        mSharedPref.setMap(pref);

        String peerNumber = "1234567";
        String cid = java.util.UUID.randomUUID().toString();

        // Testing ---------------------------------------------------------------------------------
        String jsonString = ProtocolMessages.makeCallKeepAliveMessage(peerNumber, cid);

        // Check Result ----------------------------------------------------------------------------
        checkBasicCondition(jsonString);

        JsonParser parser = new JsonParser();
        JsonElement element = parser.parse(jsonString);

        assertEquals("keepalive", element.getAsJsonObject().get("msg").getAsString());
        assertEquals("calling", element.getAsJsonObject().get("status").getAsString());
        assertEquals(mSharedPref.getString("my_number", ""), element.getAsJsonObject().get("from").getAsString());
        assertEquals(peerNumber, element.getAsJsonObject().get("to").getAsString());
        assertEquals(cid, element.getAsJsonObject().get("cid").getAsString());
        assertEquals("accessToken1234", element.getAsJsonObject().get("token").getAsString());
    }

    @Test
    public void makeStandbyKeepAliveMessage() {
        /*{
              msg : keepalive,
              status : standby,
              phoneNo : 111-222-3333,
              token :
        }*/
        // Precondition ----------------------------------------------------------------------------
        HashMap<String, String> pref = new HashMap<>();
        pref.put("accessToken", "accessToken1234");
        pref.put("my_number", "98765");
        mSharedPref.setMap(pref);

        String cid = java.util.UUID.randomUUID().toString();

        // Testing ---------------------------------------------------------------------------------
        String jsonString = ProtocolMessages.makeStandbyKeepAliveMessage();

        // Check Result ----------------------------------------------------------------------------
        checkBasicCondition(jsonString);

        JsonParser parser = new JsonParser();
        JsonElement element = parser.parse(jsonString);

        assertEquals("keepalive", element.getAsJsonObject().get("msg").getAsString());
        assertEquals("standby", element.getAsJsonObject().get("status").getAsString());
        assertEquals(mSharedPref.getString("my_number", ""), element.getAsJsonObject().get("phoneNo").getAsString());
        assertEquals("accessToken1234", element.getAsJsonObject().get("token").getAsString());
    }

    @Test
    public void makeSendTextMessage() {
        /*{
            msg : textmsg,
            seqNo : 1
            from : 111-222-3333,
            to : 111-222-4444,
            message : “bla bla bla”
            token : “”,
        }*/
        // Precondition ----------------------------------------------------------------------------
        HashMap<String, String> pref = new HashMap<>();
        pref.put("accessToken", "accessToken1234");
        pref.put("my_number", "98765");
        mSharedPref.setMap(pref);

        String peerNumber = "1234567";
        String cid = java.util.UUID.randomUUID().toString();
        String message = "Hello, 7 hours";

        // Testing ---------------------------------------------------------------------------------
        String jsonString = ProtocolMessages.makeSendTextMessage(peerNumber, message);

        // Check Result ----------------------------------------------------------------------------
        checkBasicCondition(jsonString);

        JsonParser parser = new JsonParser();
        JsonElement element = parser.parse(jsonString);

        assertEquals("textmsg", element.getAsJsonObject().get("msg").getAsString());
        assertTrue(element.getAsJsonObject().has("seqNo"));
        assertEquals(mSharedPref.getString("my_number", ""), element.getAsJsonObject().get("from").getAsString());
        assertEquals(peerNumber, element.getAsJsonObject().get("to").getAsString());
        assertEquals(message, element.getAsJsonObject().get("message").getAsString());
        assertEquals("accessToken1234", element.getAsJsonObject().get("token").getAsString());
    }

    private void checkBasicCondition(String jsonString) {
        assertNotNull(jsonString);                                            // should not be null
        assertTrue(jsonString.length() > 0);                       // should not be empty
        int indexOfLastLetter = jsonString.lastIndexOf('\n');           // last letter should be '\n'
        assertEquals(jsonString.length() - 1, indexOfLastLetter);
    }
}
