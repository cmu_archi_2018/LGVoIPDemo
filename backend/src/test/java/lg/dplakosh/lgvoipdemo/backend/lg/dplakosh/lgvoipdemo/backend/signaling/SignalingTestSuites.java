package lg.dplakosh.lgvoipdemo.backend.lg.dplakosh.lgvoipdemo.backend.signaling;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import lg.dplakosh.lgvoipdemo.backend.signaling.CallMessageHandler;

@RunWith(value=Suite.class)
@Suite.SuiteClasses(value={CallMessageHandlerTest.class, ProtocolMessagesTest.class})
public class SignalingTestSuites {
}


//TestCase를 확장하지 않았기 때문에 'assert*' 메소드를 사용하기 위해서는 'import static org.junit.Assert.*;' static imports 해준다.

/*
@RunWith(value=Suite.class)
@SuiteClasses(value={CalculatorTest.class, AnotherTest.class})
public class AllTests{
}
*/

/*
@Test(expected = Exception.class)
@Test의 expected에 클래스 메소드안의 try, catch에서 발생한 예외를 정의해준다.
정의를 해준 예외는 메소드안에서 try, catch해 줄 필요가 없다.
*/

// @Ignore("테스트 실행에서 제외되는 이유")

/*
@RunWith(value=Parameterized.class)
public class FactorialTest{

    private long expected;
    private int value;

    @Parameters
    public static Collection data(){
        return Arrays.asList(new Object[][]{
                {1, 0}, // expected, value
                {1, 1},
                {2, 2},
                {24, 4},
                {5040, 7}
        });
    }

    public FactorialTest(long expected, int value){
        this.expected = expected;
        this.value = value;
    }

    @Test
    public void factorial(){
        Calculator calculator = new Calculator();
        assertEquals(expected, calculator.factorial(value));
    }
}

실행결과
        factorial#0:  assertEquals( 1, calculator.factorial( 0 ) );
        factorial#1:  assertEquals( 1, calculator.factorial( 1 ) );
        factorial#2:  assertEquals( 2, calculator.factorial( 2 ) );
        factorial#3:  assertEquals( 24, calculator.factorial( 4 ) );
        factorial#4:  assertEquals( 5040, calculator.factorial( 7 ) );
*/