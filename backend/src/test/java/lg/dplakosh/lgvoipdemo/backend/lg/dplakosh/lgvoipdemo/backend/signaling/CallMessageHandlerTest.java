package lg.dplakosh.lgvoipdemo.backend.lg.dplakosh.lgvoipdemo.backend.signaling;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.HashMap;

import lg.dplakosh.lgvoipdemo.backend.lg.dplakosh.lgvoipdemo.backend.signaling.testmock.PacketIoMock;
import lg.dplakosh.lgvoipdemo.backend.lg.dplakosh.lgvoipdemo.backend.signaling.testmock.VoiceServiceMock;
import lg.dplakosh.lgvoipdemo.backend.net.IVoiceService;
import lg.dplakosh.lgvoipdemo.backend.signaling.CallMessageHandler;
import lg.dplakosh.lgvoipdemo.backend.signaling.CallThreadEventListener;
import lg.dplakosh.lgvoipdemo.backend.utils.Logger;
import lg.dplakosh.lgvoipdemo.backend.utils.Preference;

import static org.junit.Assert.*;

public class CallMessageHandlerTest {

    private static CallMessageHandler mCallMessageHandler;
    private static TestPreference mSharedPref;
    private static IVoiceService mVoiceService = new VoiceServiceMock();
    private static PacketIoMock mPacketIO = new PacketIoMock();

    @BeforeClass
    public static void before() {
        Logger.DONT_PRINT_LOG = true;
        mSharedPref = new TestPreference();
        Preference.init(mSharedPref);
        mCallMessageHandler = new CallMessageHandler(mVoiceService, mPacketIO);
        mCallMessageHandler.setCallThreadEventListner(mCallThreadEventListener, 1);
        mCallMessageHandler.start();
    }

    @AfterClass
    public static void after() {
        // Access Token
        // Number
        //
        mCallMessageHandler.stopCallMessageHandler();
        mCallMessageHandler = null;

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Logger.DONT_PRINT_LOG = false;
    }

    @Test
    public void sendInvite() {
        // Precondition ----------------------------------------------------------------------------
        HashMap<String, String> pref = new HashMap<>();
        pref.put("my_codec", "gsm");
        mSharedPref.setMap(pref);

        // Testing ---------------------------------------------------------------------------------
        mCallMessageHandler.sendInvite("111-222");

        // Check Result ----------------------------------------------------------------------------
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Check
        System.out.println("sendInvite - " + mPacketIO.mResultOfSendingMessage);
        assertNotNull(mPacketIO.mResultOfSendingMessage);

        mPacketIO.clearResult();
    }

    @Test
    public void sendRinging() {
        // Precondition ----------------------------------------------------------------------------
        HashMap<String, String> pref = new HashMap<>();
        pref.put("my_codec", "gsm");
        mSharedPref.setMap(pref);

        // Testing ---------------------------------------------------------------------------------
        mCallMessageHandler.sendRinging();

        // Check Result ----------------------------------------------------------------------------
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // Check
        System.out.println("sendRinging - " + mPacketIO.mResultOfSendingMessage);
        assertNotNull(mPacketIO.mResultOfSendingMessage);

        mPacketIO.clearResult();
    }

    @Test
    public void sendAccept() {
        fail("not implemented");
    }

    @Test
    public void sendReject() {
        fail("not implemented");
    }

    @Test
    public void sendBye() {
        fail("not implemented");
    }

    @Test
    public void onNewTextMessage() {
        fail("not implemented");
    }

    @Test
    public void onAccept() {
        fail("not implemented");
    }

    @Test
    public void onReject() {
        fail("not implemented");
    }

    @Test
    public void onBye() {
        fail("not implemented");
    }

    private static CallThreadEventListener mCallThreadEventListener = new CallThreadEventListener() {

        @Override
        public String getCallId() {
            return null;
        }

        @Override
        public void updateCallId(String callId) {

        }

        @Override
        public void progressing() {

        }

        @Override
        public void connected() {

        }

        @Override
        public void rejected(int reason) {

        }

        @Override
        public void disconnected(int reason) {

        }
    };
}
