package lg.dplakosh.lgvoipdemo.backend.lg.dplakosh.lgvoipdemo.backend.signaling.testmock;

import java.net.Socket;

import lg.dplakosh.lgvoipdemo.backend.signaling.network.IPacketIO;
import lg.dplakosh.lgvoipdemo.backend.utils.MyTextUtil;

public class PacketIoMock implements IPacketIO {

    public boolean Prop_CreateSocket_OK = true;
    public boolean Prop_SetSocket_OK = true;
    public boolean Prop_UpdateBufferedIO_OK = true;
    public boolean Prop_SocketIsClosed = false;
    public String Prop_Message = "";

    public String mResultOfSendingMessage = null;

    private Object receiveWait = new Object();

    @Override
    public boolean createSocket() {
        return Prop_CreateSocket_OK;
    }

    @Override
    public void closeSocket() {
        synchronized (receiveWait) {
            receiveWait.notify();
        }
    }

    @Override
    public boolean updateBufferedIOStream() {
        return Prop_UpdateBufferedIO_OK;
    }

    @Override
    public boolean sendMessage(String msg) {
        System.out.println("sendMessage - " + msg);
        mResultOfSendingMessage = msg;
        return Prop_SocketIsClosed;
    }

    @Override
    public boolean isSocketClosed() {
        return Prop_SocketIsClosed;
    }

    @Override
    public boolean checkNetworkAvailable(String msgName) {
        return !Prop_SocketIsClosed;
    }

    @Override
    public boolean setSocket(Socket socket) {
        return Prop_SetSocket_OK;
    }

    @Override
    public String readMessage() {
        if (MyTextUtil.isEmpty(Prop_Message)) {
            synchronized (receiveWait) {
                try {
                    receiveWait.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        return Prop_Message;
    }

    public void clearResult() {
        mResultOfSendingMessage = null;
    }
}
