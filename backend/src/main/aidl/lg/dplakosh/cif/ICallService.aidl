package lg.dplakosh.cif;

import lg.dplakosh.cif.ICallServiceListener;
import lg.dplakosh.cif.ICallSession;

interface ICallService
{
    void setLoginResult(boolean result, String myNumber, String accessToken);

    /* Call operation */
    ICallSession openSession();
    ICallSession attachSession(int sessionKey);
    void closeSession(ICallSession session);

    /* Text message operation */
    void sendTextMessage(String phoneNumber, String msg);
    String retrieveMessage(int msgKey);

    /* Callback */
    void setListener(ICallServiceListener listener);
}
