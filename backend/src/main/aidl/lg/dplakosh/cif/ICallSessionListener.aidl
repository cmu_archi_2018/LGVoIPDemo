package lg.dplakosh.cif;

interface ICallSessionListener
{
	void progressing();
	void started();
	void startFailed(int reason, int code);
	void terminated(int reason);
}

