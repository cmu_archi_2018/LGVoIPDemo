package lg.dplakosh.lgvoipdemo.backend.net.voiceservice.transport;

public interface IVoiceMixer {
    /**
     * Returns the {@link IVoiceStream}s in this group.
     */
    IVoiceStream[] getStreams();

    int getMode();

    void setMode(int mode);

    void add(IVoiceStream stream);

    void remove(IVoiceStream stream);

    void clear();

}
