package lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.echo;

import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.codec.IVoicePacket;
import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.codec.PacketDecorator;

public abstract class EchoDecorator extends PacketDecorator{
	protected PacketDecorator decorable;
	public EchoDecorator(PacketDecorator decorable) {
		this.decorable = decorable;
	}

	public abstract String getDescription();
}
