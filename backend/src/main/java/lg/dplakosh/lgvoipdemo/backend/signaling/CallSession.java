package lg.dplakosh.lgvoipdemo.backend.signaling;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.security.GeneralSecurityException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

import lg.dplakosh.cif.ICallSession;
import lg.dplakosh.cif.ICallSessionListener;
import lg.dplakosh.lgvoipdemo.backend.R;
import lg.dplakosh.lgvoipdemo.backend.utils.Logger;
import lg.dplakosh.lgvoipdemo.backend.utils.Worker;

public class CallSession extends ICallSession.Stub implements CallThreadEventListener {

    private static final String TAG = "voipbend|CallSession";

    private final Context mContext;
    private final int mSessionKey;
    private ICallSessionListener mListener;
    private final Worker mWorkerThread = new Worker();
    private String mCallId;
    private final CallMessageHandler mCallMessageHandler;


    public CallSession(Context context, int sessionKey, CallMessageHandler callMessageHandler) {
        mContext = context;
        mSessionKey = sessionKey;
        mCallMessageHandler = callMessageHandler;
        mCallMessageHandler.setCallThreadEventListner(this, sessionKey);

        mCallId = java.util.UUID.randomUUID().toString();

        Log.d(TAG, "CallSession ctor");
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        mWorkerThread.quit();
    }

    private final Handler mHandler = new Handler(mWorkerThread.getLooper()) {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 200:
                    mCallMessageHandler.sendRinging();
                    break;
            }
        }
    };

    @Override
    public void start(String phoneNumber) throws RemoteException {
        Log.d(TAG, "start");

        mCallMessageHandler.sendInvite(phoneNumber);
    }

    @Override
    public void accept() throws RemoteException {
        Log.d(TAG, "accept");

        mCallMessageHandler.sendAccept();
    }

    @Override
    public void reject(int reason) throws RemoteException {
        Log.d(TAG, "reject - reason : " + reason);

        mCallMessageHandler.sendReject(reason);
    }

    @Override
    public void terminate(int reason) throws RemoteException {
        Log.d(TAG, "terminate - reason : " + reason);

        mCallMessageHandler.sendBye(reason);
    }

    public void sendRinging() {
        mHandler.sendEmptyMessageDelayed(200, 50);
    }

    @Override
    public void setListener(ICallSessionListener listener) throws RemoteException {
        mListener = listener;
    }

    @Override
    public int getProperty(int item) throws RemoteException {
        return 0;
    }

    @Override
    public String getCallId() {
        return mCallId;
    }

    @Override
    public void updateCallId(String callId) {
        mCallId = callId;
    }

    @Override
    public void progressing() {
        try {
            Log.d(TAG, "progressing");
            mListener.progressing();
        } catch (RemoteException e) {
            e.printStackTrace();
            Logger.e(TAG,e);
        }
    }

    @Override
    public void connected() {
        try {
            Log.d(TAG, "connected");
            CallNode node = CallManager.get().getCallNode(mSessionKey);
            if (node != null)
                node.setState(CallState.ACTIVE);

            mListener.started();
        } catch (RemoteException e) {
            e.printStackTrace();
            Logger.e(TAG,e);
        }
    }

    @Override
    public void rejected(int reason) {
        try {
            Log.d(TAG, "rejected");
            CallNode node = CallManager.get().getCallNode(mSessionKey);
            if (node != null) {
                node.setState(CallState.DISCONNECTED);
                CallManager.get().removeCallNode(mSessionKey);
            }

            mListener.startFailed(reason, 0);
        } catch (RemoteException e) {
            e.printStackTrace();
            Logger.e(TAG,e);
        }
    }

    @Override
    public void disconnected(int reason) {
        try {
            Log.d(TAG, "disconnected");
            CallNode node = CallManager.get().getCallNode(mSessionKey);
            if (node != null) {
                node.setState(CallState.DISCONNECTED);
                CallManager.get().removeCallNode(mSessionKey);
            }

            mListener.terminated(reason);

        } catch (RemoteException e) {
            e.printStackTrace();
            Logger.e(TAG,e);
        }
    }

    protected Socket getConnection(String ip, int port) throws IOException, KeyStoreException, CertificateException, NoSuchAlgorithmException, KeyManagementException {
        try {
            KeyStore trustStore = KeyStore.getInstance("BKS");
            InputStream trustStoreStream = mContext.getResources().openRawResource(R.raw.server);
            trustStore.load(trustStoreStream, "keypass".toCharArray());

            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
            trustManagerFactory.init(trustStore);

            SSLContext sslContext = SSLContext.getInstance("TLS");
            sslContext.init(null, trustManagerFactory.getTrustManagers(), null);
            SSLSocketFactory factory = sslContext.getSocketFactory();
            SSLSocket socket = (SSLSocket) factory.createSocket(ip, port);
            socket.setEnabledCipherSuites(getCipherSuitesWhiteList(socket.getEnabledCipherSuites()));
            return socket;
        } catch (GeneralSecurityException e) {
            Log.e(this.getClass().toString(), "Exception while creating context: ", e);
            throw new IOException("Could not connect to SSL Server", e);
        }
    }

    private static String[] getCipherSuitesWhiteList(String[] cipherSuites) {
        List<String> whiteList = new ArrayList<>();
        List<String> rejected = new ArrayList<>();
        for (String suite : cipherSuites) {
            String s = suite.toLowerCase();
            if (s.contains("anon") || //reject no anonymous
                    s.contains("export") || //reject no export
                    s.contains("null") || //reject no encryption
                    s.contains("md5") || //reject MD5 (weaknesses)
                    s.contains("_des") || //reject DES (key size too small)
                    s.contains("krb5") || //reject Kerberos: unlikely to be used
                    s.contains("ssl") || //reject ssl (only tls)
                    s.contains("empty")) {    //not sure what this one is
                rejected.add(suite);
            } else {
                whiteList.add(suite);
            }
        }
        return whiteList.toArray(new String[whiteList.size()]);
    }
}
