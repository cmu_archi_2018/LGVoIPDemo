package lg.dplakosh.lgvoipdemo.backend;

import android.app.Application;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import lg.dplakosh.lgvoipdemo.backend.signaling.CallService;
import lg.dplakosh.lgvoipdemo.backend.signaling.ServerEventListenThread;
import lg.dplakosh.lgvoipdemo.backend.utils.AppContext;
import lg.dplakosh.lgvoipdemo.backend.utils.Preference;

public class BackEndApp extends Application {

    private static final String TAG = "voipbend|BackEndApp";

    private ServerEventListenThread recvThread;

    @Override
    public void onCreate() {
        super.onCreate();

        new AppContext(getApplicationContext());

        SharedPreferences pref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        Preference.init(pref);

        startService(new Intent(this, CallService.class));

        Log.d(TAG, "OnCreate");
    }

    @Override
    public void onTerminate() {
        super.onTerminate();

        Log.d(TAG, "OnDestroy");
    }
}
