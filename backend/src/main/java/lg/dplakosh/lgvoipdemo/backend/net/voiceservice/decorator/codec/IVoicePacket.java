package lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.codec;

public interface IVoicePacket {
    byte[] encode(byte[] payload, int[] samples);
    byte[] decode(int[] samples, int count, byte[] payload, int length);
}
