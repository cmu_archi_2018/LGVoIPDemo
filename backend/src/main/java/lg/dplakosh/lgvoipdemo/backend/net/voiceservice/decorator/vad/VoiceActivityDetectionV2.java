package lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.vad;

import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.codec.IVoicePacket;
import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.codec.PacketDecorator;

public class VoiceActivityDetectionV2 extends VADDecorator implements IVoicePacket {

    private final String TAG = VoiceActivityDetectionV2.class.getSimpleName();

    public VoiceActivityDetectionV2(PacketDecorator decorable) {
        super(decorable);
    }

    public String getDescription() {
        return decorable.getDescription() + ", " + VoiceActivityDetectionV2.class.getSimpleName();
    }

    public double cost() {
        // write code here...
        return decorable.cost() + 0.20;

    }

    @Override
    public byte[] encode(byte[] payload, int[] samples) {
        return new byte[0];
    }

    @Override
    public byte[] decode(int[] samples, int count, byte[] payload, int length) {
        return new byte[0];
    }
}
