package lg.dplakosh.lgvoipdemo.backend.utils;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Base64;
import android.util.Log;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

import static android.content.Context.WIFI_SERVICE;

public class Helper {

    final static String TAG = Helper.class.getSimpleName();
    private static String mStrMyIpAddress = "127.0.0.1";

    public static void setLocalIpAddress(String _ipaddress) {
        Helper.mStrMyIpAddress = _ipaddress;
    }

    public static String getmStrMyIpAddress() {
        return Helper.mStrMyIpAddress;
    }

    public static InetAddress getLocalIpAsInetAddress() {
        InetAddress inetAddress = null;
        try {
            inetAddress = InetAddress.getByName(Helper.mStrMyIpAddress);
        } catch (UnknownHostException e) {
            Logger.e(TAG, e);
        } catch (Exception e) {
            Logger.e(TAG, e);
        }
        return inetAddress;
    }

    public static InetAddress getTargetInetaddress(String target) {
        InetAddress targetInetAddress = null;
        try {

            Log.i("remoteaddress", "remote address:" + target);
            targetInetAddress = InetAddress.getByName(target.trim());
        } catch (UnknownHostException e) {
            Logger.e(TAG, e);
        } catch (Exception e) {
            Logger.e(TAG, e);
        }
        return targetInetAddress;
    }

    /**
     * @param text
     * @return
     * @throws NoSuchAlgorithmException
     */
    public static String SHA256(String text) throws NoSuchAlgorithmException {

        MessageDigest md = MessageDigest.getInstance("SHA-256");

        md.update(text.getBytes());
        byte[] digest = md.digest();

        return Base64.encodeToString(digest, Base64.DEFAULT);
    }

    public static String getLocalIP(Context context) {
        int LocalIpAddressBin = 0;
        WifiManager wifiManager = (WifiManager) context.getApplicationContext().getSystemService(WIFI_SERVICE);
        if (wifiManager != null) {
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            LocalIpAddressBin = wifiInfo.getIpAddress();
            return String.format(Locale.US, "%d.%d.%d.%d", (LocalIpAddressBin & 0xff), (LocalIpAddressBin >> 8 & 0xff), (LocalIpAddressBin >> 16 & 0xff), (LocalIpAddressBin >> 24 & 0xff));
        }
        return "NO ADDRESS";
    }
}
