package lg.dplakosh.lgvoipdemo.backend.signaling.messages;

public abstract class CallMessage {

    public static final String MSG_SEND_INVITE = "sendInvite";
    public static final String MSG_SEND_RINGING = "sendRinging";
    public static final String MSG_SEND_ACCEPT = "sendAccept";
    public static final String MSG_SEND_ACK = "sendAck";
    public static final String MSG_SEND_REJECT = "sendReject";
    public static final String MSG_SEND_BYE = "sendBye";
    public static final String MSG_SEND_TEXT = "sendText";
    public static final String MSG_SEND_KEEPALIVE = "sendKeepAlive";
    public static final String MSG_EXIT_LOOP = "exitLoop";

    public static final String MSG_ON_INVITE = "onInvite";
    public static final String MSG_ON_TRYING = "onTrying";
    public static final String MSG_ON_RINGING = "onRinging";
    public static final String MSG_ON_ACCEPT = "onAccept";
    public static final String MSG_ON_REJECT = "onReject";
    public static final String MSG_ON_ACK = "onAck";
    public static final String MSG_ON_BYE = "onBye";
    public static final String MSG_ON_RESULT_SEND_TEXT = "onResultSendText";
    public static final String MSG_ON_NEW_TEXT = "onNewText";
    public static final String MSG_ON_RESULT_KEEPALIVE = "onResultKeepAlive";

    private String mMsgName;

    public CallMessage(String msgName) {
        mMsgName = msgName;
    }

    public abstract boolean isRequest();

    public boolean process() {
        return false;
    }

    public String getMsgName() {
        return mMsgName;
    }

    public boolean isSocketOpenMessage() {
        return mMsgName.equals(MSG_SEND_INVITE) || mMsgName.equals(MSG_SEND_TEXT);
    }

    public boolean isSocketCloseMessage() {
        return mMsgName.equals(MSG_SEND_BYE) ||
                mMsgName.equals(MSG_SEND_REJECT) ||
                mMsgName.equals(MSG_ON_REJECT) ||
                mMsgName.equals(MSG_ON_BYE) ||
                mMsgName.equals(MSG_ON_RESULT_SEND_TEXT);
    }
}
