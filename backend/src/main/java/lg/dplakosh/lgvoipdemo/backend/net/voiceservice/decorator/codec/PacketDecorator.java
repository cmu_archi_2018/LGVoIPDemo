package lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.codec;

public abstract class PacketDecorator {
	String description = "Unknown Packet";

	public String getDescription() {
		return description;
	}

	public abstract double cost();

//	public abstract byte[] mix(byte[] packet);
}
