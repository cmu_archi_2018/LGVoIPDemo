package lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.secure;

import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.codec.PacketDecorator;

public class DTLS extends SecureDecorator {

	private final String TAG =  DTLS.class.getSimpleName();
	public DTLS(PacketDecorator decorable) {
		super(decorable);
	}

	public String getDescription() {
		return decorable.getDescription() + ", "+ DTLS.class.getSimpleName();
	}

	public double cost() {
		// write code here...
		return decorable.cost() + 0.20;

	}
}
