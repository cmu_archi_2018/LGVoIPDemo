package lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.codec;

public class GSMEFR extends PacketDecorator implements IVoicePacket {
	public GSMEFR() {
		description =  GSMEFR.class.getSimpleName();
	}
	public double cost() {
		return .89;
	}

	@Override
	public byte[] encode(byte[] payload, int[] samples) {
		return new byte[0];
	}

	@Override
	public byte[] decode(int[] samples, int count, byte[] payload, int length) {
		return new byte[0];
	}
}
