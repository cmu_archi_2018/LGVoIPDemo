package lg.dplakosh.lgvoipdemo.backend.utils;

import android.content.SharedPreferences;

public class Preference {

    private static final String TAG = "voipbend|PreferenceHelper";
    private static SharedPreferences mPref;

    public static void init(SharedPreferences pref) {
        mPref = pref;
    }

    public static boolean isLoopback() {
        return mPref.getBoolean("loopback_switch", false);
    }

    public static boolean isDevMode() {
        return mPref.getBoolean("key_developer_mode", false);
    }

    public static String getServerIpAddress() {
        return mPref.getString("server_ip", "10.0.3.3");
    }

    public static void setServerIpAddress(String ipAddress) {
        SharedPreferences.Editor editor = mPref.edit();
        editor.putString("server_ip", ipAddress);
        editor.apply();
    }

    public static int getServerPort() {
        return Integer.parseInt(mPref.getString("server_port", "5060"));
    }

    public static void setServerPort(String port) {
        SharedPreferences.Editor editor = mPref.edit();
        editor.putString("server_port", port);
        editor.apply();
    }

    public static String getMyPhoneNumber() {
        return mPref.getString("my_number", "100");
    }

    public static void setMyPhoneNumber(String accessToken) {
        SharedPreferences.Editor editor = mPref.edit();
        editor.putString("my_number", accessToken);
        editor.apply();
    }

    public static String getAccessToken() {
        return mPref.getString("accessToken", "0");
    }

    public static void setAccessToken(String accessToken) {
        SharedPreferences.Editor editor = mPref.edit();
        editor.putString("accessToken", accessToken);
        editor.apply();
    }

    public static String getCodec() {
        return mPref.getString("my_codec", "GSM");
    }

    public static boolean isVADOn() {
        return mPref.getBoolean("key_voice_active_detection",false);
    }


    public static boolean isEchoSuppressOn() {
         return mPref.getBoolean("key_echo_suppress",false);
    }
    public static boolean isEchoCancelingOn() {
        return mPref.getBoolean("key_echo_cancel",false);
    }

    /*
     * get service impl kind
     *
     * @return 0 use android internal framework, 1 use external code, 2 use COTS
     */
    public static int getVoiceServiceImplKind() {
        return 0;
    }


    public static String getDataExchangeProtocol() {
        return mPref.getString("my_data_protocol", "RTP");
    }
}
