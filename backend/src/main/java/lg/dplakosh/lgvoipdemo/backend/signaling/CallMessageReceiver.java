package lg.dplakosh.lgvoipdemo.backend.signaling;

import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import org.json.JSONException;

import lg.dplakosh.lgvoipdemo.backend.signaling.network.ISecurePacketIO;
import lg.dplakosh.lgvoipdemo.backend.utils.Logger;
import lg.dplakosh.lgvoipdemo.backend.utils.MyTextUtil;

class CallMessageReceiver extends Thread {

    private static final String TAG = "voipbend|CallMsgReceiver";

    private boolean mDone;
    private boolean mNeedToCloseSocket;
    private ISecurePacketIO mPacketIO;
    private JsonParser parser = new JsonParser();

    interface Callback {
        // Originating Call
        void onTrying();
        void onRinging();
        void onAccept(String remoteIpAddress, int remotePort, int remotePortRecv);
        void onReject(String cid, int reason);

        // Incoming Call
        void onInvite(ISecurePacketIO packetIO, CallMessageReceiver receiver, String peerNumber, String codec, String protocol);
        void onAck(String remoteIpAddress, int remotePort, int remotePortRecv);

        // Text message
        void onResultOfSendText(String senderNumber, String status, int result);
        void onNewTextMessage(String senderNumber, String text);

        // KeepAlive
        void onResultOfKeepAlive(int seqNo);

        // Common
        void onBye(String cid, int reason);
        void onError(int errorCode);
    }

    private Callback mCallback;

    CallMessageReceiver(ISecurePacketIO packetIO, Callback callback) {
        mPacketIO = packetIO;
        mCallback = callback;
        mDone = false;
    }

    @Override
    public void run() {

        Logger.d(TAG, "start CallMsgReceiver");

        try {
            while (!mDone) {
                String in = mPacketIO.readMessage();

                if (MyTextUtil.isEmpty(in)) {
                    Logger.e(TAG, "connection closed (in is null)");
                    if (!mDone) {
                        mDone = true;
                        mCallback.onError(200);
                    }
                    break;
                }

                Logger.d(TAG, "got message - " + in.length());
                String msg;

                try {
                    JsonElement reader = parser.parse(in);
                    msg = reader.getAsJsonObject().get("msg").getAsString();

                    Logger.d(TAG, "recv - " + msg);

                    switch (msg) {
                        case "invite":   receiveInvite(reader); break;
                        case "trying":   receiveTrying(); break;
                        case "ringing":  receiveRinging(); break;
                        case "accept":   receiveAccept(reader); break;
                        case "reject":   receiveReject(reader); break;
                        case "ack":       receiveAck(reader); break;
                        case "bye":       receiveBye(reader); break;
                        case "keepalive": receiveKeepAlive(reader); break;
                        case "textmsg":    receiveTextMessage(reader); break;
                        default:
                            Logger.e(TAG, "unknown msg:" + msg);
                            break;
                    }
                } catch (JSONException e) {
                    Logger.e(TAG, e);
                }
            }

            Logger.d(TAG, "exit CallMsgReceiver - socket closed ? " + mPacketIO.isSocketClosed());

        } finally {
            if (mNeedToCloseSocket) {
                mPacketIO.closeSocket();
            }
            mPacketIO = null;
        }
    }

    private void receiveInvite(JsonElement reader) throws JSONException {
        Logger.d(TAG, "receiveInvite");

        String peerNumber = reader.getAsJsonObject().get("from").getAsString();

        //--------------------------------
        // 전화를 건 사람의 코덱설정을 따라간다.
        // Follow the codec rule of some who begins calling.
        //--------------------------------
        String strCodec = "GSM";
        try {
            strCodec = reader.getAsJsonObject().get("codec").getAsString();
        } catch (Exception je) {
            Logger.e(TAG, "maybe conference call? default: GSM");
        }
        Log.e("CODEC", "caller set codec to " + strCodec);


        String strProtocol = "RTP";
        try {
            strProtocol = reader.getAsJsonObject().get("protocol").getAsString();
        } catch (Exception je) {
            Logger.e(TAG, "default: RTP");
        }


        mCallback.onInvite(mPacketIO, this, peerNumber, strCodec, strProtocol);
    }

    private void receiveTrying() {
        Logger.d(TAG, "receiveTrying");

        mCallback.onTrying();
    }

    private void receiveRinging() {
        Logger.d(TAG, "receiveRinging");
        mCallback.onRinging();
    }

    private void receiveAccept(JsonElement reader) throws JSONException {
        Logger.d(TAG, "receiveAccept");

        String remoteIpAddress = reader.getAsJsonObject().get("rtp_ip").getAsString();
        int remotePort = 9994;
        int remotePortRecv = 9996;
        try {
            remotePort = reader.getAsJsonObject().get("rtp_port").getAsInt();
            remotePortRecv = reader.getAsJsonObject().get("rtp_port_recv").getAsInt();
        } catch (Exception e) {
            Logger.e(TAG, e);
        }
        Logger.e(TAG, "receiveAccept_(" + remoteIpAddress + ":" + remotePort + ")");

        mCallback.onAccept(remoteIpAddress, remotePort, remotePortRecv);
    }

    private void receiveReject(JsonElement reader) throws JSONException {
        Logger.d(TAG, "receiveReject");

        int reason = reader.getAsJsonObject().get("reason").getAsInt();
        String cid = "";
        if (reader.getAsJsonObject().has("cid")) {
            reader.getAsJsonObject().get("cid").getAsString();
        }

        mCallback.onReject(cid, reason);
    }

    private void receiveAck(JsonElement reader) throws JSONException {
        Logger.d(TAG, "receiveAck");

        String remoteIpAddress = reader.getAsJsonObject().get("rtp_ip").getAsString();
        int remotePort = 9996;
        int remotePortRecv = 9998;
        try {
            remotePort = reader.getAsJsonObject().get("rtp_port").getAsInt();
            remotePortRecv = reader.getAsJsonObject().get("rtp_port_recv").getAsInt();
        } catch (Exception e) {
            Logger.e(TAG, e);
        }
        mCallback.onAck(remoteIpAddress, remotePort, remotePortRecv);
    }

    private void receiveBye(JsonElement reader) throws JSONException {
        Logger.d(TAG, "receiveBye");

        int reason = reader.getAsJsonObject().get("reason").getAsInt();
        String cid = "";
        if (reader.getAsJsonObject().has("cid")) {
            reader.getAsJsonObject().get("cid").getAsString();
        }
        mCallback.onBye(cid, reason);
    }

    private void receiveTextMessage(JsonElement reader) throws JSONException {
        Logger.d(TAG, "receiveTextMessage");

        String senderNumber = reader.getAsJsonObject().get("from").getAsString();

        if (reader.getAsJsonObject().has("status")) {
            String status = reader.getAsJsonObject().get("status").getAsString();
            int reason = reader.getAsJsonObject().get("reason").getAsInt();
            mCallback.onResultOfSendText(senderNumber, status, reason);
        }
        else if (reader.getAsJsonObject().has("message")) {
            String message = reader.getAsJsonObject().get("message").getAsString();
            mCallback.onNewTextMessage(senderNumber, message);
            mDone = true;
            mNeedToCloseSocket = true;
        }
    }

    private void receiveKeepAlive(JsonElement reader) throws JSONException {
        int seqNo = reader.getAsJsonObject().get("seqNo").getAsInt();
        mCallback.onResultOfKeepAlive(seqNo);
    }

    public void setAsDisconnectMode() {
        mDone = true;
    }
}
