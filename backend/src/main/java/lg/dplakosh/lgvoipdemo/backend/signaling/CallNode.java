package lg.dplakosh.lgvoipdemo.backend.signaling;

import lg.dplakosh.cif.ICallSession;

public class CallNode {

    private ICallSession mCallSession;
    private CallState mState = CallState.IDLE;

    public int sessionKey = 0;
    private boolean incoming = false;
    public final String peerPhoneNum = "";
    private static int mSessionKeyInLastActiveState = 0;
    public int terminate_reason;
    public int terminate_code;
    public String mCallId;

    public CallNode(ICallSession session) {
        mCallSession = session;
    }

    public void setCallSession(ICallSession session) {
        mCallSession = session;
    }

    public ICallSession getCallSession() {
        return mCallSession;
    }

    public void setState(CallState state) {
        mState = state;
        if (state == CallState.INCOMING) {
            incoming = true;
        } else if (state == CallState.DIALING) {
            incoming = false;
        } else if (state == CallState.ACTIVE){
            mSessionKeyInLastActiveState = sessionKey;
        }
    }

    public CallState getState() {
        return mState;
    }

    public boolean isOffHook() {
        return !mState.isIdle();
    }

    public boolean isRinging() {
        return mState.isRinging();
    }

    public boolean isDisconnected() {
        return !mState.isAlive();
    }

    public String getPeerNumber() {
        return peerPhoneNum;
    }

    public String getCallId() { return mCallId; }

    public void setCallId(String callId) { mCallId = callId; }
}
