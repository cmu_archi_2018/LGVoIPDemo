package lg.dplakosh.lgvoipdemo.backend.signaling;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;

import com.google.common.util.concurrent.SimpleTimeLimiter;
import com.google.common.util.concurrent.TimeLimiter;
import com.google.common.util.concurrent.UncheckedTimeoutException;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import lg.dplakosh.lgvoipdemo.backend.utils.Logger;
import lg.dplakosh.lgvoipdemo.backend.utils.Preference;

public class KeepAliveJobService extends JobService {

    private static String TAG = "voipbend|KeepService";
    boolean isWorking = false;
    boolean jobCancelled = false;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Logger.d(TAG, "onStartCommand");
        return START_NOT_STICKY;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Logger.d(TAG, "onCreate");
        jobCancelled = false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Logger.d(TAG, "onDestroy");
    }

    @Override
    public boolean onStartJob(JobParameters params) {
        Logger.d(TAG, "onStartJob - " + params.getJobId());

        isWorking = true;
        startWorkOnNewThread(params);

        return isWorking;
    }

    private void startWorkOnNewThread(final JobParameters jobParameters) {
        new Thread(() -> doWork(jobParameters)).start();
    }

    private void doWork(JobParameters jobParameters) {
        if (!CallManager.get().isNetworkAvailable()) {
            Logger.w(TAG, "onStartJob - skip : network not available");
            return;
        }

        if (CallManager.get().hasAliveCall()) {
            Logger.w(TAG, "onStartJob - skip : active call exist");
            return;
        }

        if (jobCancelled) {
            Logger.w(TAG, "onStartJob - skip : jobCancelled");
            return;
        }

        sendKeepAlive();

        isWorking = false;
        jobFinished(jobParameters, false);
    }

    @Override
    public boolean onStopJob(JobParameters params) {

        Logger.w(TAG, "onStopJob");

        jobCancelled = true;
        boolean needsReschedule = isWorking;
        jobFinished(params, needsReschedule);
        return needsReschedule;
    }

    public static void sendKeepAlive() {
        Logger.d(TAG, "sendKeepAlive");

        String SERVER_IP = Preference.getServerIpAddress();
        int SERVER_PORT = Preference.getServerPort() + 2;

        Logger.d(TAG, "Server Address : " + SERVER_IP + ":" + SERVER_PORT);

        Socket socket;
        try {
            InetAddress serverAddr = InetAddress.getByName(SERVER_IP);
            socket = new Socket(serverAddr, SERVER_PORT);
        } catch (UnknownHostException ex) {
            Logger.e(TAG, "Unknown Server Address : " + SERVER_IP + ":" + SERVER_PORT);
            return;
        } catch (IOException ex) {
            Logger.e(TAG, ex);
            ex.printStackTrace();
            return;
        }

        BufferedOutputStream outputStream;
        BufferedInputStream  inputStream;
        try {
            outputStream = new BufferedOutputStream(socket.getOutputStream());
            inputStream = new BufferedInputStream(socket.getInputStream());
        } catch (Exception ex) {
            Logger.e(TAG, "run - error in init stream - " + ex.getMessage());

            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
                Logger.e(TAG,e);
            }
            return;
        }

        sendKeepAliveMessage(outputStream);

        String in = readLine(inputStream);

        if (in == null) {
            Logger.e(TAG, "connection closed");
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
                Logger.e(TAG,e);

            }
            return;
        }

        Logger.d(TAG, "got message - " + in.length());

        try {
            JSONObject reader = new JSONObject(in);
            String msg = reader.getString("msg");

            Logger.d(TAG, "recv - " + msg);

            if (msg.equals("keepalive")) {
                Logger.d(TAG, "recv keep alive ack from server");
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                Logger.e(TAG, e);
            }
        }
    }

    private static String readLine(InputStream is) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));

        TimeLimiter timeLimiter = SimpleTimeLimiter.create(Executors.newSingleThreadExecutor());

        String line = "";
        try {
            line = timeLimiter.callWithTimeout(reader::readLine, 10, TimeUnit.SECONDS);
        } catch (TimeoutException | UncheckedTimeoutException e) {
            // timed out
            Logger.d(TAG, "readLine - time out : " + e.getMessage());
        } catch (Exception e) {
            // something bad happened while reading the line
            Logger.d(TAG, "readLine - exception - " + e.getMessage());
        }

        return line;
    }

    private static void sendKeepAliveMessage(BufferedOutputStream outputStream) {

        Logger.d(TAG, "sendKeepAliveMessage");

        String msg = ProtocolMessages.makeStandbyKeepAliveMessage();

        Logger.d(TAG, "sendKeepAliveMessage - " + msg);

        sendMessage(outputStream, msg.getBytes(), msg.length());
    }

    private static void sendMessage(final BufferedOutputStream outputStream, final byte[] data, final int dataLen) {
        new Thread() {
            @Override
            public void run() {
                try {
                    Logger.d(TAG, "sendMessage - E");
                    outputStream.write(data, 0, dataLen);
                    outputStream.flush();
                    Logger.d(TAG, "sendMessage - X");
                } catch (IOException e) {
                    e.printStackTrace();
                    Logger.e(TAG, e);

                }
            }
        }.start();
    }
}
