package lg.dplakosh.lgvoipdemo.backend.signaling;

import android.telephony.PhoneNumberUtils;
import java.util.HashMap;

import lg.dplakosh.cif.ICallSession;
import lg.dplakosh.lgvoipdemo.backend.utils.Logger;

public class CallManager {

    private static final String TAG = "voipbend|CallManager";
    private static int mSessionKeyBase;
    private final HashMap<Integer, CallNode> mCallNodeArray;

    private Object mNetworkAvailableSync = new Object();
    private boolean mNetworkAvailable;

    private static final CallManager mInstance = new CallManager();

    public static CallManager get() {
        return mInstance;
    }

    public static int createNewSessionKey() {
        return ++mSessionKeyBase;
    }

    private CallManager() {
        mCallNodeArray = new HashMap<>();
    }

    public void addNewCallNode(int sessionKey, CallNode node) {
        node.sessionKey = sessionKey;
        mCallNodeArray.put(node.sessionKey, node);
    }

    public void removeAllCallNode() {
        mCallNodeArray.clear();
    }

    public void removeCallNode(int sessionKey) {
        mCallNodeArray.remove(sessionKey);
    }

    public void removeCallNode(final ICallSession session) {
        for (HashMap.Entry<Integer, CallNode> entry : mCallNodeArray.entrySet()) {
            if (entry.getValue() == session) {
                removeCallNode(entry.getKey());
                break;
            }
        }
    }

    public CallNode getCallNode(int sessionKey) {
        return mCallNodeArray.get(sessionKey);
    }

    public CallNode getCallNode(ICallSession session) {
        if (session != null) {
            for (HashMap.Entry<Integer, CallNode> entry : mCallNodeArray.entrySet()) {
                if (entry.getValue() == session) {
                    return entry.getValue();
                }
            }
        }

        return null;
    }

    public CallNode getCallNode(String phoneNumber) {

        for (HashMap.Entry<Integer, CallNode> entry : mCallNodeArray.entrySet()) {
            CallNode node = entry.getValue();

            String normalizePeerNumber = PhoneNumberUtils.normalizeNumber(node.peerPhoneNum);
            if (phoneNumber != null
                    && PhoneNumberUtils.normalizeNumber(phoneNumber).equals(normalizePeerNumber)) {
                return node;
            }
        }
        return null;
    }

    public HashMap<Integer, CallNode> getAllCallNode() {
        return mCallNodeArray;
    }

    public int getActiveCallCount() {
        int activeCallCount = 0;

        for (HashMap.Entry<Integer, CallNode> entry : mCallNodeArray.entrySet()) {
            CallNode node = entry.getValue();

            if (node.getState().isAlive()) {
                activeCallCount++;
                Logger.i(TAG, "activeCallCount++ : " + activeCallCount);
            } else {
                Logger.e(TAG, "state is not alive");
            }
        }

        return activeCallCount;
    }

    public boolean hasAliveCall() {
        return getActiveCallCount() > 0;
    }

    public boolean isNetworkAvailable() {
        synchronized (mNetworkAvailableSync) {
            return mNetworkAvailable;
        }
    }

    public void setNetworkAvailable(boolean networkAvailable) {
        synchronized (mNetworkAvailableSync) {
            mNetworkAvailable = networkAvailable;
        }
    }
}
