package lg.dplakosh.lgvoipdemo.backend.net.voiceservice.transport;

import java.net.InetAddress;
import java.net.SocketException;

public interface IVoiceStream {

    int create(String address) throws SocketException;

    int write(byte[] stream, int length);

    int read(byte[] stream, int length);

    InetAddress getLocalAddress();

    int getLocalPort();

    InetAddress getRemoteAddress();

    int getRemotePort();

    boolean isBusy();

    int getMode();

    void setMode(int mode);

    void associate(InetAddress address, int port);

    int getSocket();

    void release();

    void join(IVoiceMixer group);

    void finalize();
}

