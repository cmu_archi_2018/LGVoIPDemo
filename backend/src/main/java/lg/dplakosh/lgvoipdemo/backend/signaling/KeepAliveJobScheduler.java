package lg.dplakosh.lgvoipdemo.backend.signaling;

import android.annotation.TargetApi;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.os.Build;
import android.util.Log;

import lg.dplakosh.lgvoipdemo.backend.utils.AppContext;

public class KeepAliveJobScheduler {
    private static String TAG = "voipbend|KeepAliveJobScheduler";

    private static final int JOB_ID = 1;

    private static KeepAliveJobScheduler sInstance = new KeepAliveJobScheduler();

    private JobScheduler mJobService;

    private KeepAliveJobScheduler() {
        mJobService = (JobScheduler)AppContext.get().getSystemService(Context.JOB_SCHEDULER_SERVICE);
    }

    public static KeepAliveJobScheduler get() {
        return sInstance;
    }

    @TargetApi(Build.VERSION_CODES.N)
    public void register(long period) {
        Log.d(TAG, "register().. - period : " + period);

        if (mJobService.getPendingJob(JOB_ID) != null) {
            Log.d(TAG, "register().. - already registered");
            mJobService.cancel(JOB_ID);
        }

        JobInfo job = new JobInfo.Builder(
                JOB_ID,
                new ComponentName(AppContext.get(), KeepAliveJobService.class))
                .setPeriodic(period)
                .build();

        int resultCode = mJobService.schedule(job);

        if (resultCode == JobScheduler.RESULT_SUCCESS) {
            Log.d(TAG, "Job scheduled!");
        } else {
            Log.d(TAG, "Job not scheduled");
        }
    }

    public void unregister() {
        Log.d(TAG, "unregister()..");

        mJobService.cancel(JOB_ID);
    }
}
