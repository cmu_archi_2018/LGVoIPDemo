package lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.secure;

import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.codec.PacketDecorator;

public abstract class SecureDecorator extends PacketDecorator {
	protected PacketDecorator decorable;
	public SecureDecorator(PacketDecorator decorable) {
		this.decorable = decorable;
	}

	public abstract String getDescription();
}
