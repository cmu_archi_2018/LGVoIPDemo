package lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.codec;

public class OPUS extends PacketDecorator implements IVoicePacket {
	public OPUS() {
		description = OPUS.class.getSimpleName();
	}

	public double cost() {
		return 1.05;
	}

	@Override
	public byte[] encode(byte[] payload, int[] samples) {
		return new byte[0];
	}

	@Override
	public byte[] decode(int[] samples, int count, byte[] payload, int length) {
		return new byte[0];
	}
}
