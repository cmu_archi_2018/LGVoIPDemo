package lg.dplakosh.lgvoipdemo.backend.net;

import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.codec.PacketDecorator;
import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.codec.GSM0611;
import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.codec.GSM0632;
import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.codec.GSMEFR;
import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.secure.DTLS;
import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.secure.Extention;
import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.secure.SRTP;
import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.secure.ZRTP;

public class PacketTest {

	public static void test() {
		PacketDecorator decorable = new GSM0611();
		System.out.println(decorable.getDescription() + " $" + decorable.cost());

		PacketDecorator decorable2 = new GSM0632();
		decorable2 = new DTLS(decorable2);
		decorable2 = new DTLS(decorable2);
		decorable2 = new SRTP(decorable2);
		System.out.println(decorable2.getDescription() + " $" + decorable2.cost());

		PacketDecorator decorable3 = new GSMEFR();
		decorable3 = new ZRTP(decorable3);
		decorable3 = new DTLS(decorable3);
		decorable3 = new SRTP(decorable3);
		System.out.println(decorable3.getDescription() + " $" + decorable3.cost());

		PacketDecorator decorable4 = new Extention(new GSM0611());
		System.out.println(decorable4.getDescription() + " $" + decorable4.cost());
	}
}
