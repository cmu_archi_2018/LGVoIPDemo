package lg.dplakosh.lgvoipdemo.backend.signaling;

import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;

import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.IVoiceService;
import lg.dplakosh.lgvoipdemo.backend.signaling.messages.CallReplyMessage;
import lg.dplakosh.lgvoipdemo.backend.signaling.messages.CallRequestMessage;
import lg.dplakosh.lgvoipdemo.backend.signaling.messages.CallMessage;
import lg.dplakosh.lgvoipdemo.backend.signaling.network.ISecurePacketIO;
import lg.dplakosh.lgvoipdemo.backend.signaling.network.SecurePacketIO;
import lg.dplakosh.lgvoipdemo.backend.utils.AppContext;
import lg.dplakosh.lgvoipdemo.backend.utils.Logger;
import lg.dplakosh.lgvoipdemo.backend.utils.MyTextUtil;
import lg.dplakosh.lgvoipdemo.backend.utils.Preference;
import lg.dplakosh.lgvoipdemo.backend.utils.Worker;

public class CallMessageHandler extends Thread implements CallMessageReceiver.Callback {

    private static final String TAG = "voipbend|CallMessageHandler";

    private CallThreadEventListener mCallEventListener;

    private int FLAG_CALL = 0x1;
    private int FLAG_MSG = 0x2;
    private int mSocketUseFlag = 0;

    private int mSessionKey;
    private String mCallPeerNumber = "";
    private String mMsgPeerNumber = "";

    private Worker mWorker;
    private Handler mHandler;
    private IVoiceService mVoiceService;
    private ISecurePacketIO mPacketIO;
    private CallMessageReceiver mCallMessageReceiver;

    public CallMessageHandler(IVoiceService service, ISecurePacketIO packetIO) {
        mVoiceService = service;
        mPacketIO = packetIO;
    }

    public void startNewCallMessageReceiver(Socket socket) {

        Logger.d(TAG, "startNewCallMessageReceiver");

        SecurePacketIO packetIO = new SecurePacketIO();
        if (packetIO.setSocket(socket)) {
            new CallMessageReceiver(packetIO, this).start();
        }
        else {
            Logger.d(TAG, "startNewCallMessageReceiver - drop incoming call due to socket error");
        }
    }

    public void setCallThreadEventListner(CallThreadEventListener listener, int sessionKey) {
        Logger.d(TAG, "setCallThreadEventListner - " + listener);
        // TODO - listener --> arrayList
        mCallEventListener = listener;
        mSessionKey = sessionKey;
    }

    private final Queue<CallMessage> mMessageQueue = new LinkedBlockingQueue<>(100);//new PriorityBlockingQueue<>();

    @Override
    public void run() {

        while ( true ) {
            try {
                CallMessage msg;

                synchronized ( mMessageQueue ) {
                    while ( mMessageQueue.isEmpty() )
                        mMessageQueue.wait();

                    msg = mMessageQueue.remove();
                }

                if (msg.getMsgName().equals(CallMessage.MSG_EXIT_LOOP)) {
                    Logger.d(TAG, "exit from run");
                    break;
                }

                if (!preProcessMessage(msg))
                    continue;

                // Process the work item
                msg.process();

                postProcessMessage(msg);
            }
            catch ( InterruptedException ie ) {
                break;  // Terminate
            }
        }
    }

    public void stopCallMessageHandler() {
        putMessage(new CallRequestMessage(CallMessage.MSG_EXIT_LOOP));
    }

    private boolean preProcessMessage(CallMessage msg) {

        if (msg.isSocketOpenMessage()) {
            Logger.d(TAG, "preProcessMessage - SocketOpenMsg : " + msg.getMsgName());

            if (!mPacketIO.createSocket()) {
                Logger.d(TAG, "preProcessMessage - peer not available");
                mCallEventListener.rejected(CallTerminateReason.PEER_NOT_AVAILABLE);
                return false;
            }

            if (msg.getMsgName().equals(CallMessage.MSG_SEND_TEXT)) {
                mSocketUseFlag |= FLAG_MSG;
            } else {
                mSocketUseFlag |= FLAG_CALL;
            }

            if (mCallMessageReceiver == null) {
                mCallMessageReceiver = new CallMessageReceiver(mPacketIO, this);
                mCallMessageReceiver.start();
            }

            startTimerThread();
        }

        return true;
    }

    private void postProcessMessage(CallMessage msg) {
        // socket close
        if (msg.isSocketCloseMessage()) {

            Logger.d(TAG, "postProcessMessage - SocketCloseMsg : " + msg.getMsgName());

            if (msg.getMsgName().equals(CallMessage.MSG_ON_RESULT_SEND_TEXT)) {
                mSocketUseFlag &= ~FLAG_MSG;
            } else {
                mSocketUseFlag &= ~FLAG_CALL;
            }

            if (mSocketUseFlag == 0) {
                Logger.d(TAG, "postProcessMessage - close socket");

                closeSocket();
            }
        }
    }

    private void closeSocket() {
        if (mCallMessageReceiver != null) {
            mCallMessageReceiver.setAsDisconnectMode();
            mCallMessageReceiver = null;
        }
        mPacketIO.closeSocket();
    }

    private void putMessage(CallMessage message) {
        synchronized (mMessageQueue) {
            mMessageQueue.add(message);
            mMessageQueue.notify();
        }
    }

    public void sendInvite(String peerNumber) {

        putMessage(new CallRequestMessage(CallMessage.MSG_SEND_INVITE) {
            // TODO
            /*
                        if (CallManager.get().hasAliveCall()) {
                Logger.d(TAG, "preProcessMessage - already active call");
                mCallEventListener.rejected(CallTerminateReason.LOCAL_BUSY);
                return false;
            }
             */
            @Override
            public boolean process() {
                boolean resultOk = false;

                if (mPacketIO.checkNetworkAvailable(getMsgName())) {
                    // Standby RTP
                    mVoiceService.initializeAudioStream();

                    mCallPeerNumber = peerNumber;

                    mVoiceService.setSecureStream(Preference.getDataExchangeProtocol() );
                    mVoiceService.setVADOn(Preference.isVADOn());
                    mVoiceService.setEchoSuppressorOn(Preference.isEchoSuppressOn());
                    mVoiceService.setEchoCancelingOn(Preference.isEchoCancelingOn());
                    mVoiceService.setCodec(Preference.getCodec());

                    String msg = ProtocolMessages.makeInviteMessage(mCallEventListener.getCallId(),
                            mCallPeerNumber);

                    if (sendMessage(msg)) {
                        resultOk = true;

                        if (mHandler != null) {
                            mHandler.sendEmptyMessageDelayed(MSG_SEND_CALL_TIMEOUT, CALL_MSG_TIMEOUT);
                        }
                    }
                }

                if (!resultOk) {
                    notifyDisconnectedEvent(CallTerminateReason.CONNECTION_LOST, "sendInvite");
                    mVoiceService.endCall();
                }

                return true;
            }
        });
    }

    private boolean sendMessage(String msg) {
        return mPacketIO.sendMessage(msg);
    }

    public void sendRinging() {

        putMessage(new CallRequestMessage(CallMessage.MSG_SEND_RINGING) {
            @Override
            public boolean process() {
                boolean resultOk = false;

                if (mPacketIO.checkNetworkAvailable(getMsgName())) {
                    String msg = ProtocolMessages.makeRingingMessage(mCallEventListener.getCallId());
                    if (sendMessage(msg)) {
                        resultOk = true;
                    }
                }

                if (!resultOk) {
                    notifyDisconnectedEvent(CallTerminateReason.CONNECTION_LOST, "sendRinging");
                    mVoiceService.endCall();
                }
                return resultOk;
            }});
    }

    public void sendAccept() {

        putMessage(new CallRequestMessage(CallMessage.MSG_SEND_ACCEPT) {
            @Override
            public boolean process() {
                boolean resultOk = false;

                if (mPacketIO.checkNetworkAvailable(getMsgName())) {
                    String msg = ProtocolMessages.makeAcceptMessage(mCallEventListener.getCallId(),
                            mVoiceService.getLocalPortOfSendStream(), mVoiceService.getLocalPortOfReceiveStream());
                    if (sendMessage(msg)) {
                        resultOk = true;
                        if (mHandler != null) {
                            mHandler.sendEmptyMessageDelayed(MSG_SEND_CALL_TIMEOUT, CALL_MSG_TIMEOUT);
                        }
                    }
                }

                if (!resultOk) {
                    notifyDisconnectedEvent(CallTerminateReason.CONNECTION_LOST, "sendAccept");
                    mVoiceService.endCall();
                }
                return resultOk;
            }});
    }

    /**
     * 내가 전화를 건다음 상대방이 억셉트를 하면 handleAccept가 온다음 내가 sendAck를 보낸다.
     */
    private void sendAck() {

        putMessage(new CallRequestMessage(CallMessage.MSG_SEND_ACK) {
            @Override
            public boolean process() {
                boolean resultOk = false;

                if (mPacketIO.checkNetworkAvailable(getMsgName())) {
                    String msg = ProtocolMessages.makeAckMessage(mCallEventListener.getCallId(),
                            mVoiceService.getLocalPortOfSendStream(), mVoiceService.getLocalPortOfReceiveStream());
                    if (sendMessage(msg)) {
                        resultOk = true;
                    }
                }
                if (!resultOk) {
                    notifyDisconnectedEvent(CallTerminateReason.CONNECTION_LOST, "sendAck");
                    mVoiceService.endCall();
                }
                return resultOk;
            }});
    }

    public void sendReject(int reason) {

        putMessage(new CallRequestMessage(CallMessage.MSG_SEND_REJECT) {
            @Override
            public boolean process() {

                if (mPacketIO.checkNetworkAvailable(getMsgName())) {
                    String msg = ProtocolMessages.makeRejectMessage(mCallEventListener.getCallId(), reason);
                    sendMessage(msg);
                }

                notifyDisconnectedEvent(CallTerminateReason.TERMINATE_REASON_USER, "sendReject");

                mVoiceService.endCall();
                return false;
            }});
    }

    public void sendBye(int reason) {

        Logger.d(TAG, "sendBye: " + reason);

        putMessage(new CallRequestMessage(CallMessage.MSG_SEND_BYE) {
            @Override
            public boolean process() {

                if (mPacketIO.checkNetworkAvailable(getMsgName())) {
                    String callId = null;
                    if (mCallEventListener != null) {
                        callId = mCallEventListener.getCallId();
                    }
                    String msg = ProtocolMessages.makeByeMessage(callId, reason);
                    sendMessage(msg);
                }

                notifyDisconnectedEvent(reason, "sendBye");

                mVoiceService.endCall();
                return true;
            }});
    }

    public void sendText(String peerNumber, String text) {

        Logger.d(TAG, "sendText: " + peerNumber + ", " + text);

        if (mHandler != null && mHandler.hasMessages(MSG_SEND_TEXT_TIMEOUT)) {
            Logger.d(TAG, "sendText: sending already inprogress");
            // TODO - notify to user
            return;
        }

        mMsgPeerNumber = peerNumber;

        putMessage(new CallRequestMessage(CallMessage.MSG_SEND_TEXT) {
            @Override
            public boolean process() {
                String msg = ProtocolMessages.makeSendTextMessage(peerNumber, text);
                if (!sendMessage(msg)) {
                    // TODO - send notification ui
                    Logger.d(TAG, "sendText - sendMessage fail");
                    return false;
                }

                if (mHandler != null) {
                    Logger.d(TAG, "sendText - start timer : " + TEXT_MSG_TIMEOUT);
                    mHandler.sendEmptyMessageDelayed(MSG_SEND_TEXT_TIMEOUT, TEXT_MSG_TIMEOUT);
                }
                else  {
                    Logger.d(TAG, "sendText - no timer ready : " + mHandler);
                }

                return true;
            }
        });
    }

    @Override
    public void onInvite(ISecurePacketIO packetIO, CallMessageReceiver receiver, String peerNumber, String strCodec, String protocol) {

        putMessage(new CallReplyMessage(CallMessage.MSG_ON_INVITE) {
            @Override
            public boolean process() {

                if (!packetIO.updateBufferedIOStream()) {
                    Logger.d(TAG, "onInvite - silently reject call : socket error");
                    packetIO.closeSocket();
                    return true;
                }

                if (CallManager.get().hasAliveCall()) {
                    Logger.d(TAG, "onInvite - silently reject call : busy");

                    String msg = ProtocolMessages.makeRejectMessage(mCallEventListener.getCallId(),
                            CallTerminateReason.PEER_BUSY);

                    packetIO.sendMessage(msg);
                    return true;
                }

                closeSocket();

                mPacketIO = packetIO;
                mCallMessageReceiver = receiver;

                mVoiceService.initializeAudioStream();

                mCallPeerNumber = peerNumber;

                //--------------------------------
                // 전화를 건 사람의 코덱설정을 따라간다.
                //--------------------------------
                Logger.d(TAG, "caller set codec to:" + strCodec + ",protocol: "+ protocol);
                mVoiceService.setSecureStream(protocol);
                mVoiceService.setVADOn(Preference.isVADOn());
                mVoiceService.setEchoSuppressorOn(Preference.isEchoSuppressOn());
                mVoiceService.setEchoCancelingOn(Preference.isEchoCancelingOn());
                mVoiceService.setCodec(strCodec);

                // Create CallNode
                CallNode node = new CallNode(null);
                CallManager.get().addNewCallNode(CallManager.createNewSessionKey(), node);

                // Send Intent
                Intent intent = new Intent("com.lge.callsvc.action.VOICE_INVITATION");
                intent.setData(Uri.fromParts("tel", peerNumber, null));
                intent.putExtra("com.lge.callsvc.extra.SESSION_KEY", node.sessionKey);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                AppContext.get().sendBroadcast(intent);

                startTimerThread();

                return false;
            }
        });
    }

    @Override
    public void onTrying() {
        if (mHandler != null) {
            mHandler.removeMessages(MSG_SEND_CALL_TIMEOUT);
        }
        putMessage(new CallReplyMessage(CallMessage.MSG_ON_TRYING) {
        });
    }

    @Override
    public void onRinging() {
        if (mHandler != null) {
            mHandler.removeMessages(MSG_SEND_CALL_TIMEOUT);
        }
        putMessage(new CallReplyMessage(CallMessage.MSG_ON_RINGING) {
            @Override
            public boolean process() {
                if (mCallEventListener != null) {
                    mCallEventListener.progressing();
                }
                return true;
            }
        });
    }

    @Override
    public void onAccept(String remoteIpAddress, int remotePort, int remotePortRecv) {
        if (mHandler != null) {
            mHandler.removeMessages(MSG_SEND_CALL_TIMEOUT);
        }
        putMessage(new CallReplyMessage(CallMessage.MSG_ON_ACCEPT) {
            @Override
            public boolean process() {
                Logger.d(TAG, "onAccept");

                if (mVoiceService != null) {
                    mVoiceService.startStream(remoteIpAddress, remotePort, remotePortRecv);
                } else {
                    Logger.e(TAG, "mVoiceService is null");
                }

                sendAck();

                if (mCallEventListener != null) {
                    mCallEventListener.connected();

                    startCallKeepAlive();
                } else {
                    Logger.e(TAG, "mCallEventListener == null");
                }

                return true;
            }
        });
    }

    private static final int MSG_SEND_KEEP_ALIVE = 200;
    private static final int MSG_SEND_CALL_TIMEOUT = 201;
    private static final int MSG_SEND_TEXT_TIMEOUT = 202;
    private static final int CALL_MSG_TIMEOUT = 5000;   // 5 secs
    private static final int TEXT_MSG_TIMEOUT = 5000;   // 5 secs

    private void startTimerThread() {
        Logger.d(TAG, "startTimerThread");

        mWorker = new Worker();

        mHandler = new Handler(mWorker.getLooper()) {
            @Override
            public void handleMessage(Message msg) {
                switch (msg.what) {
                    case MSG_SEND_KEEP_ALIVE:
                        Logger.d(TAG, "send KeepAlive");

                        String keepAliveMsg = ProtocolMessages.makeCallKeepAliveMessage(
                                                      mCallPeerNumber,
                                                      mCallEventListener.getCallId());
                        CallMessageHandler.this.sendMessage(keepAliveMsg);

                        sendEmptyMessageDelayed(MSG_SEND_KEEP_ALIVE, 20 * 1000);
                        break;

                    case MSG_SEND_CALL_TIMEOUT: {
                        Logger.d(TAG, "Send Call Message Timeout");
                        notifyDisconnectedEvent(CallTerminateReason.TIMEOUT, "sendmsg timeout");
                        break;
                    }

                    case MSG_SEND_TEXT_TIMEOUT: {
                        Logger.d(TAG, "Send Text Message Timeout");
                        onResultOfSendText(mMsgPeerNumber, "nack", CallTerminateReason.TIMEOUT);
                        break;
                    }

                    default:
                        break;
                }
            }
        };
    }

    private void startCallKeepAlive() {
        if (mHandler != null) {
            mHandler.sendEmptyMessageDelayed(MSG_SEND_KEEP_ALIVE, 20 * 1000);
        }
    }

    @Override
    public void onReject(String cid, int reason) {
        if (mHandler != null) {
            mHandler.removeMessages(MSG_SEND_CALL_TIMEOUT);
        }

        CallNode node = CallManager.get().getCallNode(mSessionKey);
        if (node != null && !MyTextUtil.isEmpty(cid)) {
            if (cid.equals(node.getCallId())) {
                Logger.w(TAG, "onReject - ignore by different cid - " + cid + " <-> " + node.getCallId());
                return;
            }
        }

        putMessage(new CallReplyMessage(CallMessage.MSG_ON_REJECT) {
            @Override
            public boolean process() {
                if (mCallEventListener == null) {
                    Logger.e(TAG, "mCallEventListener == null");
                    return false;
                }

                CallNode node = CallManager.get().getCallNode(mSessionKey);
                if (node != null && node.getState().isAlive() && mCallEventListener != null) {
                    mCallEventListener.rejected(reason);
                }

                mVoiceService.endCall();

                return true;
            }
        });
    }

    @Override
    public void onAck(String remoteIpAddress, int remotePort, int remotePortRecv) {
        if (mHandler != null) {
            mHandler.removeMessages(MSG_SEND_CALL_TIMEOUT);
        }
        putMessage(new CallReplyMessage(CallMessage.MSG_ON_ACK) {
            @Override
            public boolean process() {
                mVoiceService.startStream(remoteIpAddress, remotePort, remotePortRecv);

                if (mCallEventListener != null) {
                    mCallEventListener.connected();

                    startCallKeepAlive();
                } else {
                    Logger.e(TAG, "mCallEventListener == null");
                }
                return true;
            }
        });
    }

    @Override
    public void onBye(String cid, final int reason) {
        if (mHandler != null) {
            mHandler.removeMessages(MSG_SEND_CALL_TIMEOUT);
        }

        CallNode node = CallManager.get().getCallNode(mSessionKey);
        if (node != null && !MyTextUtil.isEmpty(cid)) {
            if (cid.equals(node.getCallId())) {
                Logger.w(TAG, "onBye - ignore by different cid - " + cid + " <-> " + node.getCallId());
                return;
            }
        }

        putMessage(new CallReplyMessage(CallMessage.MSG_ON_BYE) {
            @Override
            public boolean process() {
                if (mCallEventListener == null) {
                    Logger.e(TAG, "handleBye>mCallEventListener == null");
                    return false;
                }

                notifyDisconnectedEvent(reason, "received bye");

                mVoiceService.endCall();
                return true;
            }
        });
    }

    private void notifyDisconnectedEvent(int reason, String logMsg) {

        Logger.d(TAG, logMsg);

        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
            mHandler = null;
        }

        if (mWorker != null) {
            mWorker.quit();
            mWorker = null;
        }

        CallNode node = CallManager.get().getCallNode(mSessionKey);

        if (node != null && node.getState().isAlive() && mCallEventListener != null) {
            mCallEventListener.disconnected(reason);
        }
    }

    @Override
    public void onResultOfSendText(String senderNumber, String status, int result) {

        if (mHandler != null) {
            mHandler.removeMessages(MSG_SEND_TEXT_TIMEOUT);
        }

        putMessage(new CallReplyMessage(CallMessage.MSG_ON_RESULT_SEND_TEXT) {
            @Override
            public boolean process() {
                if ("nack" .equalsIgnoreCase(status)) {
                    // TODO - send notification ui fail
                } else {
                    // TODO - send notification ui ok
                }
                return true;
            }
        });
    }

    @Override
    public void onNewTextMessage(String senderNumber, String text) {
        putMessage(new CallReplyMessage(CallMessage.MSG_ON_NEW_TEXT) {
            @Override
            public boolean process() {

                // Insert To DB
                Logger.d(TAG, "onNewTextMessage - from : " + senderNumber + ", "
                        + "msg :" + text);

                DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss", Locale.US);
                Calendar cal = Calendar.getInstance();
                String date = df.format(cal.getTime());
                ContentValues values = new ContentValues();
                values.put("DATE", date);
                values.put("PHONENO",  senderNumber);
                values.put("READORNOT", "NOTREAD");
                values.put("MESSAGE", text);

                Uri CONTENT_URI = Uri.parse("content://lg.dplakosh.lgvoipdemo.db.sms.SmsContentProvider/SMSDATA");

                AppContext.get().getContentResolver().insert(CONTENT_URI, values);

                // Show in notification
                VoipMsgNotification.getInstance().updateSoftPhoneNotification(senderNumber, date);

                // Send Intent
                Intent intent = new Intent("com.lge.callsvc.action.NEW_MESSAGE");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setData(Uri.fromParts("tel", senderNumber, null));
                AppContext.get().sendBroadcast(intent);

                return true;
            }
        });
    }

    @Override
    public void onResultOfKeepAlive(int seqNo) {
        //MSG_ON_RESULT_KEEPALIVE
    }

    @Override
    public void onError(int errorCode) {

        Logger.e(TAG, "onError - errorCode : " + errorCode);

        boolean closeSocket = false;

        if (errorCode == 100) {  // start fail
            notifyDisconnectedEvent(CallTerminateReason.PEER_NOT_AVAILABLE, "CallMessageReceiver not started");
            mVoiceService.endCall();
            closeSocket = true;
        }
        else if (errorCode == 200) {   // io fail
            notifyDisconnectedEvent(CallTerminateReason.PEER_NOT_AVAILABLE, "CallMessageReceiver exit loop");
            mVoiceService.endCall();
            closeSocket = true;
        }

        if (closeSocket) {
            closeSocket();
            mSocketUseFlag = 0;
        }
    }
}
