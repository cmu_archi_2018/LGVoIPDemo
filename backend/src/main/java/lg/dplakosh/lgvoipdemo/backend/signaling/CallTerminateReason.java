package lg.dplakosh.lgvoipdemo.backend.signaling;

public class CallTerminateReason {

    public static final int NONE = 0;
    public static final int TERMINATE_REASON_USER = 1;
    public static final int LOCAL_BUSY = 3;
    public static final int INTERNAL_ERROR = 4;
    public static final int TIMEOUT = 5;

    public static final int PEER_BUSY = 100;
    public static final int PEER_NOT_AVAILABLE = 101;
    public static final int PEER_INVALID_MEDIA_PARAM  = 102;
    public static final int CONNECTION_LOST = 103;
}
