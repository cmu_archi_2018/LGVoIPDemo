package lg.dplakosh.lgvoipdemo.backend.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.util.Log;

import lg.dplakosh.lgvoipdemo.backend.signaling.CallService;

public class SystemEventReceiver extends BroadcastReceiver {

    private static final String TAG = "voipbend|SystemEventRv";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive() - " + intent.getAction());

        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)
                || intent.getAction().equals(Intent.ACTION_MY_PACKAGE_REPLACED)) {
            context.startService(new Intent(context.getApplicationContext(), CallService.class));
        } else if(intent.getAction().equals(WifiManager.WIFI_STATE_CHANGED_ACTION)){

            int wifiState = intent.getIntExtra(WifiManager.EXTRA_WIFI_STATE, WifiManager.WIFI_STATE_UNKNOWN);
            int previousWifiState = intent.getIntExtra(WifiManager.EXTRA_PREVIOUS_WIFI_STATE, WifiManager.WIFI_STATE_UNKNOWN);

            Log.i(TAG, "wifiState Changed " + wifiState + " ->" + previousWifiState);
        }
    }
}
