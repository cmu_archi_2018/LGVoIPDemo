package lg.dplakosh.lgvoipdemo.backend.net;

import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.IVoiceService;
import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.VoiceServiceManagerImplAndroid;
import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.VoiceServiceImplSevenHours;
import lg.dplakosh.lgvoipdemo.backend.utils.Preference;

public class VoiceServiceManager {

    private volatile static IVoiceService instance;

    public static int readSetting() {
        //read from preference or whatever.
        return Preference.getVoiceServiceImplKind();
    }

    public static IVoiceService getInstance() {
        if (instance == null) {
            synchronized (VoiceServiceManager.class) {
                //TODO read preference config value
                // Adapt android framework's internal android.net.rtp
                switch (readSetting()) {
                    case 0:
                    default:
                        if (instance == null) {
                            instance = new VoiceServiceManagerImplAndroid();
                        }
                        break;
                    case 1:
                        if (instance == null) {
                            instance = new VoiceServiceImplSevenHours();
                        }
                        break;
                }

            }
        }
        return instance;
    }
}
