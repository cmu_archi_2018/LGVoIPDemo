package lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.secure;

import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.codec.IVoicePacket;
import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.codec.PacketDecorator;

public class Extention extends SecureDecorator  implements IVoicePacket {

	public Extention(PacketDecorator decorable) {
		super(decorable);
	}

	public String getDescription() {
		return decorable.getDescription() + ", "+ Extention.class.getSimpleName();
	}

	public double cost() {
		// write code here...
		return decorable.cost() + 0.10;
	}

	@Override
	public byte[] encode(byte[] payload, int[] samples) {
		return new byte[0];
	}

	@Override
	public byte[] decode(int[] samples, int count, byte[] payload, int length) {
		return new byte[0];
	}
}
