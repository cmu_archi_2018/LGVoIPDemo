package lg.dplakosh.lgvoipdemo.backend.signaling;

public interface CallThreadEventListener {

    String getCallId();
    void updateCallId(String callId);

    void progressing();
    void connected();
    void rejected(int reason);
    void disconnected(int reason);
}
