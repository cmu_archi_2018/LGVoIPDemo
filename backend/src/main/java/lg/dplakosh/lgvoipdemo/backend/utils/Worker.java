package lg.dplakosh.lgvoipdemo.backend.utils;

import android.os.Looper;
import java.io.PrintWriter;
import java.io.StringWriter;

public class Worker implements Runnable {

    private final Object mLock = new Object();
    private Looper mLooper;

    public Worker() {
        Thread t = new Thread(null, this);
        t.start();
        synchronized (mLock) {
            while (mLooper == null) {
                try {
                    mLock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    StringWriter sw = new StringWriter();
                    e.printStackTrace(new PrintWriter(sw));

//                    Toast.makeText(mContext, sw.toString(), Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    public Looper getLooper() {
        return mLooper;
    }

    public void run() {
        synchronized (mLock) {
            Looper.prepare();
            mLooper = Looper.myLooper();
            mLock.notifyAll();
        }
        Looper.loop();
    }

    public void quit() {
        mLooper.quit();
    }
}
