package lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.echo;

import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.codec.IVoicePacket;
import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.codec.PacketDecorator;

public class EchoSuppresor extends EchoDecorator  implements IVoicePacket {

	private final String TAG =  EchoSuppresor.class.getSimpleName();
	public EchoSuppresor(PacketDecorator decorable) {
		super(decorable);
	}

	public String getDescription() {
		return decorable.getDescription() + ", "+ EchoSuppresor.class.getSimpleName();
	}

	public double cost() {
		// write code here...
		return decorable.cost() + 0.20;

	}

	@Override
	public byte[] encode(byte[] payload, int[] samples) {
		return new byte[0];
	}

	@Override
	public byte[] decode(int[] samples, int count, byte[] payload, int length) {
		return new byte[0];
	}
}
