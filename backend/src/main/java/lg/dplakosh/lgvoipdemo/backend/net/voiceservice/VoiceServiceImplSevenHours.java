package lg.dplakosh.lgvoipdemo.backend.net.voiceservice;

import android.util.Log;

import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.codec.AMR;
import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.codec.GSM0611;
import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.codec.GSMEFR;
import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.codec.OPUS;
import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.codec.PCMA;
import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.codec.PCMU;
import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.codec.PacketDecorator;
import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.echo.EchoCanceller;
import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.echo.EchoSuppresor;
import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.secure.DTLS;
import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.secure.SRTP;
import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.secure.ZRTP;
import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.vad.VoiceActivityDetectionV1;

public class VoiceServiceImplSevenHours implements IVoiceService {

    private final String TAG = VoiceServiceImplSevenHours.class.getSimpleName();

    private boolean isVADOn;
    private boolean isEchoSuppersserOn;
    private String mStrSecureKind;
    private boolean isEchoCancellerOn;

    private PacketDecorator decorable;

    private String mCodec = "GSM";

    @Override
    public void initializeAudioStream() {

    }

    @Override
    public void endCall() {

    }

    @Override
    public void setCodec(String codec) {
        mCodec = codec;
    }

    @Override
    public void setEchoCancelingOn(boolean bSet) {

        isEchoCancellerOn = bSet;
    }

    @Override
    public void setEchoSuppressorOn(boolean bSet) {
        isEchoSuppersserOn = bSet;

    }

    @Override
    public void setVADOn(boolean bSet) {
        isVADOn = bSet;
    }

    @Override
    public void setSecureStream(String secureProtocol) {
        mStrSecureKind = secureProtocol;
    }

    @Override
    public String[] getAvailableCodecs() {
        return new String[]{"GSM_EFR", "AMR", "PCMU", "PCMA", "GSM", "OPUS", "SPEEX"};
    }

    @Override
    public String getCurrentCodec() {
        return mCodec;
    }

    @Override
    public int getLocalPortOfSendStream() {
        return 0;
    }

    @Override
    public int getLocalPortOfReceiveStream() {
        return 0;
    }

    @Override
    public void startStream(String remoteIpAddress, int remotePortOfSend, int remotePortOfRecv) {
        switch (getCurrentCodec()) {
            case "GSM_EFR":
                decorable = new GSMEFR();
                break;
            case "AMR":
                decorable = new AMR();
                break;
            case "PCMU":
                decorable = new PCMU();
                break;
            case "PCMA":
                decorable = new PCMA();
                break;
            case "OPUS":
                decorable = new OPUS();
                break;
            case "GSM":
            default:
                decorable = new GSM0611();
                Log.i(TAG, decorable.getDescription() + " $" + decorable.cost());
                break;
        }
        if (isVADOn) {
            decorable = new VoiceActivityDetectionV1(decorable);
        }
        if (isEchoCancellerOn) {
            decorable = new EchoCanceller(decorable);
        }
        if (isEchoSuppersserOn) {
            decorable = new EchoSuppresor(decorable);
        }
        switch (mStrSecureKind) {
            case "DTLS":
                decorable = new DTLS(decorable);
                break;
            case "SRTP":
                decorable = new SRTP(decorable);
                break;
            case "ZRTP":
                decorable = new ZRTP(decorable);
                break;
            default:
                Log.i(TAG, "NO secure support defeind " + mStrSecureKind);
                break;
        }
    }
}
