package lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.codec;

public class GSM0632 extends PacketDecorator implements IVoicePacket {
	public GSM0632() {
		description = GSM0632.class.getSimpleName();
	}

	public double cost() {
		return .99;
	}

	@Override
	public byte[] encode(byte[] payload, int[] samples) {
		return new byte[0];
	}

	@Override
	public byte[] decode(int[] samples, int count, byte[] payload, int length) {
		return new byte[0];
	}
}
