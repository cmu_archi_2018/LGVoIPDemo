package lg.dplakosh.lgvoipdemo.backend.signaling;

import android.util.Log;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

import lg.dplakosh.lgvoipdemo.backend.utils.Logger;

public class ServerEventListenThread extends Thread {

    private static final String TAG = "voipbend|ServerEventListenThread";

    private static final int INCOMING_CALL_PORT = 5060;

    private CallMessageHandler mCallMessageHandler;

    ServerEventListenThread(CallMessageHandler callMessageHandler) {
        mCallMessageHandler = callMessageHandler;
    }

    @Override
    public void run() {

        ServerSocket listenSocket;

        try {
            listenSocket = new ServerSocket(INCOMING_CALL_PORT);
        } catch (IOException e) {
            Logger.e(TAG, "Fail to open listening socket(5060)", e);
            return;
        }

        Logger.d(TAG, "start listening in " + INCOMING_CALL_PORT);

        while (true) {

            try {
                Socket socket = listenSocket.accept();

                Log.d(TAG, "accept new connection");

                mCallMessageHandler.startNewCallMessageReceiver(socket);

            } catch (IOException e) {
                Logger.e(TAG, "Fail to accept new connection", e);
            }
        }
    }
}

