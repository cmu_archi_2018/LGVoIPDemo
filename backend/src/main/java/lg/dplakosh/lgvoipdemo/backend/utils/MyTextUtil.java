package lg.dplakosh.lgvoipdemo.backend.utils;

public class MyTextUtil {
    public static boolean isEmpty(CharSequence str) {
        return str == null || str.length() == 0;
    }
}
