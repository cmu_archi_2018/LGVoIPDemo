package lg.dplakosh.lgvoipdemo.backend.signaling.messages;

public class CallReplyMessage extends CallMessage {

    public CallReplyMessage(String msgName) {
        super(msgName);
    }

    public boolean isRequest() { return false; }
}
