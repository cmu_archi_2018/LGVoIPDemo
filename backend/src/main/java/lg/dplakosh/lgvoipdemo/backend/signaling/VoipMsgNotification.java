package lg.dplakosh.lgvoipdemo.backend.signaling;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import lg.dplakosh.lgvoipdemo.backend.R;
import lg.dplakosh.lgvoipdemo.backend.utils.AppContext;

public class VoipMsgNotification {

    private static VoipMsgNotification sInstance = new VoipMsgNotification();
    private NotificationManager mNotificationManager;
    private static final int VOIP_SMS_NOTIFICATION_INDEX = 400;
    private static final String ACTION_SHOW_SMS = "lg.dplakosh.lgvoipdemo.ACTION_SHOW_SMS";

    public static VoipMsgNotification getInstance() {
        return sInstance;
    }

    private VoipMsgNotification() {
        mNotificationManager = (NotificationManager) AppContext.get().getSystemService(Context.NOTIFICATION_SERVICE);
    }

    public synchronized void updateSoftPhoneNotification(String from, String date) {

        Notification notification = makeNotification(from, date);

        if (notification != null) {
            mNotificationManager.notify(VOIP_SMS_NOTIFICATION_INDEX, notification);
        }
    }

    private Notification makeNotification(String from, String date) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(AppContext.get(),
                "voipmsg");

        builder.setCategory(Notification.CATEGORY_MESSAGE);
        builder.setSmallIcon(android.R.drawable.stat_notify_chat);
        builder.setContentIntent(createSmsActivityIntent());
        builder.setAutoCancel(true);
        builder.setShowWhen(false);

        builder.setPriority(NotificationManager.IMPORTANCE_HIGH);
        builder.setContentTitle(AppContext.get().getText(R.string.new_voip_message));

        String msg = "Message from " + from + " " + date;

        builder.setContentText(msg);

        return builder.build();
    }

    private PendingIntent createSmsActivityIntent() {
        Intent intent = new Intent(ACTION_SHOW_SMS);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        return PendingIntent.getActivity(AppContext.get(), 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
    }
}
