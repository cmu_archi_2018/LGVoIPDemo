package lg.dplakosh.lgvoipdemo.backend.net.voiceservice.transport;

public class VoiceMixerDoubleBufferAndroid implements IVoiceMixer {
    @Override
    public IVoiceStream[] getStreams() {
        return new IVoiceStream[0];
    }

    @Override
    public int getMode() {
        return 0;
    }

    @Override
    public void setMode(int mode) {

    }

    @Override
    public void add(IVoiceStream stream) {

    }

    @Override
    public void remove(IVoiceStream stream) {

    }

    @Override
    public void clear() {

    }
}
