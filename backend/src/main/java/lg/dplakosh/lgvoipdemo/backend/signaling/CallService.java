package lg.dplakosh.lgvoipdemo.backend.signaling;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.LinkProperties;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.os.IBinder;
import android.os.RemoteException;
import android.support.annotation.Nullable;
import android.text.format.DateUtils;
import android.util.Log;

import lg.dplakosh.cif.ICallService;
import lg.dplakosh.cif.ICallServiceListener;
import lg.dplakosh.cif.ICallSession;
import lg.dplakosh.lgvoipdemo.backend.net.VoiceServiceManager;
import lg.dplakosh.lgvoipdemo.backend.signaling.network.SecurePacketIO;
import lg.dplakosh.lgvoipdemo.backend.utils.Helper;
import lg.dplakosh.lgvoipdemo.backend.utils.Logger;
import lg.dplakosh.lgvoipdemo.backend.utils.Preference;

public class CallService extends Service {

    private static final String TAG = "voipbend|CallService";

    private ServerEventListenThread mListeningThread;
    private CallMessageHandler  mCallMessageHandler;
    private ConnectivityManager networkManager;

    @Override
    public void onCreate() {
        super.onCreate();

        networkManager =  (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        Log.d(TAG, "OnCreate");
        checkNetworkConnection();
    }

    private final ConnectivityManager.NetworkCallback networkCallback = new ConnectivityManager.NetworkCallback() {
        @Override
        public void onAvailable(Network network) {
            super.onAvailable(network);

            Log.d(TAG, "networkCallback - onAvailable");

            checkNetworkConnection();
        }

        @Override
        public void onLost(Network network) {
            super.onLost(network);

            Log.d(TAG, "networkCallback - onLost");
            CallManager.get().setNetworkAvailable(false);
        }

        public void onLinkPropertiesChanged(Network network, LinkProperties linkProperties) {
            Log.d(TAG, "networkCallback - onLinkPropertiesChanged");
            KeepAliveJobScheduler.get().register(DateUtils.MINUTE_IN_MILLIS * 15);
        }

        public void onUnavailable() {
            Log.d(TAG, "networkCallback - onUnavailable");
        }
    };

    private void checkNetworkConnection() {
        if (!networkManager.isActiveNetworkMetered()) {
            Log.d(TAG, "networkCallback - WIFI Available");
            Helper.setLocalIpAddress(Helper.getLocalIP(getApplicationContext()));
            CallManager.get().setNetworkAvailable(true);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        if (mCallMessageHandler == null) {
            mCallMessageHandler = new CallMessageHandler(VoiceServiceManager.getInstance(),
                    new SecurePacketIO());
            mCallMessageHandler.start();
        }

        if (mListeningThread == null) {
            mListeningThread = new ServerEventListenThread(mCallMessageHandler);
            mListeningThread.start();

            NetworkRequest request = new NetworkRequest.Builder()
                    .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
                    .build();

            networkManager.registerNetworkCallback(request, networkCallback);
        }

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Log.d(TAG, "OnDestroy");
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind - " + mCallService);
        return mCallService;
    }


    private final ICallService.Stub mCallService = new ICallService.Stub() {
        private CallSession mCallSession = null;
        private ICallServiceListener mUCServiceListener = null;

        @Override
        public void setLoginResult(boolean result, String myNumber, String accessToken) throws RemoteException {

            Log.d(TAG, "setLoginResult");

            if (result) {
                if (accessToken.equals("123454321")) {
                    myNumber = Preference.getMyPhoneNumber();
                }
                Preference.setMyPhoneNumber(myNumber);
                Preference.setAccessToken(accessToken);

                KeepAliveJobScheduler.get().register(DateUtils.MINUTE_IN_MILLIS * 15);
            }
            else {
                KeepAliveJobScheduler.get().unregister();
            }
        }

        @Override
        public ICallSession openSession() throws RemoteException {
            int sessionKey = CallManager.createNewSessionKey();

            Log.d(TAG, "openSession");


            //현재 폰의 로컬 ip를 세팅한다.
            //TODO 콜 시점을 바꿔야 될수도 ..
            Helper.setLocalIpAddress(Helper.getLocalIP(getApplicationContext()));

            try {
                mCallSession = new CallSession(getApplicationContext(), sessionKey, mCallMessageHandler);
            } catch (Exception ex) {
                Logger.e(TAG, ex);
                return null;
            }

            CallNode node = new CallNode(mCallSession);
            CallManager.get().addNewCallNode(sessionKey, node);
            node.setState(CallState.DIALING);

            return mCallSession;
        }

        @Override
        public void closeSession(ICallSession session) throws RemoteException {
            Log.d(TAG, "closeSession");
            CallNode node = CallManager.get().getCallNode(session);
            if (node != null) {

                if (node.isDisconnected() == false) {
                    CallSession ucSession = (CallSession) session;
                    ucSession.terminate(CallTerminateReason.TERMINATE_REASON_USER);
                }
                node.setState(CallState.IDLE);
                CallManager.get().removeCallNode(session);
            }
        }

        @Override
        public void sendTextMessage(String phoneNumber, String msg) throws RemoteException {
            Log.d(TAG, phoneNumber + ":" + msg);

            // TODO
            mCallMessageHandler.sendText(phoneNumber, msg);
        }

        @Override
        public String retrieveMessage(int msgKey) throws RemoteException {
            return null;
        }

        @Override
        public ICallSession attachSession(int sessionKey) throws RemoteException {

            //TODO 콜 시점을 바꿔야 될수도 ..
            Helper.setLocalIpAddress(Helper.getLocalIP(getApplicationContext()));

            CallNode node = CallManager.get().getCallNode(sessionKey);
            if (node == null) {
                Log.e(TAG, "Invalid sessionKey.. something wrong");
                return null;
            }

            mCallSession = new CallSession(getApplicationContext(), sessionKey, mCallMessageHandler);
            node.setCallSession(mCallSession);
            node.setState(CallState.INCOMING);

            mCallSession.sendRinging();

            return mCallSession;
        }

        @Override
        public void setListener(ICallServiceListener listener) throws RemoteException {
            Log.d(TAG, "setListener");
            mUCServiceListener = listener;
        }
    };
}
