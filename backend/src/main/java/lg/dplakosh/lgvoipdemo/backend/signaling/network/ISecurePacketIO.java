package lg.dplakosh.lgvoipdemo.backend.signaling.network;

import java.io.BufferedOutputStream;
import java.io.InputStream;
import java.net.Socket;

public interface ISecurePacketIO {
    boolean createSocket();
    void closeSocket();
    boolean updateBufferedIOStream();
    boolean sendMessage(String msg);
    boolean isSocketClosed();
    boolean checkNetworkAvailable(String msgName);

    boolean setSocket(Socket socket);
    String readMessage();
}
