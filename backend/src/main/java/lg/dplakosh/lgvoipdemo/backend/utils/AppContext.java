package lg.dplakosh.lgvoipdemo.backend.utils;

import android.content.Context;
import android.content.ContextWrapper;

public class AppContext extends ContextWrapper {

    private static AppContext instance;

    public AppContext(Context context) {
        super(context);
        instance = this;
    }

    public static Context get() {
        return instance;
    }
}
