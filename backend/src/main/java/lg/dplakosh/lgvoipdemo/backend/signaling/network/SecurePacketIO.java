package lg.dplakosh.lgvoipdemo.backend.signaling.network;

import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import lg.dplakosh.lgvoipdemo.backend.utils.Logger;
import lg.dplakosh.lgvoipdemo.backend.utils.Preference;

/*
// Open SSLSocket
SocketFactory sf = SSLSocketFactory.getDefault();
SSLSocket socket = (SSLSocket) sf.createSocket("gmail.com", 443);
HostnameVerifier hv = HttpsURLConnection.getDefaultHostnameVerifier();
SSLSession s = socket.getSession();

// Verify that the certicate hostname is for mail.google.com
// This is due to lack of SNI support in the current SSLSocket.
if (!hv.verify("mail.google.com", s)) {
    throw new SSLHandshakeException("Expected mail.google.com, "
                                    "found " + s.getPeerPrincipal());
}

// At this point SSLSocket performed certificate verificaiton and
// we have performed hostname verification, so it is safe to proceed.

// ... use socket ...
socket.close();
*/

public class SecurePacketIO implements ISecurePacketIO {

    private static final String TAG = "voipbend|SecurePacketIO";
    private final Object mSendLock = new Object();

    private Socket mSocket;
    private BufferedOutputStream mOutputStream;
    private BufferedInputStream mInputStream;

    @Override
    public boolean createSocket() {
        if (isSocketClosed()) {
            return _createSocket();
        } else {
            Logger.d(TAG, "already - socket exist");
        }
        return true;
    }

    private boolean _createSocket() {
        String SERVER_IP = Preference.getServerIpAddress();
        int SERVER_PORT = Preference.getServerPort();
        Logger.d(TAG, "createSocket - Signaling Address: " + SERVER_IP + ":" + SERVER_PORT);

        Socket socket;
        try {
            InetAddress serverAddr = InetAddress.getByName(SERVER_IP);
            socket = new Socket(serverAddr, SERVER_PORT);
        } catch (UnknownHostException ex) {
            Logger.e(TAG, "Unknown Server Address : " + SERVER_IP + ":" + SERVER_PORT);
            return false;
        }  catch (IOException ex) {
            Logger.e(TAG, ex);
            return false;
        }

        return setSocket(socket);
    }

    @Override
    public boolean setSocket(Socket socket) {
        mSocket = socket;
        return updateBufferedIOStream();
    }

    @Override
    public void closeSocket() {
        if (mSocket != null) {
            try {
                mSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            mSocket = null;
        }
    }

    @Override
    public boolean updateBufferedIOStream() {
        try {
            mOutputStream = new BufferedOutputStream(mSocket.getOutputStream());
            mInputStream = new BufferedInputStream(mSocket.getInputStream());
            Logger.d(TAG, "updateBufferedIOStream - ok");
            return true;
        } catch (Exception ex) {
            Logger.e(TAG, "error in updateBufferedIOStream - " + ex.getMessage());
            Logger.e(TAG, ex);
        }
        return false;
    }

    @Override
    public boolean sendMessage(String msg) {
        return sendMessage(mOutputStream, msg);
    }

    private boolean sendMessage(BufferedOutputStream outputStream, String msg) {

        final byte[] data = msg.getBytes();
        final int dataLen = msg.length();

        try {
            synchronized (mSendLock) {
                Log.d(TAG, "sendMessage - E");
                if (outputStream != null) {
                    Log.d(TAG, "sendMessage : " + msg);
                    outputStream.write(data, 0, dataLen);
                    outputStream.flush();
                }
                Log.d(TAG, "sendMessage - X");
                return true;
            }
        } catch (IOException e) {
            Logger.e(TAG, "sendMessage failure - ", e);
        }

        return false;
    }

    @Override
    public boolean isSocketClosed() {
        return mSocket == null || mSocket.isClosed();
    }

    @Override
    public boolean checkNetworkAvailable(String msgName) {
        Logger.d(TAG, msgName);

        if (mSocket == null) {
            Logger.d(TAG, "checkNetworkAvailable - socket not opened");
            return false;
        }

        return true;
    }

    @Override
    public String readMessage() {

        Logger.d(TAG, "readMessage - " + mSocket.isConnected());

        BufferedReader reader = new BufferedReader(new InputStreamReader(mInputStream));

        String line = null;
        try {
            line = reader.readLine();
        } catch (IOException e) {
            Logger.d(TAG, "readMessage - error : " + e.getMessage());
        }

        return line;
    }
}
