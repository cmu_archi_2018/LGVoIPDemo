package lg.dplakosh.lgvoipdemo.backend.utils;

public class StrictModeUtil {

    public static void RemoveStrictMode() {
        int SDK_INT = android.os.Build.VERSION.SDK_INT;

        if (SDK_INT > 8) {
            android.os.StrictMode.ThreadPolicy policy = new android.os.StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            android.os.StrictMode.setThreadPolicy(policy);
        }
    }
}
