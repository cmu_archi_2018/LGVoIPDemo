package lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.secure;

import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.codec.IVoicePacket;
import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.codec.PacketDecorator;

public class ZRTP extends SecureDecorator  implements IVoicePacket {

	public ZRTP(PacketDecorator decorable) {
		super(decorable);
	}

	public String getDescription() {
		return decorable.getDescription() + ", "+ ZRTP.class.getSimpleName();
	}
	public double cost() {
		// write code here...
		return decorable.cost() + 0.15;

	}

	@Override
	public byte[] encode(byte[] payload, int[] samples) {
		return new byte[0];
	}

	@Override
	public byte[] decode(int[] samples, int count, byte[] payload, int length) {
		return new byte[0];
	}
}
