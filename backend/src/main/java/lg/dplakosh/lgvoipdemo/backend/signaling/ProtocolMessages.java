package lg.dplakosh.lgvoipdemo.backend.signaling;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import lg.dplakosh.lgvoipdemo.backend.utils.Helper;
import lg.dplakosh.lgvoipdemo.backend.utils.Logger;
import lg.dplakosh.lgvoipdemo.backend.utils.MyTextUtil;
import lg.dplakosh.lgvoipdemo.backend.utils.Preference;

public class ProtocolMessages {

    private static String TAG = "voipbend|ProtocolMessages";

    private static int NextSeqNo = 1;

    public static String makeInviteMessage(String callId, String peerNumber ) {
        /*{
           “msg” : “invite”,
           “seqNo” : 1,
           “cid” : “guid”,
           “to”: 1004,
           “from”:1001,
           “protocol”:”…”
           “codec”:”…”
           "token":
        }*/

        Gson gson = new Gson();
        JsonObject object = new JsonObject();
        object.addProperty("msg", "invite");
        object.addProperty("seqNo", NextSeqNo++);
        object.addProperty("cid", callId);
        object.addProperty("to", peerNumber);
        object.addProperty("from", Preference.getMyPhoneNumber());
        object.addProperty("protocol", Preference.getDataExchangeProtocol());   //[20180618]rtp, srtp, zrtp...
        object.addProperty("codec", Preference.getCodec());  //[20180618] GSM_EFR, AMR, GSM, PCMU, PCMA
        object.addProperty("token", Preference.getAccessToken());
        String json = gson.toJson(object) + "\n";

        Logger.d(TAG, "makeInviteMessage - " + json);

        return json;
    }

    public static String makeRingingMessage(String callId) {
        /*{
            msg : ringing,
            seqNo : 11,
            cid : guid
            token:
        }*/

        Gson gson = new Gson();
        JsonObject object = new JsonObject();
        object.addProperty("msg", "ringing");
        object.addProperty("seqNo", NextSeqNo++);
        object.addProperty("cid", callId);
        object.addProperty("token", Preference.getAccessToken());
        String json = gson.toJson(object) + "\n";

        Logger.d(TAG, "makeRingingMessage - " + json);

        return json;
    }

    public static String makeAcceptMessage(String callId, int localPort, int localPortRecv) {
        /*{
            msg : accept,
            seqNo : 12,
            cid : guid,
            rtp ip : 192.168.0.11
            rtp port : 50000
            rtp_port_recv
            token:
        }*/

        Gson gson = new Gson();
        JsonObject object = new JsonObject();
        object.addProperty("msg", "accept");
        object.addProperty("seqNo", NextSeqNo++);
        object.addProperty("cid", callId);
        object.addProperty("rtp_ip", Helper.getmStrMyIpAddress());
        object.addProperty("rtp_port", localPort);
        object.addProperty("rtp_port_recv", localPortRecv);
        object.addProperty("token", Preference.getAccessToken());
        String json = gson.toJson(object) + "\n";

        Logger.d(TAG, "makeAcceptMessage - " + json);

        return json;
    }

    public static String makeAckMessage(String callId, int localPort, int localPortRecv) {
        /*{
            msg : ack,
            seqNo : 2,
            cid : guid
            rtp ip : 192.168.0.12
            rtp port : 50010
            rtp_port_recv
            token:
        }*/

        Gson gson = new Gson();
        JsonObject object = new JsonObject();
        object.addProperty("msg", "ack");
        object.addProperty("seqNo", NextSeqNo++);
        object.addProperty("cid", callId);
        object.addProperty("rtp_ip", Helper.getmStrMyIpAddress());
        object.addProperty("rtp_port", localPort);
        object.addProperty("rtp_port_recv", localPortRecv);
        object.addProperty("token", Preference.getAccessToken());
        String json = gson.toJson(object) + "\n";

        Logger.d(TAG, "makeAckMessage - " + json);

        return json;
    }

    public static String makeRejectMessage(String callId, int reason) {
        /*{
            msg : reject,
            seqNo : 13,
            cid : guid,
            reason : 100
            token:
        }*/

        Gson gson = new Gson();
        JsonObject object = new JsonObject();
        object.addProperty("msg", "reject");
        object.addProperty("seqNo", NextSeqNo++);
        object.addProperty("cid", callId);
        object.addProperty("reason", reason);
        if (!MyTextUtil.isEmpty(Preference.getAccessToken())) {
            object.addProperty("token", Preference.getAccessToken());
        }
        String json = gson.toJson(object) + "\n";

        Logger.d(TAG, "makeRejectMessage - " + json);

        return json;
    }

    public static String makeByeMessage(String callId, int reason) {
        /*{
            msg : bye,
            seqNo : 6,
            cid : guid,
            reason :
            token:
        }*/
        Gson gson = new Gson();
        JsonObject object = new JsonObject();
        object.addProperty("msg", "bye");
        object.addProperty("seqNo", NextSeqNo++);
        if (!MyTextUtil.isEmpty(callId)) {
            object.addProperty("cid", callId);
        }
        object.addProperty("reason", reason);
        if (!MyTextUtil.isEmpty(Preference.getAccessToken())) {
            object.addProperty("token", Preference.getAccessToken());
        }
        String json = gson.toJson(object) + "\n";

        Logger.d(TAG, "makeByeMessage - " + json);

        return json;
    }

    public static String makeCallKeepAliveMessage(String peerNumber, String callId) {
        /*{
            msg : keepalive,
            status : calling,
            from : 111-222-3333,
            to : 111-222-4444,
            cid :
            token :
        }*/
        Gson gson = new Gson();
        JsonObject object = new JsonObject();
        object.addProperty("msg", "keepalive");
        object.addProperty("status", "calling");
        object.addProperty("from", Preference.getMyPhoneNumber());
        object.addProperty("to", peerNumber);
        if (!MyTextUtil.isEmpty(callId)) {
            object.addProperty("cid", callId);
        }
        object.addProperty("token", Preference.getAccessToken());
        String json = gson.toJson(object) + "\n";

        Logger.d(TAG, "makeCallKeepAliveMessage - " + json);

        return json;
    }

    public static String makeStandbyKeepAliveMessage() {
        /*{
              msg : keepalive,
              status : standby,
              phoneNo : 111-222-3333,
              token :
        }*/
        Gson gson = new Gson();
        JsonObject object = new JsonObject();
        object.addProperty("msg", "keepalive");
        object.addProperty("status", "standby");
        object.addProperty("phoneNo", Preference.getMyPhoneNumber());
        object.addProperty("token", Preference.getAccessToken());
        String json = gson.toJson(object) + "\n";

        Logger.d(TAG, "makeStandbyKeepAliveMessage - " + json);

        return json;
    }

    public static String makeSendTextMessage(String peerNumber, String message) {
        /*{
            msg : textmsg,
            seqNo : 1
            from : 111-222-3333,
            to : 111-222-4444,
            message : “bla bla bla”
            token : “”,
        }*/

        /*{
            msg : textmsg,
            seqNo : 1
            status : ack | nack,
            reason : 0
            from : 111-222-3333,
            to : 111-222-4444,
            token : “”,
        }*/
        Gson gson = new Gson();
        JsonObject object = new JsonObject();
        object.addProperty("msg", "textmsg");
        object.addProperty("seqNo", "NextSeqNo++");
        object.addProperty("from", Preference.getMyPhoneNumber());
        object.addProperty("to", peerNumber);
        object.addProperty("message", message);
        object.addProperty("token", Preference.getAccessToken());
        String json = gson.toJson(object) + "\n";

        Logger.d(TAG, "makeSendTextMessage - " + json);

        return json;
    }
}
