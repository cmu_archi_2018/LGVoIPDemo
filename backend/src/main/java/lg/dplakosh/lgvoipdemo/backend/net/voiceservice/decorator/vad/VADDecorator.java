package lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.vad;

import lg.dplakosh.lgvoipdemo.backend.net.voiceservice.decorator.codec.PacketDecorator;

public abstract class VADDecorator extends PacketDecorator{
    protected PacketDecorator decorable;
    public VADDecorator(PacketDecorator decorable) {
        this.decorable = decorable;
    }

    public abstract String getDescription();
}
