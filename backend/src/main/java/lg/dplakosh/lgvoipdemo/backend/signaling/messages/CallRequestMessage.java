package lg.dplakosh.lgvoipdemo.backend.signaling.messages;

public class CallRequestMessage extends CallMessage {

    public CallRequestMessage(String msgName) {
        super(msgName);
    }

    public boolean isRequest() { return true; }
}
