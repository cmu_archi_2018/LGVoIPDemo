package lg.dplakosh.lgvoipdemo.backend.net.voiceservice;

public interface IVoiceService {


    /**
     * end calling
     * this should be called when call is done. Otherwise stream will remain opened
     */
    void endCall();

    /**
     * warning codec will be set when call is made 실제 call 이 이루어질때 세팅됨.
     * @param codec see getAvailableCodecs
     */
    void setCodec(String codec);

    void setEchoCancelingOn(boolean bSet);

    void setEchoSuppressorOn(boolean bSet);

    void setVADOn(boolean bSet);

    void setSecureStream(String secureProtocol);

    /**
     * show available codecs
     * @return
     */
    String[] getAvailableCodecs();

    /**
     * show current setting of codec
     * @return currently set codec
     */
    String getCurrentCodec();

    /**
     * show port number of receving stream
     * @return port number set to stream of receiving
     */
    int getLocalPortOfSendStream();

    /**
     *
     * @return port number set to stream of receiving
     */
    int getLocalPortOfReceiveStream();


    /**
     * ready
     */
    void initializeAudioStream();
    /**
     *
     * @param remoteIpAddress
     * @param remotePortOfSend
     * @param remotePortOfRecv
     */
    void startStream(String remoteIpAddress, int remotePortOfSend, int remotePortOfRecv);
}
