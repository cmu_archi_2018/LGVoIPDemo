package lg.dplakosh.lgvoipdemo.backend.net.voiceservice;

import android.net.rtp.AudioCodec;
import android.net.rtp.AudioGroup;
import android.net.rtp.AudioStream;
import android.net.rtp.RtpStream;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.util.Log;
import android.widget.Toast;

import java.net.InetAddress;
import java.net.SocketException;

import lg.dplakosh.lgvoipdemo.backend.utils.Helper;
import lg.dplakosh.lgvoipdemo.backend.utils.Logger;
import lg.dplakosh.lgvoipdemo.backend.utils.AppContext;

/**
 *
 */
public class VoiceServiceManagerImplAndroid implements IVoiceService {

    private final static String TAG = VoiceServiceManagerImplAndroid.class.getSimpleName();

    /**
     * Client functions to manipulate the AudioGroup and AudioStream.
     */
    private AudioGroup audioGroupSendOnly;
    private AudioGroup audioGroupReceiveOnly;

    private AudioStream audioStreamReceiveOnly;
    private AudioStream audioStreamSendOnly;

    private InetAddress mLocalnetAddress;
    private int mLocalAudioPort, localAudioPortRecv;

    //송신하는 경우 로컬포트가 임의로 정해진다.
    @Override
    public int getLocalPortOfSendStream() {
        return mLocalAudioPort;
    }

    @Override
    public int getLocalPortOfReceiveStream() {
        return localAudioPortRecv;
    }

    private String mStrCodec = "GSM";


    private boolean isEchoSuppersserOn;
    private String mStrSecureKind;
    private boolean isEchoCancellerOn;
    private boolean isVADOn;


    /**
     * RFC 규약에 의해 로컬의 랜덤포트를 열어서 오디오스트림을 시작한다.
     */
    private void getReadyAudioStream() {
        Log.e(TAG, "getReadyAudioStream()");

        try {
            mLocalnetAddress = Helper.getLocalIpAsInetAddress();
        } catch (Exception e) {
            Logger.e(TAG, e);
        }
        try {
            audioStreamReceiveOnly = new AudioStream(mLocalnetAddress);
            audioStreamSendOnly = new AudioStream(mLocalnetAddress);

        } catch (SocketException e) {
            Logger.e(TAG, e);
        }
        mLocalAudioPort = audioStreamReceiveOnly.getLocalPort();
        localAudioPortRecv = audioStreamSendOnly.getLocalPort();
    }

    public VoiceServiceManagerImplAndroid() {
        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
    }

    /**
     * Resets all data of the Service
     */
    private void closeAll() {
        Log.e(TAG, "closeAll()");
        if (audioStreamReceiveOnly != null) {
            audioStreamReceiveOnly.join(null);
            audioStreamReceiveOnly.release();

        } else {
            Logger.e(TAG, "Something is wrong! closeAll is called before audioStreamReceiveOnly is called!!");
        }

        if (audioStreamSendOnly != null) {
            audioStreamSendOnly.join(null);
            audioStreamSendOnly.release();

        } else {
            Logger.e(TAG, "Something is wrong! closeAll is called before audioStreamSendOnly is called!!");
        }
        audioGroupSendOnly = null;
        audioGroupReceiveOnly = null;

        audioStreamReceiveOnly = null;
        audioStreamSendOnly = null;

        mLocalAudioPort = 0;
        localAudioPortRecv = 0;
        mLocalnetAddress = null;
        Log.i("VoiceAndroidAdapterImpl", "Resources reset.");
    }


    //통화를 종료한다.
    public void endCall() {
        Log.e(TAG, "endCall()");
        closeAll();
    }

    public void initializeAudioStream() {
        Log.e(TAG, "initializeAudioStream()");
        mLocalnetAddress = Helper.getLocalIpAsInetAddress();
        getReadyAudioStream();
    }

    /**
     * To be called when the following variables are set:
     * remoteInetAddress, remoteAudioPort
     * mLocalnetAddress, mLocalAudioPort
     * When both users run this function, full-duplex audio conversation starts.
     */
    public void startStream(String remoteIpAddress, int remotePortOfSend, int remotePortOfRecv) {

        Log.e(TAG, "startStream()" + remoteIpAddress + ":" + remotePortOfSend);

        AudioCodec localAudioCodec;
        switch (mStrCodec) {
            case "GSM_EFR":
                localAudioCodec = AudioCodec.GSM_EFR;
                break;
            case "AMR":
                localAudioCodec = AudioCodec.AMR;
                break;
            case "PCMU":
                localAudioCodec = AudioCodec.PCMU;
                break;
            case "PCMA":
                localAudioCodec = AudioCodec.PCMA;
                break;
            case "GSM":
                localAudioCodec = AudioCodec.GSM;
                break;
            default:
                localAudioCodec = AudioCodec.GSM;
                Log.e("CDOEC", "unknown codec name. set default to GSM while you input (" + mStrCodec + ")");
                break;

        }
        Log.e(TAG, "Your codec set to :" + mStrCodec);


        //-----------------------------------------------
        // 현재 코덱이 무엇인지 띄워준다.
        //------------------------------------------------
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(AppContext.get().getApplicationContext(), mStrCodec, Toast.LENGTH_SHORT).show();
            }
        });

        audioGroupSendOnly = new AudioGroup();
        audioGroupReceiveOnly = new AudioGroup();

        audioStreamReceiveOnly.setMode(RtpStream.MODE_RECEIVE_ONLY);
        audioStreamReceiveOnly.setCodec(localAudioCodec);
        audioStreamReceiveOnly.associate(Helper.getTargetInetaddress(remoteIpAddress), remotePortOfRecv);


        audioStreamSendOnly.setMode(RtpStream.MODE_SEND_ONLY);
        audioStreamSendOnly.setCodec(localAudioCodec);
        audioStreamSendOnly.associate(Helper.getTargetInetaddress(remoteIpAddress), remotePortOfSend);


        if (isEchoSuppersserOn) {
            audioGroupSendOnly.setMode(AudioGroup.MODE_ECHO_SUPPRESSION);
            new Handler(Looper.getMainLooper()).
                    post(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(AppContext.get().getApplicationContext(), "ECHO SUPPRESS ON", Toast.LENGTH_SHORT).show();
                        }
                    });
        } else {
            audioGroupSendOnly.setMode(AudioGroup.MODE_NORMAL);

        }
        audioGroupReceiveOnly.setMode(AudioGroup.MODE_MUTED);


        audioStreamSendOnly.join(audioGroupSendOnly);
        audioStreamReceiveOnly.join(audioGroupReceiveOnly);

        Log.i("VoiceAndroidAdapterImpl", "audioStreamReceiveOnly associated with remote peer." + remoteIpAddress + ":" + remotePortOfSend);
    }

    public void setCodec(String codecName) {
        mStrCodec = codecName;
    }

    @Override
    public void setEchoCancelingOn(boolean bSet) {
        isEchoCancellerOn = bSet;
    }

    @Override
    public void setEchoSuppressorOn(boolean bSet) {
        isEchoSuppersserOn = bSet;
    }

    @Override
    public void setVADOn(boolean bSet) {
        isVADOn = bSet;

    }

    @Override
    public void setSecureStream(String secureProtocol) {
        mStrSecureKind = secureProtocol;
    }

    @Override
    public String[] getAvailableCodecs() {
        return new String[]{"GSM_EFR", "AMR", "PCMU", "PCMA", "GSM"};
    }

    @Override
    public String getCurrentCodec() {
        return mStrCodec;
    }
}
