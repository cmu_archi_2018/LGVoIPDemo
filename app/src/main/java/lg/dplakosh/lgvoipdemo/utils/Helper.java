package lg.dplakosh.lgvoipdemo.utils;

import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.security.NoSuchAlgorithmException;
import java.util.Locale;

import lg.dplakosh.lgvoipdemo.VoiceCallApp;

import static android.content.Context.WIFI_SERVICE;

public class Helper {

    public static String ipaddress="127.0.0.1";
    public static void setLocalIpAddress(String _ipaddress){
        Helper.ipaddress = _ipaddress;
    }
    public static InetAddress getLocalIpAddress() {
        InetAddress inetAddress = null;
        try {
            inetAddress = InetAddress.getByName(Helper.ipaddress);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return inetAddress;
    }

    public static InetAddress getTargetInetaddress(String target) {
        InetAddress targetInetAddress = null;
        try {

            Log.i("remoteaddress", "remote address:" + target);
            targetInetAddress = InetAddress.getByName(target.trim());
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return targetInetAddress;
    }

    /**
     *
     * @param text
     * @return
     * @throws NoSuchAlgorithmException
     */
    public static String SHA256 (String text) throws NoSuchAlgorithmException {

//        MessageDigest md = MessageDigest.getInstance("SHA-256");
//
//        md.update(text.getBytes());
//        byte[] digest = md.digest();
//
//        String ret =  Base64.encodeToString(digest, Base64.DEFAULT);
//        Log.i("sha256",String.format("(%s) to (%s)",text,ret));
//        return ret;
        return text;
    }

    public static String getLocalIP(Context context)
    {
        int LocalIpAddressBin = 0;
        WifiManager wifiManager = (WifiManager) VoiceCallApp.mContext.getApplicationContext().getSystemService(WIFI_SERVICE);
        if (wifiManager != null) {
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            LocalIpAddressBin = wifiInfo.getIpAddress();
            return  String.format(Locale.US, "%d.%d.%d.%d", (LocalIpAddressBin & 0xff), (LocalIpAddressBin >> 8 & 0xff), (LocalIpAddressBin >> 16 & 0xff), (LocalIpAddressBin >> 24 & 0xff));
        }
        return "NO ADDRESS";
    }
}
