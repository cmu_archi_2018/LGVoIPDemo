package lg.dplakosh.lgvoipdemo.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class Config {

    private static SharedPreferences sp;
    private static final String USER_PREF = "USER_PREF";
    private static final String KEY_NAME = "KEY_EMAIL";
    private static final String KEY_PASSWORD = "KEY_PASSWORD";
    private static final String KEY_TOKEN = "KEY_TOKEN";
    private static final String KEY_PHONENO = "KEY_PHONENO";
    private static final String KEY_LOGIN = "KEY_LOGIN";


    private static final String KEY_ADDRESS = "KEY_ADDRESS";
    private static final String KEY_CREDITCARD_NO = "KEY_CREDITCARD_NO";
    private static final String KEY_CREDITCARD_EXPIR_DATE = "KEY_CREDITCARD_EXPIR_DATE";
    private static final String KEY_CREDITCARD_CVC = "KEY_CREDITCARD_CVC";
    private static final String KEY_OPEN_API_SERVERURL = "KEY_OPEN_API_SERVERURL";

    public static final String STATUS_LOGIN = "login";
    public static final String STATUS_LOGOUT = "logout";
    public static final String STATUS_EXPIRED = "expired";

    public static void init(Context context) {
        sp = context.getSharedPreferences(USER_PREF, Context.MODE_PRIVATE);
    }

    private static String getValueOfKey(String key) {
        return sp.getString(key, "");
    }

    private static void saveKeyValue(String key, String value) {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static void setServerUrl(String string) {
        saveKeyValue(KEY_OPEN_API_SERVERURL, string);
    }

    public static String getServerUrl() {
        return getValueOfKey(KEY_OPEN_API_SERVERURL);
    }


    public static void setEmail(String string) {
        saveKeyValue(KEY_NAME, string);
    }

    public static String getEmail() {
        return getValueOfKey(KEY_NAME);
    }

    public static void setPassword(String string) {
        saveKeyValue(KEY_PASSWORD, string);
    }

    public static String getPassword() {
        return getValueOfKey(KEY_PASSWORD);
    }

    public static void setLoginStatus(String status) {
        saveKeyValue(KEY_LOGIN, status);
    }

    public static String getLoginStatus() {
        return getValueOfKey(KEY_LOGIN);
    }

    public static boolean isLogInStatus() {
        return getLoginStatus().equals(STATUS_LOGIN);
    }

    public static void setToken(String string) {
        saveKeyValue(KEY_TOKEN, string);
    }

    public static String getToken() {
        return sp.getString(KEY_TOKEN, STATUS_LOGOUT);
    }

    public static void setPhoneNo(String string) {
        saveKeyValue(KEY_PHONENO, string);
    }

    public static String getPhoneNo() {
        return getValueOfKey(KEY_PHONENO);
    }

    public static void setCreditcardNo(String string) {
        saveKeyValue(KEY_CREDITCARD_NO, string);
    }

    public static String getCreditcardNo() {
        return getValueOfKey(KEY_CREDITCARD_NO);
    }

    public static void setCreditcardExpirDate(String string) {
        saveKeyValue(KEY_CREDITCARD_EXPIR_DATE, string);
    }

    public static String getCreditcardExpirDate() {
        return getValueOfKey(KEY_CREDITCARD_EXPIR_DATE);
    }

    public static String getAddress() {
        return getValueOfKey(KEY_ADDRESS);
    }

    public static void setAddress(String string) {
        saveKeyValue(KEY_ADDRESS, string);
    }

    public static String getCVC() {
        return getValueOfKey(KEY_CREDITCARD_CVC);
    }

    public static void setCVC(String string) {
        saveKeyValue(KEY_CREDITCARD_CVC, string);
    }

    /**
     * 등록여부
     *
     * @return 등록이 되어있으면 true, 그렇지 않으면 false
     */
    public static boolean isRegistered() {
        //return (!getEmail().isEmpty() && !getPassword().isEmpty() && !getPhoneNo().isEmpty() && !getToken().isEmpty());
        return (!getPhoneNo().isEmpty());
    }
}

