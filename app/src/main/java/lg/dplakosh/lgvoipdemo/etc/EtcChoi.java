package lg.dplakosh.lgvoipdemo.etc;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EtcChoi {

    public static boolean checkEmail(String email){

        String regex = "^[_a-zA-Z0-9-\\.]+@[\\.a-zA-Z0-9-]+\\.[a-zA-Z]+$";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(email);
        return m.matches();

    }

    public static boolean checkPhoneNum(String phonenumber){
        String regex = "(^[0-9]*$)";
        //String regex = "[\\d]*";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(phonenumber);
        return m.matches();
    }
}