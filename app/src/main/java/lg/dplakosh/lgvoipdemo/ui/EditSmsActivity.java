package lg.dplakosh.lgvoipdemo.ui;

import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.PhoneNumberUtils;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import lg.dplakosh.lgvoipdemo.R;
import lg.dplakosh.lgvoipdemo.db.contact.Contact;
import lg.dplakosh.lgvoipdemo.db.contact.ContactHandler;
import lg.dplakosh.lgvoipdemo.utils.Logger;
import lg.dplakosh.lgvoipdemo.voicecall.callctrl.VoiceCallControl;

public class EditSmsActivity extends AppCompatActivity implements TextWatcher {

    private static String TAG = "voip|EditSmsActivity";
    private EditText editText;
    private TextView textCount;
    private EditText msgReceiver;

    private ContactHandler contactDB;
    private String mTargetNumber;
    private boolean nameConverted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        nameConverted = false;
        setContentView(R.layout.activity_edit_sms);

        Intent myIntent = getIntent(); // gets the previously created intent
        String str_Name = myIntent.getStringExtra("Name");
        String str_phone= myIntent.getStringExtra("Phone");
        String str_email= myIntent.getStringExtra("Email");

        msgReceiver = findViewById(R.id.msgReceiver);

        msgReceiver.setText(str_phone);

        msgReceiver.setOnFocusChangeListener((View v, boolean hasFocus) -> {
            Logger.d(TAG, "msgReceiver focus :" + hasFocus);
            if (!nameConverted && !hasFocus) {
                searchContactName(mTargetNumber);
            }
        });

        String number = PhoneNumberUtils.getNumberFromIntent(getIntent(), getApplicationContext());
        if (!TextUtils.isEmpty(number)) {
            mTargetNumber = number;
            if (!searchContactName(number)) {
                msgReceiver.setText(number);
            }
        }

        final ImageButton searchBtn = findViewById(R.id.search_btn);
        searchBtn.setOnClickListener((View v) -> {
            // TODO
            // search contact and change name if exist.
            Intent myintent = new Intent(getApplicationContext(), MainTabActivity.class);
            myintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(myintent);
            finish();
            Toast.makeText(this, "Show Contact List", Toast.LENGTH_SHORT).show();
        });

        editText = findViewById(R.id.editText);
        textCount = findViewById(R.id.textCount);
        textCount.setText(Integer.toString(1000));
        textCount.setTextColor(Color.BLACK);
        editText.addTextChangedListener(this);
        editText.setText("");

        Toolbar myToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        myToolbar.setTitleTextColor(Color.WHITE);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.title_edit_sms);
        }

        Button button_signup = findViewById(R.id.button_send_sms);
        button_signup.setOnClickListener(v -> {

            String phoneNumber = String.valueOf(msgReceiver.getText());
            String message =  String.valueOf(editText.getText());

            if (TextUtils.isEmpty(phoneNumber)) {
                msgReceiver.requestFocus();
                return;
            }
            if (TextUtils.isEmpty(message)) {
                editText.requestFocus();
                return;
            }

            mTargetNumber = phoneNumber;
            new SendToServer().execute(mTargetNumber);
        });
    }

    boolean searchContactName(String number) {

        if (TextUtils.isEmpty(number)) {
            Logger.d(TAG, "searchContactName number is empty");
            return false;
        }

        if (contactDB == null) {
            contactDB = new ContactHandler(this);
        }
        contactDB.getReadableDatabase();
        List<Contact> contacts = contactDB.getAllContacts();
        contactDB.getReadableDatabase();

        Logger.d(TAG, "searchContactName - " + number + ", contactDB count :" + contacts.size());

        for (Contact cn : contacts) {

            Logger.d(TAG, "  -> " + cn.getPhoneNumber() + ", " + cn.getName());

            if (cn.getPhoneNumber().equals(number) && !TextUtils.isEmpty(cn.getName())) {
                msgReceiver.setText(cn.getName());
                nameConverted = true;
                Logger.d(TAG, "searchContactName - found!!");
                return true;
            }
        }
        Logger.d(TAG, "searchContactName - not found !!");

        return false;
    }

    class SendToServer extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... phoneNumber) {
            String message =  String.valueOf(editText.getText());
            boolean result = VoiceCallControl.getInstance().sendTextMessage(phoneNumber[0], message);
            return result ? "Send OK" : "Seng Fail!";
        }

        @Override
        protected void onPostExecute(String result) {
            Toast.makeText(EditSmsActivity.this, result, Toast.LENGTH_LONG).show();

            // TODO - this should be called when receiving result from server
            msgReceiver.setText("");
            editText.setText("");

            moveTaskToBack(true);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        int count = 1000 - editText.length();
        textCount.setText(Integer.toString(count));
        textCount.setTextColor(Color.BLACK);
        if (count < 10)
            textCount.setTextColor(Color.YELLOW);
        if (count < 0)
            textCount.setTextColor(Color.RED);
    }
}
