package lg.dplakosh.lgvoipdemo.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import lg.dplakosh.lgvoipdemo.R;
import lg.dplakosh.lgvoipdemo.db.contact.Contact;
import lg.dplakosh.lgvoipdemo.db.contact.ContactHandler;
import lg.dplakosh.lgvoipdemo.etc.EtcChoi;


public class ModifyContactActivity extends Activity {




    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modifycontact);
        Intent myIntent = getIntent(); // gets the previously created intent

        Button Button_Act;
        Button_Act = (Button) findViewById(R.id.ModifyContact);

        String str_Name = myIntent.getStringExtra("Name");
        String str_phone= myIntent.getStringExtra("Phone");
        String str_email= myIntent.getStringExtra("Email");

        final EditText editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        final TextView textViewEmail = (TextView) findViewById(R.id.textViewEmail);

        final EditText editTextPhonenumber = (EditText) findViewById(R.id.editTextPhonenumber);
        final TextView textViewPhonenumber = (TextView) findViewById(R.id.textViewPhonenumber);

        final EditText editTextName = (EditText) findViewById(R.id.editTextName);
        final TextView textViewName = (TextView) findViewById(R.id.textViewName);

        editTextEmail.setText(str_email);
        editTextPhonenumber.setText(str_phone);
        editTextName.setText(str_Name);

        long findid;

        ContactHandler db = new ContactHandler(getApplicationContext());

        findid = db.getPrimaryid(str_Name,str_phone,str_email);

        //db.updateContact(new Contact(str_Name,str_phone,str_email));

        Button_Act.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {

                String textEmail = editTextEmail.getText().toString();
                String textPhonenumber = editTextPhonenumber.getText().toString();
                String textName = editTextName.getText().toString();
                if (EtcChoi.checkEmail(textEmail)&& EtcChoi.checkPhoneNum(textPhonenumber)) {
                    db.updateContact(new Contact((int) findid, textName, textPhonenumber, textEmail));

                    ContactFragment.allow=true;
                    Intent myintent = new Intent(ModifyContactActivity.this, MainTabActivity.class);
                    myintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(myintent);
                    finish();
                }
                else{
                    Toast.makeText(ModifyContactActivity.this, "Format was wrong!!! Please check your information", Toast.LENGTH_SHORT).show();
                }
            }
        });


        editTextEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = editTextEmail.getText().toString();
                int num = text.length();
                textViewEmail.setText(Integer.toString(num) + "/150");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editTextName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = editTextName.getText().toString();
                int num = text.length();
                textViewName.setText(Integer.toString(num) + "/150");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editTextPhonenumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = editTextPhonenumber.getText().toString();
                int num = text.length();
                textViewPhonenumber.setText(Integer.toString(num) + "/150");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }
}