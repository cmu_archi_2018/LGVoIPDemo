package lg.dplakosh.lgvoipdemo.ui;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.webkit.WebView;
import android.webkit.WebViewClient;


public class WebViewBillingClient extends WebViewClient {

    private Activity activity = null;

    public WebViewBillingClient(Activity activity) {
        this.activity = activity;
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView webView, String url) {
// 딴 URL 로 세는걸 막기 위한 것으로 보임. 불필요함.
//  if (url.indexOf("google.com") > -1) return false;

        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        activity.startActivity(intent);
        return true;
    }

}