package lg.dplakosh.lgvoipdemo.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import lg.dplakosh.lgvoipdemo.R;
import lg.dplakosh.lgvoipdemo.db.contact.Contact;
import lg.dplakosh.lgvoipdemo.list.smslist.SMSBookAdapter;
import lg.dplakosh.lgvoipdemo.db.sms.SMSSQLiteDatabaseHelper;
import lg.dplakosh.lgvoipdemo.db.sms.SMS;
import lg.dplakosh.lgvoipdemo.list.smslist.SMSBook;

public class SMSFragment extends Fragment {

    private ListView lvSms;

    public static boolean allow = true;

    //Overriden method onCreateView
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Returning the layout file after inflating
        //Change R.layout.tab1 in you classes

        super.onCreate(savedInstanceState);

        View v = inflater.inflate(R.layout.fragment_sms, container, false);
        lvSms = (ListView) v.findViewById(R.id.listSms);

        SMSSQLiteDatabaseHelper message_db = new SMSSQLiteDatabaseHelper(getContext());
        //message_db.deleteDatabase();

        //message_db.addSMS(new SMS("2018-06-12T22:06:54.840Z","123-456-7890","dummy message","readorno"));

        message_db.getReadableDatabase();
        List<SMS> contacts = message_db.getAllSMSs();
        message_db.getReadableDatabase();

        List<SMSBook> listSMSBook = new ArrayList<>();

        for (SMS cn : contacts) {

            String date = cn.get_date();
            String phonenum = cn.get_phone_number();
            String message = cn.get_message();
            String readorno = cn.get_readornot();
            listSMSBook.add(new SMSBook(
                    BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher_background),
                    date, phonenum, message, readorno));
        }

        SMSBookAdapter adapter = new SMSBookAdapter(getContext(), listSMSBook);
        lvSms.setAdapter(adapter);

        lvSms.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //String item = (String) parent.getItemAtPosition(position);
                String item = ((TextView) view).getText().toString();
                //Toast.makeText(getActivity(), item, Toast.LENGTH_LONG).show();
                Intent intent1 = new Intent(getActivity(), SelectPhoneNumberActivity.class);
                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent1);
            }
        });

        message_db.close();

        //return lvSms;

        v.findViewById(R.id.ClearAllSms).setOnClickListener(new Button.OnClickListener(){
            public void onClick(View v) {

                SMSFragment.allow=true;
                AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                alert.setTitle("Alert!!");
                alert.setMessage("Are you sure to delete all sms?");
                alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //do your work here
                        SMSSQLiteDatabaseHelper db = new SMSSQLiteDatabaseHelper(getContext());
                        db.deleteDatabase();
                        db.close();
                        refreshFragment();
                        Toast.makeText(getActivity(), "Deleting All Sms Is Success!!! ", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                });
                alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });

                alert.show();
            }
        });

        return v;
    }

    //https://stackoverflow.com/questions/20702333/refresh-fragment-at-reload
    private void refreshFragment() {
        if(this.allow==true) {
            this.allow = false;
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.detach(this).attach(this).commit();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {

        super.setUserVisibleHint(
                isVisibleToUser);

        // Refresh tab data:

        if (getFragmentManager() != null) {

            getFragmentManager()
                    .beginTransaction()
                    .detach(this)
                    .attach(this)
                    .commit();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshFragment();
    }


    @Override
    public void onPause() {
        super.onPause();
        refreshFragment();
    }

}

