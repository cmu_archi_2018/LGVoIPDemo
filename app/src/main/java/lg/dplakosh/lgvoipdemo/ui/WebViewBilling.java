package lg.dplakosh.lgvoipdemo.ui;


import android.app.Activity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

import java.io.IOException;

import lg.dplakosh.lgvoipdemo.R;
import lg.dplakosh.lgvoipdemo.utils.Logger;


public class WebViewBilling extends Activity {

    private final static String TAG = WebViewBilling.class.getSimpleName();
    private WebView webView = null;


    private boolean executeCommand(String ipaddress) {
        System.out.println("executeCommand");
        Runtime runtime = Runtime.getRuntime();
        try {
            Process mIpAddrProcess = runtime.exec("/system/bin/ping -c 1 " + ipaddress);
            int mExitValue = mIpAddrProcess.waitFor();
            System.out.println(" mExitValue " + mExitValue);
            return mExitValue == 0;
        } catch (InterruptedException ignore) {
            ignore.printStackTrace();
            System.out.println(" Exception:" + ignore);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println(" Exception:" + e);
        }
        return false;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview_billing);

        this.webView = findViewById(R.id.webview);

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        WebViewBillingClient webViewClient = new WebViewBillingClient(this);
        webView.setWebViewClient(webViewClient);


//        webView.getSettings().setAppCacheMaxSize(5 * 1024 * 1024); // 5MB
        webView.getSettings().setAppCachePath(getApplicationContext().getCacheDir().getAbsolutePath());
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setAppCacheEnabled(true);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT); // load online by default

        //TODO 로컬호스트 주소로 바꾸자.
        if (executeCommand("8.8.8.8") || executeCommand("10.0.3.2") || executeCommand("10.0.1.2")  ) {
            Logger.e(TAG, "internet ok!");
        } else {
            webView.getSettings().setCacheMode(WebSettings.LOAD_CACHE_ELSE_NETWORK);
            Logger.e(TAG, "seems no internet connection! Using cache!");
            Toast.makeText(this, "Server connection failed! Previous billing information is now displaying. Please check connection status!", Toast.LENGTH_SHORT).show();
        }
//        webView.loadUrl("https://www.google.com");
        String strUrl = "http://10.0.3.2:3000";
        webView.loadUrl(strUrl);
        Toast.makeText(this, strUrl, Toast.LENGTH_SHORT).show();


    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if ((keyCode == KeyEvent.KEYCODE_BACK) && this.webView.canGoBack()) {
            this.webView.goBack();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}