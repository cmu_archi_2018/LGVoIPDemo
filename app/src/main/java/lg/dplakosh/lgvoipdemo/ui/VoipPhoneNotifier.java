package lg.dplakosh.lgvoipdemo.ui;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import lg.dplakosh.lgvoipdemo.R;
import lg.dplakosh.lgvoipdemo.utils.Config;
import lg.dplakosh.lgvoipdemo.utils.Logger;

public class VoipPhoneNotifier {
    private static final String TAG = "voip|VoipPhoneNotifier";

    private static final String ACTION_START_UI = "lg.dplakosh.lgvoipdemo.ACTION_START_UI";
    private static final String ACTION_LOGOUT = "lg.dplakosh.lgvoipdemo.ACTION_NOTIFY_LOGOUT";

    private static final int VOIPCALL_NOTIFICATION_INDEX = 300;

    private static VoipPhoneNotifier sInstance = null;

    private Context mContext;
    private NotificationManager mNotificationManager;
    private boolean mBackEndRegistered = true;

    public static void init(Context context) {
        if (sInstance == null) {
            sInstance = new VoipPhoneNotifier(context);
        }
    }

    public static VoipPhoneNotifier getInstance() {
        return sInstance;
    }

    private VoipPhoneNotifier(Context context) {
        mContext = context;
        mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);
        registerForBackEndStatusChange();
    }

    public void destroy() {
        sInstance = null;
        unregisterForBackEndStatusChange();
    }

    public synchronized void updateVoipPhoneNotification() {

        if (!Config.isLogInStatus()) {
            mNotificationManager.cancel(VOIPCALL_NOTIFICATION_INDEX);
            Logger.d(TAG, "updateVoipPhoneNotification() - cancel");

        } else {
            Notification notification = makeNotification();
            Logger.d(TAG, "updateVoipPhoneNotification() - notify");

            if (notification != null) {
                mNotificationManager.notify(VOIPCALL_NOTIFICATION_INDEX, notification);
            }
        }
    }

    private Notification makeNotification() {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(mContext, "voip_ui");

        builder.setCategory(Notification.CATEGORY_MESSAGE);
        builder.setSmallIcon(R.drawable.tream4_logo);
        builder.setAutoCancel(false);
        builder.setContentIntent(createSoftPhoneSettingIntent());
        builder.setOngoing(true);
        builder.setShowWhen(false);

        if (mBackEndRegistered) {
            builder.setPriority( NotificationManager.IMPORTANCE_HIGH);
            builder.setContentTitle(mContext.getText(R.string.voipdemo_noti_panel_title));
        } else {
            builder.setPriority( NotificationManager.IMPORTANCE_MIN);
            builder.setContentTitle(mContext.getText(R.string.voipdemo_noti_panel_call_unavailable));
        }

        builder.setContentText(getContextText());

        builder.addAction(R.drawable.noti_panel_logout,
                mContext.getText(R.string.btn_logout), createLogOutIntent());

        return builder.build();
    }

    private String getContextText() {
        return Config.getPhoneNo() + ", " + Config.getEmail();
    }

    private PendingIntent createSoftPhoneSettingIntent() {
        Intent intent = new Intent(ACTION_START_UI);
        return PendingIntent.getBroadcast(mContext, 0, intent, 0);
    }

    private PendingIntent createLogOutIntent() {
        Intent intent = new Intent(ACTION_LOGOUT);
        return PendingIntent.getBroadcast(mContext, 0, intent, 0);
    }

    private void registerForBackEndStatusChange() {
    }

    private void unregisterForBackEndStatusChange() {
    }

    private void backendKeepAliveStatusChanged() {
        if (mBackEndRegistered != checkBackEndKeepAliveStatus()) {
            mBackEndRegistered = !mBackEndRegistered;
            updateVoipPhoneNotification();
        }
    }

    private boolean checkBackEndKeepAliveStatus() {
        return true;
    }
}
