package lg.dplakosh.lgvoipdemo.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import lg.dplakosh.lgvoipdemo.R;
import lg.dplakosh.lgvoipdemo.db.contact.Contact;
import lg.dplakosh.lgvoipdemo.utils.Config;
import lg.dplakosh.lgvoipdemo.db.contact.ContactHandler;
import lg.dplakosh.lgvoipdemo.list.phone.PhoneBook;
import lg.dplakosh.lgvoipdemo.list.phone.PhoneBookAdapter;

public class ContactFragment extends Fragment {

    private ListView lvPhone;

    public static boolean allow = true;

    //Overriden method onCreateView
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Returning the layout file after inflating
        //Change R.layout.tab1 in you classes

        super.onCreate(savedInstanceState);

        View v = inflater.inflate(R.layout.fragment_contact, container, false);
        lvPhone = (ListView) v.findViewById(R.id.listPhone);

        ContactHandler db = new ContactHandler(getContext());

        //db.getAllContacts();

        //db.deleteDatabase();

        // Inserting Contacts
        //db.addContact(new Contact("Alex Park", "234-567-8901", "abc@def.net"));

        db.getReadableDatabase();
        List<Contact> contacts = db.getAllContacts();
        db.getReadableDatabase();

        List<PhoneBook> listPhoneBook = new ArrayList<>();

        listPhoneBook.add(new PhoneBook(
                BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher_background),
                "My Info", Config.getPhoneNo(), Config.getEmail()));

        for (Contact cn : contacts) {

            String name = cn.getName();
            String phonenum = cn.getPhoneNumber();
            String email = cn.getEmail();
            listPhoneBook.add(new PhoneBook(
                    BitmapFactory.decodeResource(getResources(), R.drawable.ic_launcher_background),
                    name ,phonenum ,email ));
        }

        PhoneBookAdapter adapter = new PhoneBookAdapter(getContext(), listPhoneBook);
        lvPhone.setAdapter(adapter);

//        db.close();

        v.findViewById(R.id.AddContact).setOnClickListener(new Button.OnClickListener(){
            public void onClick(View v) {
                Intent myintent = new Intent(getActivity(), AddContactActivity.class);
                myintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(myintent);
            }
        });

        v.findViewById(R.id.ClearAllContact).setOnClickListener(new Button.OnClickListener(){
            public void onClick(View v) {
                ContactFragment.allow=true;
                AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                alert.setTitle("Alert!!");
                alert.setMessage("Are you sure to delete all contacts?");
                alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //do your work here
                        ContactHandler db = new ContactHandler(getContext());
                        db.deleteDatabase();
                        db.close();
                        refreshFragment();
                        Toast.makeText(getActivity(), "Deleting All Contact Is Success!!! ", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();

                    }
                });
                alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });

                alert.show();
            }
        });

        //return lvPhone;

        return v;

    }

    //https://stackoverflow.com/questions/20702333/refresh-fragment-at-reload
    private void refreshFragment() {
        if(this.allow==true) {
            this.allow=false;
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            ft.detach(this).attach(this).commit();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {

        super.setUserVisibleHint(
                isVisibleToUser);

        // Refresh tab data:

        if (getFragmentManager() != null) {

            getFragmentManager()
                    .beginTransaction()
                    .detach(this)
                    .attach(this)
                    .commit();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        refreshFragment();
    }


    @Override
    public void onPause() {
        super.onPause();
        refreshFragment();
    }

}
