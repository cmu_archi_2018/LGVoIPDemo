package lg.dplakosh.lgvoipdemo.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Vector;

import lg.dplakosh.lgvoipdemo.R;
import lg.dplakosh.lgvoipdemo.etc.EtcChoi;
import lg.dplakosh.lgvoipdemo.net.AsyncResponse;
import lg.dplakosh.lgvoipdemo.net.HttpUtils;
import lg.dplakosh.lgvoipdemo.utils.Config;
import lg.dplakosh.lgvoipdemo.etc.AsteriskPasswordTransformationMethod;

public class RegistrationActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        final EditText editTextEmail = findViewById(R.id.editTextEmail);
        final TextView textViewEmail = findViewById(R.id.textViewEmail);
        final EditText editTextPasswords = findViewById(R.id.editTextPasswords);
        final TextView textViewPasswords = findViewById(R.id.textViewPasswords);
        final EditText editTextAddress = findViewById(R.id.editTextAddress);
        final TextView textViewAddress = findViewById(R.id.textViewAddress);
        final EditText editTextCardNum = findViewById(R.id.editTextCardNum);
        final TextView textViewCardNum = findViewById(R.id.textViewCardNum);
        final EditText editTextDay = findViewById(R.id.editTextDay);
        final TextView textViewDay = findViewById(R.id.textViewDay);
        final EditText editTextCVC = findViewById(R.id.editTextCVC);
        final TextView textViewCVC = findViewById(R.id.textViewCVC);

        //TextView testView_Act1;
        Button Button_Act1;
        //testView_Act1 = (TextView) findViewById(R.id.textViewActivity1);
        Button_Act1 = findViewById(R.id.buttonActivity1);
        Button_Act1.setOnClickListener(new Button.OnClickListener() {
            @Override
               public void onClick(View v) {
                final String textEmail = editTextEmail.getText().toString();
                final String textPasswords = editTextPasswords.getText().toString();
                String textAddress = editTextAddress.getText().toString();
                final String textCardNum = editTextCardNum.getText().toString();
                final String textDay = editTextDay.getText().toString();
                String textCVC = editTextCVC.getText().toString();
                if (EtcChoi.checkEmail(textEmail)) {
                    String string = HttpUtils.requestRegister(textEmail, textPasswords, textAddress, textCardNum, textDay, textCVC, RegistrationActivity.this, new AsyncResponse() {
                        @Override
                        public void processFinish(String rawFormat, boolean result, Vector<String> values) {
                            if (result) {
                                Config.setPhoneNo(values.elementAt(0));
                                Config.setEmail(textEmail);
                                Config.setPassword(textPasswords);
                                Config.setCreditcardNo(textCardNum);
                                Config.setCreditcardExpirDate(textDay);
                                Config.setCVC(textCVC);

                                //일단은 다음창으로 넘어간다
                                Toast.makeText(RegistrationActivity.this, "Regietration Success!!!", Toast.LENGTH_SHORT).show();
                                Intent intent1 = new Intent(RegistrationActivity.this, LoginActivity.class);
                                intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent1);
                                finish();
                            } else {
                                Toast.makeText(RegistrationActivity.this, "Regietration Fail!!!" + values, Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
                else{
                    Toast.makeText(RegistrationActivity.this, "Format was wrong!!! Please check your information", Toast.LENGTH_SHORT).show();
                }
            }
           }
        );

        editTextEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = editTextEmail.getText().toString();
                int num = text.length();
                textViewEmail.setText(Integer.toString(num) + "/150");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editTextPasswords.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = editTextPasswords.getText().toString();
                int num = text.length();
                textViewPasswords.setText(Integer.toString(num) + "/150");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editTextAddress.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = editTextAddress.getText().toString();
                int num = text.length();
                textViewAddress.setText(Integer.toString(num) + "/150");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editTextCardNum.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = editTextCardNum.getText().toString();
                int num = text.length();
                textViewCardNum.setText(Integer.toString(num) + "/150");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editTextDay.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = editTextDay.getText().toString();
                int num = text.length();
                textViewDay.setText(Integer.toString(num) + "/150");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editTextCVC.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = editTextCVC.getText().toString();
                int num = text.length();
                textViewCVC.setText(Integer.toString(num) + "/150");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        //패스워드 별표
        editTextCVC.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        editTextPasswords.setTransformationMethod(new AsteriskPasswordTransformationMethod());

//        if( config.isRegistered() )
//        {
//            //goto login page automatically,
//
//        }
    }
}