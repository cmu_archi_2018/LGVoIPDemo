package lg.dplakosh.lgvoipdemo.ui;


import android.Manifest;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import lg.dplakosh.lgvoipdemo.etc.AsteriskPasswordTransformationMethod;
import lg.dplakosh.lgvoipdemo.etc.EtcChoi;

import java.util.Vector;

import lg.dplakosh.lgvoipdemo.R;
import lg.dplakosh.lgvoipdemo.net.AsyncResponse;
import lg.dplakosh.lgvoipdemo.net.HttpUtils;
import lg.dplakosh.lgvoipdemo.test.TestMainActivity;
import lg.dplakosh.lgvoipdemo.utils.Config;
import lg.dplakosh.lgvoipdemo.utils.Helper;
import lg.dplakosh.lgvoipdemo.voicecall.callctrl.VoiceCallControl;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = LoginActivity.class.getSimpleName();

    // Requesting permission to RECORD_AUDIO
    private boolean permissionToRecordAccepted = false;
    private final String[] permissions = {Manifest.permission.RECORD_AUDIO};
    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_RECORD_AUDIO_PERMISSION:
                if (grantResults.length > 0 && permissions.length == grantResults.length)
                    permissionToRecordAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                Log.e(TAG, "Request for Permission To Record Audio Granted");
                break;
        }
        if (!permissionToRecordAccepted) {
            Log.e(TAG, "Request for Permission To Record Audio Not Granted");
            finish();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        onFirstExecution();

        final EditText editTextEmail = findViewById(R.id.editTextEmail);
        final TextView textViewEmail = findViewById(R.id.textViewEmail);

        final EditText editTextPasswords = findViewById(R.id.editTextPasswords);
        final TextView textViewPasswords = findViewById(R.id.textViewPasswords);

        ((TextView) findViewById(R.id.textViewMyPhoneNo)).setText(Config.getPhoneNo());
        ((TextView) findViewById(R.id.textViewMyIP)).setText(Helper.getLocalIP(getApplicationContext()));

        if (Config.isLogInStatus()) {


            HttpUtils.sendHeartBeatAndGetMessage(Config.getToken(), Config.getPhoneNo(), null, new AsyncResponse() {
                @Override
                public void processFinish(String rawFormat, boolean result, Vector<String> values) {
                    if (result == false) {
                        Toast.makeText(LoginActivity.this, "Failed to send heartbeat. Please login again", Toast.LENGTH_SHORT).show();
                    } else {
                        startMainActivity();
                        overridePendingTransition(0, 0);
                        finish();
                    }
                }
            });


            return;
        }

//        이미 저장된 계정정보가 있으면 세팅한다.
        if (!Config.getEmail().isEmpty()) {
            editTextEmail.setText(Config.getEmail());
        }
        if (!Config.getPassword().isEmpty()) {
            editTextPasswords.setText(Config.getPassword());
        }


        findViewById(R.id.LogIn).setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                String textEmail = editTextEmail.getText().toString();
                String textPasswords = editTextPasswords.getText().toString();
                if (EtcChoi.checkEmail(textEmail)) {
                    String string = HttpUtils.requestLogin(textEmail, textPasswords, LoginActivity.this, new AsyncResponse() {//requestRegister(textEmail, textPasswords, textAddress, textCardNum, textDay, textCVC, new AsyncResponse() {
                        @Override
                        public void processFinish(String rawFormat, boolean result, Vector<String> values) {

//                            Toast.makeText(LoginActivity.this, rawFormat, Toast.LENGTH_LONG).show();
                            Log.d(TAG, rawFormat);
                            if (result) {
                                Config.setPhoneNo(values.elementAt(1));
                                Config.setToken(values.elementAt(0));
                                Config.setEmail(textEmail);
                                Config.setPassword(textPasswords);

                                Log.i(TAG, Config.getToken());
                                Toast.makeText(LoginActivity.this, "Login Success!!!", Toast.LENGTH_SHORT).show();
                                //heartbeat with ip update
                                HttpUtils.sendHeartBeatAndGetMessage(Config.getToken(), Config.getPhoneNo(), LoginActivity.this, new AsyncResponse() {
                                    @Override
                                    public void processFinish(String rawFormat, boolean result, Vector<String> values) {
                                        if (result == false) {
                                            Toast.makeText(LoginActivity.this, "Failed to send heartbeat", Toast.LENGTH_SHORT).show();
                                        } else {
                                            Config.setLoginStatus(Config.STATUS_LOGIN);
                                            VoipPhoneNotifier.getInstance().updateVoipPhoneNotification();
                                            startMainActivity();
                                            finish();
                                        }
                                    }
                                });
                            } else {
                                Config.setLoginStatus(Config.STATUS_LOGOUT);
                                VoipPhoneNotifier.getInstance().updateVoipPhoneNotification();

                                VoiceCallControl.getInstance().setLoginResult(false, "", "");
                                Toast.makeText(LoginActivity.this, "Login Fail!!!" + values.elementAt(0), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                } else {
                    Toast.makeText(LoginActivity.this, "Format was wrong!!! Please check your information", Toast.LENGTH_SHORT).show();
                }
            }
        });

        findViewById(R.id.GotoSignUp).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(LoginActivity.this, RegistrationActivity.class);
                startActivity(intent1);
            }
        });

        findViewById(R.id.button_skip_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(LoginActivity.this, MainTabActivity.class);
                startActivity(intent1);
            }
        });


        findViewById(R.id.button_recover_password).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this, RequestToRecoveryPasswordActivity.class));
            }
        });

        editTextEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = editTextEmail.getText().toString();
                int num = text.length();
                textViewEmail.setText(Integer.toString(num) + "/150");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editTextPasswords.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = editTextPasswords.getText().toString();
                int num = text.length();
                textViewPasswords.setText(Integer.toString(num) + "/150");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editTextPasswords.setTransformationMethod(new AsteriskPasswordTransformationMethod());


        findViewById(R.id.button_goto_setting).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(LoginActivity.this, TestMainActivity.class);
                startActivity(intent1);
            }
        });
    }

    private void startMainActivity() {
        VoiceCallControl.getInstance().setLoginResult(true, Config.getPhoneNo(), Config.getToken());
        Intent intent1 = new Intent(LoginActivity.this, MainTabActivity.class);
        intent1.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent1);
    }

    private void onFirstExecution() {
        //------------------------------------------------
        //  녹음기능에 대한 권한을 얻는다.
        //------------------------------------------------
        if (checkSelfPermission(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {
            permissionToRecordAccepted = true;
            Log.e(TAG, "Permission To Record Audio Granted");
        } else {
            ActivityCompat.requestPermissions(this, permissions, REQUEST_RECORD_AUDIO_PERMISSION);

            //also request backend permission
            Intent intent = LoginActivity.this.getPackageManager().getLaunchIntentForPackage("lg.dplakosh.lgvoipdemo.backend");
            startActivity(intent);
        }
    }
}
