package lg.dplakosh.lgvoipdemo.ui;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import lg.dplakosh.lgvoipdemo.R;
import lg.dplakosh.lgvoipdemo.utils.Config;

public class SettingsActivity extends AppCompatActivity {

    private static final String TAG = SettingsActivity.class.getSimpleName();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_settings);

        Toolbar myToolbar = findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        myToolbar.setTitleTextColor(Color.WHITE);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(R.string.title_setting);
        }

        ListView listView = findViewById(R.id.listSettings);
        TextView textView = findViewById(R.id.textView);
        String[] listItem = getResources().getStringArray(R.array.array_technology);

        final ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, listItem);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                // TODO Auto-generated method stub
                String value = adapter.getItem(position);

                //<item>Modify My Info</item>
                //<item>Request To Recovery Password </item>
                //<item>Request To Modify Password </item>

                switch (value) {
                    case "Modify My Info": {
                        Intent intent = new Intent(getApplicationContext(), ChangeUserInfoActivity.class);
                        startActivity(intent);
                    }
                    break;
                    case "Request To Modify Password": {
                        Intent intent = new Intent(getApplicationContext(), ChangePasswordActivity.class);
                        startActivity(intent);
                    }
                    break;
                    case "Billing Info": {
                        Intent intent = new Intent(getApplicationContext(), WebViewBilling.class);
                        startActivity(intent);
                    }
                    break;
                    //https://stackoverflow.com/questions/2209513/how-to-start-activity-in-another-application
                    case "Call Setting":{
                        Intent intent = getApplicationContext().getPackageManager().getLaunchIntentForPackage("lg.dplakosh.lgvoipdemo.backend");
                        startActivity(intent);
                    }
                    case "Log out":{
                        Config.setLoginStatus(Config.STATUS_LOGOUT);
                        VoipPhoneNotifier.getInstance().updateVoipPhoneNotification();
                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                    }
                    default:
                        //when login, this msg happend. so I remove toast.
                        Log.w(TAG, "exception case!");
                        break;
                }
            }
        });
    }
}
