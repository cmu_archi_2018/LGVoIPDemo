package lg.dplakosh.lgvoipdemo.ui;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import lg.dplakosh.lgvoipdemo.R;
import lg.dplakosh.lgvoipdemo.voicecall.VoiceCallActivity;

public class DialFragment extends Fragment {

    private EditText EditText_number;
    private SharedPreferences mPref;

    private static String mPrevVoiceCallNumber = "";
    private static String mPrevSendTextNumber = "";

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mPref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mPrevVoiceCallNumber = mPref.getString(PreferenceConstants.KEY_VOICECALL, "");
        mPrevSendTextNumber = mPref.getString(PreferenceConstants.KEY_SENDTEXT, "");

        View v = inflater.inflate(R.layout.fragment_dialer, container, false);

        v.findViewById(R.id.one).setOnClickListener(mClickListener);
        v.findViewById(R.id.two).setOnClickListener(mClickListener);
        v.findViewById(R.id.three).setOnClickListener(mClickListener);
        v.findViewById(R.id.four).setOnClickListener(mClickListener);
        v.findViewById(R.id.five).setOnClickListener(mClickListener);
        v.findViewById(R.id.six).setOnClickListener(mClickListener);
        v.findViewById(R.id.seven).setOnClickListener(mClickListener);
        v.findViewById(R.id.eight).setOnClickListener(mClickListener);
        v.findViewById(R.id.nine).setOnClickListener(mClickListener);
        v.findViewById(R.id.zero).setOnClickListener(mClickListener);
        v.findViewById(R.id.star).setOnClickListener(mClickListener);
        v.findViewById(R.id.pound).setOnClickListener(mClickListener);
        v.findViewById(R.id.btn_voicecall).setOnClickListener(mClickListener);
        v.findViewById(R.id.btn_sendtext).setOnClickListener(mClickListener);
        v.findViewById(R.id.btn_delete).setOnClickListener(mClickListener);

        v.findViewById(R.id.btn_voicecall).setOnLongClickListener(mLongClickListener);
        v.findViewById(R.id.btn_sendtext).setOnLongClickListener(mLongClickListener);
        v.findViewById(R.id.btn_delete).setOnLongClickListener(mLongClickListener);

        EditText_number = v.findViewById(R.id.edit_phonenumber);
        EditText_number.setInputType(0);
        EditText_number.setSingleLine(false);

        setHasOptionsMenu(true);

        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        boolean changed = false;
        SharedPreferences.Editor editor = mPref.edit();
        if (!TextUtils.isEmpty(mPrevVoiceCallNumber)) {
            editor.putString(PreferenceConstants.KEY_VOICECALL, mPrevVoiceCallNumber);
            changed = true;
        }
        if (!TextUtils.isEmpty(mPrevSendTextNumber)) {
            editor.putString(PreferenceConstants.KEY_SENDTEXT, mPrevSendTextNumber);
            changed = true;
        }
        if (changed) {
            editor.apply();
        }
    }

    private final Button.OnClickListener mClickListener = new Button.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.btn_voicecall:
                    if (TextUtils.isEmpty(getNumber())) {
                        EditText_number.setText(normalizeNumber(mPrevVoiceCallNumber));
                    } else {
                        mPrevVoiceCallNumber = getNumber();
                        EditText_number.setText("");

                        startVoiceCall(mPrevVoiceCallNumber);
                        saveLastTryCallNumberToPrefDB(PreferenceConstants.KEY_VOICECALL,
                                mPrevVoiceCallNumber);

                        // TODO
                    }
                    break;

                case R.id.btn_sendtext:
                    if (TextUtils.isEmpty(getNumber())) {
                        EditText_number.setText(normalizeNumber(mPrevSendTextNumber));
                    } else {
                        mPrevSendTextNumber = getNumber();
                        EditText_number.setText("");

                        sendTextMessage(mPrevSendTextNumber);
                        saveLastTryCallNumberToPrefDB(PreferenceConstants.KEY_SENDTEXT,
                                mPrevSendTextNumber);

                        // TODO
                    }
                    break;

                case R.id.btn_delete:
                    String number = getNumber();
                    if (TextUtils.isEmpty(number)) {
                        break;
                    }
                    int strLength = number.length();
                    if (strLength >= 2) {
                        number = number.substring(0, number.length() - 1);
                    } else {
                        number = "";
                    }
                    number = normalizeNumber(number);
                    EditText_number.setText(number);
                    break;

                case R.id.zero:
                case R.id.one:
                case R.id.two:
                case R.id.three:
                case R.id.four:
                case R.id.five:
                case R.id.six:
                case R.id.seven:
                case R.id.eight:
                case R.id.nine:
                case R.id.star:
                case R.id.pound:
                    keypad_click(v);
                    break;

                default:
                    break;
            }
        }
    };

    private void saveLastTryCallNumberToPrefDB(String key, String lastTryNumber) {
        if (!TextUtils.isEmpty(lastTryNumber)) {
            SharedPreferences.Editor editor = mPref.edit();
            editor.putString(key, lastTryNumber);
            editor.apply();
        }
    }

    private void startVoiceCall(String number) {
        Intent intent = new Intent(getActivity(), VoiceCallActivity.class);
        intent.setData(Uri.fromParts("tel", number, null));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void sendTextMessage(String number) {
        Toast.makeText(getActivity(),"Text!",Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(getActivity(), EditSmsActivity.class);
        intent.setData(Uri.fromParts("tel", number, null));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private String getNumber() {
        String phoneNumber = EditText_number.getText().toString();

        return removeHyphen(phoneNumber);
    }

    public static String removeHyphen(String phoneNumberHasHyphen) {
        if (TextUtils.isEmpty(phoneNumberHasHyphen)) {
            return null;
        }
        String[] strings = phoneNumberHasHyphen.split("-");
        StringBuilder phoneNumberHasNoHyphen_buf = new StringBuilder();
        for (String token : strings) {
            phoneNumberHasNoHyphen_buf.append(token);
        }

        return phoneNumberHasNoHyphen_buf.toString();
    }

    Button.OnLongClickListener mLongClickListener = new Button.OnLongClickListener() {
        @Override
        public boolean onLongClick(View v) {
            switch (v.getId()) {

                case R.id.btn_voicecall:
                    if (TextUtils.isEmpty(getNumber()) && !TextUtils.isEmpty(mPrevVoiceCallNumber)) {
                        EditText_number.setText("");
                        startVoiceCall(mPrevVoiceCallNumber);
                    } else {
                        Toast.makeText(getActivity(),"Please input number", Toast.LENGTH_SHORT).show();
                    }
                    break;

                case R.id.btn_sendtext:
                    if (TextUtils.isEmpty(getNumber()) && !TextUtils.isEmpty(mPrevSendTextNumber)) {
                        EditText_number.setText("");
                        sendTextMessage(mPrevSendTextNumber);
                    } else {
                        Toast.makeText(getActivity(),"Please input number", Toast.LENGTH_SHORT).show();
                    }
                    break;

                case R.id.btn_delete:
                    EditText_number.setText("");
                    break;

                default:
                    break;
            }
            return false;
        }
    };

    public void keypad_click(View v) {
        String input = "";
        switch (v.getId()) {
            case R.id.zero:
                input = "0";
                break;
            case R.id.one:
                input = "1";
                break;
            case R.id.two:
                input = "2";
                break;
            case R.id.three:
                input = "3";
                break;
            case R.id.four:
                input = "4";
                break;
            case R.id.five:
                input = "5";
                break;
            case R.id.six:
                input = "6";
                break;
            case R.id.seven:
                input = "7";
                break;
            case R.id.eight:
                input = "8";
                break;
            case R.id.nine:
                input = "9";
                break;
            case R.id.star:
                input = "*";
                break;
            case R.id.pound:
                input = "#";
                break;
        }

        String number = EditText_number.getText().toString() + input;
        EditText_number.setText(number);
    }

    public static String normalizeNumber(String number) {
        return PhoneNumberUtils.normalizeNumber(number);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.dialer_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.menu_setting: {
                Intent intent = new Intent(getActivity(), SettingsActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                return true;
            }

            case R.id.menu_conference: {
                Intent intent = new Intent(getActivity(), ConferenceActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                return true;
            }

            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
