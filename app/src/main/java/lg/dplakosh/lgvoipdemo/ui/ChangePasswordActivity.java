package lg.dplakosh.lgvoipdemo.ui;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Vector;

import lg.dplakosh.lgvoipdemo.R;
import lg.dplakosh.lgvoipdemo.etc.AsteriskPasswordTransformationMethod;
import lg.dplakosh.lgvoipdemo.net.AsyncResponse;
import lg.dplakosh.lgvoipdemo.net.HttpUtils;
import lg.dplakosh.lgvoipdemo.utils.Config;

public class ChangePasswordActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_activity_change_password);

        // Example of a call to a native method
        final EditText editText_oldPassword = findViewById(R.id.editText_oldPassword);
        final EditText editText_newPassword = findViewById(R.id.editText_newPassword);
        final EditText editText_confirm_newPassword= findViewById(R.id.editText_confirm_newPassword);

        final TextView textView_oldPassword = findViewById(R.id.textView_oldPassword);
        final TextView textView_newPassword = findViewById(R.id.textView_newPassword);
        final TextView textView_confirm_newPassword= findViewById(R.id.textView_confirm_newPassword);

        Button button_login = findViewById(R.id.button_confirm_change_password);
        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String oldPassword = editText_oldPassword.getText().toString();
                final String newPassword = editText_newPassword.getText().toString();
                final String confirmPassword = editText_confirm_newPassword.getText().toString();


                if(!newPassword.equals(confirmPassword))
                {
                    Toast.makeText(ChangePasswordActivity.this, "Confirm your new password again.", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (oldPassword.isEmpty() || newPassword.isEmpty()) {
                    Toast.makeText(ChangePasswordActivity.this, "invalid password", Toast.LENGTH_SHORT).show();
                    return;
                }

                String string = HttpUtils.changeUserPassword(Config.getToken(), Config.getEmail(), oldPassword, newPassword, ChangePasswordActivity.this, new AsyncResponse() {
                    @Override
                    public void processFinish(String rawFormat, boolean result, Vector<String> values) {
                        Toast.makeText(ChangePasswordActivity.this, rawFormat, Toast.LENGTH_SHORT).show();

                        if (result) {
                            //성공시 새로운 패스워드를 저장한다.
                            Config.setPassword(newPassword);
//                            Intent intent = new Intent(ChangePasswordActivity.this, TestDialerActivity.class);
//                            startActivity(intent);
                            finish(); //이전 액티비티로 돌아간다. //https://stackoverflow.com/questions/4038479/android-go-back-to-previous-activity
                        } else {
                            //로그인실패할경우
                        }
                    }
                });
            }
        });

        editText_oldPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = editText_oldPassword.getText().toString();
                int num = text.length();
                textView_oldPassword.setText(Integer.toString(num) + "/150");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editText_newPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = editText_newPassword.getText().toString();
                int num = text.length();
                textView_newPassword.setText(Integer.toString(num) + "/150");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editText_confirm_newPassword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = editText_confirm_newPassword.getText().toString();
                int num = text.length();
                textView_confirm_newPassword.setText(Integer.toString(num) + "/150");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editText_oldPassword.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        editText_newPassword.setTransformationMethod(new AsteriskPasswordTransformationMethod());
        editText_confirm_newPassword.setTransformationMethod(new AsteriskPasswordTransformationMethod());
    }
}