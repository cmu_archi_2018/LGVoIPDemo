package lg.dplakosh.lgvoipdemo.ui;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;

import lg.dplakosh.lgvoipdemo.R;
import lg.dplakosh.lgvoipdemo.db.contact.ContactHandler;
import lg.dplakosh.lgvoipdemo.db.sms.SMSSQLiteDatabaseHelper;

public class SelectPhoneNumberActivity extends Activity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phoneselect);

        Button Button_Act;
        Button_Act = findViewById(R.id.selectphonenumberactivity);

        Button_Act.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {
                ContactHandler db = new ContactHandler(getApplicationContext());
                db.deleteDatabase();

                SMSSQLiteDatabaseHelper message_db = new SMSSQLiteDatabaseHelper(getApplicationContext());
                message_db.deleteDatabase();

                db.close();
                message_db.close();
            }
        });
    }
}
