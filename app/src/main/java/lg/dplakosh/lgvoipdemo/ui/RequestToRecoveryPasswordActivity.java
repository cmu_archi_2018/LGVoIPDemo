package lg.dplakosh.lgvoipdemo.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Vector;

import lg.dplakosh.lgvoipdemo.R;
import lg.dplakosh.lgvoipdemo.net.AsyncResponse;
import lg.dplakosh.lgvoipdemo.net.HttpUtils;
import lg.dplakosh.lgvoipdemo.utils.Config;

public class RequestToRecoveryPasswordActivity extends AppCompatActivity {


    private static final String LOG_TAG = LoginActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requesttorecoverypassword);

        final EditText editTextEmail = findViewById(R.id.editTextEmail);
        final TextView textViewEmail = findViewById(R.id.textViewEmail);


        if (!Config.getEmail().isEmpty()) {
            editTextEmail.setText(Config.getEmail());
        }

        findViewById(R.id.RequestToRecoveryPassword).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String textEmail = editTextEmail.getText().toString();

                Toast.makeText(RequestToRecoveryPasswordActivity.this, "PASS", Toast.LENGTH_LONG).show();

                HttpUtils.recoverPassword(textEmail, RequestToRecoveryPasswordActivity.this, new AsyncResponse() {
                    @Override
                    public void processFinish(String rawFormat, boolean result, Vector<String> values) {
                        if (result) {
                            Toast.makeText(RequestToRecoveryPasswordActivity.this, "Check your email!", Toast.LENGTH_SHORT).show();
                            //email 클라이언트를 실행한다.
//                            Intent intent = new Intent(Intent.ACTION_VIEW);
//                            Uri data = Uri.parse("mailto:" + textEmail + "?subject=" + "-" + "&body=" + "-");
//                            intent.setData(data);
//                            startActivity(intent);
                            Intent intent2 = Intent.makeMainSelectorActivity(Intent.ACTION_MAIN, Intent.CATEGORY_APP_EMAIL);
                            intent2.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent2);
                            finish();

                        } else {
                            Toast.makeText(RequestToRecoveryPasswordActivity.this, rawFormat, Toast.LENGTH_SHORT).show();
                        }
                    }
                });

            }
        });

        editTextEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = editTextEmail.getText().toString();
                int num = text.length();
                textViewEmail.setText(Integer.toString(num) + "/150");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}
