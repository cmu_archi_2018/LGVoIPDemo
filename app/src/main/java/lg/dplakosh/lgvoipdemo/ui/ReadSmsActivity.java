package lg.dplakosh.lgvoipdemo.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import lg.dplakosh.lgvoipdemo.R;

public class ReadSmsActivity extends Activity {
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_readsms);

        Intent myIntent = getIntent(); // gets the previously created intent

        String str_date = myIntent.getStringExtra("Date");
        String str_phone= myIntent.getStringExtra("Phone");
        String str_message= myIntent.getStringExtra("Message");
        String str_readornot= myIntent.getStringExtra("Readornot");

        final TextView textViewDate = findViewById(R.id.textViewDate);
        final TextView textViewPhoneNumber = findViewById(R.id.textViewPhoneNumber);
        final TextView textViewMessage = findViewById(R.id.textViewMessage);

        textViewDate.setText(str_date);
        textViewPhoneNumber.setText(str_phone);
        textViewMessage.setText(str_message);
    }
}
