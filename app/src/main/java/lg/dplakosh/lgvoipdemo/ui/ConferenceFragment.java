package lg.dplakosh.lgvoipdemo.ui;

import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.tsongkha.spinnerdatepicker.SpinnerDatePickerDialogBuilder;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import lg.dplakosh.lgvoipdemo.R;
import lg.dplakosh.lgvoipdemo.db.conference.Conference;
import lg.dplakosh.lgvoipdemo.db.conference.ConferenceSQLiteDatabaseHelper;
import lg.dplakosh.lgvoipdemo.list.conference.ConferenceBook;
import lg.dplakosh.lgvoipdemo.list.conference.ConferenceBookAdapter;
import lg.dplakosh.lgvoipdemo.net.AsyncResponse;
import lg.dplakosh.lgvoipdemo.net.HttpUtils;
import lg.dplakosh.lgvoipdemo.utils.Config;
import lg.dplakosh.lgvoipdemo.utils.Logger;
import lg.dplakosh.lgvoipdemo.voicecall.VoiceCallActivity;

public class ConferenceFragment extends Fragment {

    private final static String TAG = ConferenceFragment.class.getSimpleName();
    private ListView litstView_conference;

    //Overriden method onCreateView
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //Returning the layout file after inflating
        //Change R.layout.tab1 in you classes
        super.onCreate(savedInstanceState);

        View v = inflater.inflate(R.layout.fragment_conference, container, false);
        litstView_conference = v.findViewById(R.id.listConference);


        //방리스트를 추출한다.
        refreshList();


        //방을 만든다.
        v.findViewById(R.id.button_register_new_conference_room).setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {


                Calendar currentTime = Calendar.getInstance();
                int hour = currentTime.get(Calendar.HOUR_OF_DAY);
                int minute = currentTime.get(Calendar.MINUTE);
                int year = currentTime.get(Calendar.YEAR);
                int month = currentTime.get(Calendar.MONTH);
                int day = currentTime.get(Calendar.DAY_OF_MONTH);


                SpinnerDatePickerDialogBuilder startDatePicker = new SpinnerDatePickerDialogBuilder()
                        .context(getActivity())
                        .callback(new com.tsongkha.spinnerdatepicker.DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(com.tsongkha.spinnerdatepicker.DatePicker view, int startYear, int startMonth, int startDay) {


                                TimePickerDialog mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                                    @Override
                                    public void onTimeSet(TimePicker timePicker, int startHour, int startMin) {


                                        SpinnerDatePickerDialogBuilder endDatePicker = new SpinnerDatePickerDialogBuilder()
                                                .context(getActivity())
                                                .callback(new com.tsongkha.spinnerdatepicker.DatePickerDialog.OnDateSetListener() {
                                                    @Override
                                                    public void onDateSet(com.tsongkha.spinnerdatepicker.DatePicker view, int endYear, int endMonth, int endDay) {


                                                        TimePickerDialog mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                                                            @Override
                                                            public void onTimeSet(TimePicker timePicker, int endHour, int endMin) {


                                                                //회의 시작시간
                                                                DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

                                                                Date startDate = new Date();
                                                                startDate.setYear(startYear);
                                                                startDate.setMonth(startMonth);
                                                                startDate.setDate(startDay);
                                                                startDate.setHours(startHour);
                                                                startDate.setMinutes(startMin);

                                                                Date endDate = new Date();
                                                                endDate.setYear(endYear);
                                                                endDate.setMonth(endMonth);
                                                                endDate.setDate(endDay);
                                                                endDate.setHours(endHour);
                                                                endDate.setMinutes(endMin);

                                                                String strStartDate = df.format(startDate);
                                                                //회의 종료시간 - ""
                                                                String strEndDate = df.format(endDate);


                                                                //---------------------------------------------------------------
                                                                // START INPUT PHONE NUMBER
                                                                //----------------------------------------------------------------
                                                                final EditText input = new EditText(getActivity());
                                                                input.setInputType(InputType.TYPE_CLASS_TEXT);
                                                                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity()).setTitle("Input phone number(s) with ,")
                                                                        .setView(input);
                                                                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                                    @Override
                                                                    public void onClick(DialogInterface dialog, int which) {
                                                                        String strPhone = input.getText().toString();
                                                                        String[] strList = strPhone.split(",");
                                                                        //초대할 사람의 폰넘버를 넣는다.
                                                                        Vector<String> guestPhoneNo = new Vector<>();
                                                                        Collections.addAll(guestPhoneNo, strList);
                                                                        //=================================================================================================
                                                                        // START INPUT TITLE
                                                                        //=================================================================================================
                                                                        final EditText input2 = new EditText(getActivity());
                                                                        input2.setInputType(InputType.TYPE_CLASS_TEXT);
                                                                        AlertDialog.Builder builderTitle = new AlertDialog.Builder(getActivity()).setTitle("Input room title")
                                                                                .setView(input2);
                                                                        builderTitle.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                                            @Override
                                                                            public void onClick(DialogInterface dialog, int which) {
                                                                                String strRoomTitle = input2.getText().toString();
                                                                                //--------------------------------------------------------------------------
                                                                                HttpUtils.registerConference(Config.getToken(), Config.getPhoneNo(), strRoomTitle, strStartDate, strEndDate, guestPhoneNo, getActivity(), new AsyncResponse() {
                                                                                    @Override
                                                                                    public void processFinish(String rawFormat, boolean result, Vector<String> values) {

                                                                                        //for debug
                                                                                        Toast.makeText(getActivity(), rawFormat, Toast.LENGTH_SHORT).show();
                                                                                        //방만들기 성공
                                                                                        if (result) {
                                                                                            refreshList();
                                                                                        } else {

                                                                                        }
                                                                                    }
                                                                                });
                                                                                //---------------------------------------------------------------------------------

                                                                            }
                                                                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                                                            @Override
                                                                            public void onClick(DialogInterface dialog, int which) {
                                                                                dialog.cancel();
                                                                            }
                                                                        }).show();
                                                                        //=================================================================================================
                                                                        // END INPUT TITLE
                                                                        //=================================================================================================
                                                                    }

                                                                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                                                    @Override
                                                                    public void onClick(DialogInterface dialog, int which) {
                                                                        dialog.cancel();
                                                                    }
                                                                }).show();
                                                                //----------------------------------
                                                                // end input phone number
                                                                //----------------------------------


                                                            }
                                                        }, hour, minute, true);//Yes 24 hour time
                                                        mTimePicker.setTitle("Select End Time");
                                                        mTimePicker.show();

                                                    }
                                                })
                                                .spinnerTheme(R.style.DatePickerSpinner)
                                                .showTitle(true)
                                                .showDaySpinner(true)
                                                .defaultDate(startYear, startMonth, startDay)
                                                .maxDate(2300, 0, 1)
                                                .minDate(startYear, startMonth, startDay);

                                        com.tsongkha.spinnerdatepicker.DatePickerDialog endDatePicker_ = endDatePicker.build();
                                        endDatePicker_.setTitle("Set End Date");
                                        endDatePicker_.show();

                                    }
                                }, hour, minute, true);//Yes 24 hour time
                                mTimePicker.setTitle("Select Start Time");
                                mTimePicker.show();

                            }
                        })
                        .spinnerTheme(R.style.DatePickerSpinner)
                        .showTitle(true)
                        .showDaySpinner(true)
                        .defaultDate(year, month, day)
                        .maxDate(2300, 0, 1)
                        .minDate(year, month, day);
                com.tsongkha.spinnerdatepicker.DatePickerDialog startDatePicker_ = startDatePicker.build();
                startDatePicker_.setTitle("Set Start Date");
                startDatePicker_.show();


            }
        });
        return v;
    }

    //https://stackoverflow.com/questions/20702333/refresh-fragment-at-reload
    private void refreshFragment() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.detach(this).attach(this).commit();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {

        super.setUserVisibleHint(
                isVisibleToUser);

        // Refresh tab data:

        if (getFragmentManager() != null) {

            getFragmentManager()
                    .beginTransaction()
                    .detach(this)
                    .attach(this)
                    .commit();
        }
    }

    private void refreshList() {


        HttpUtils.retrieveConference(Config.getToken(), Config.getPhoneNo(), getActivity(), new AsyncResponse() {
            @Override
            public void processFinish(String rawFormat, boolean result, Vector<String> values) {

                Log.i(TAG, "start reading database");
                ConferenceSQLiteDatabaseHelper db = new ConferenceSQLiteDatabaseHelper(getActivity());
                SQLiteDatabase helper = db.getReadableDatabase();
                List<Conference> contacts = db.getAll();
                db.getReadableDatabase();
                Log.i(TAG, "end reading database");
                Log.i(TAG, contacts.toString());

                List<ConferenceBook> listConferenceBook = new ArrayList<>();
                for (Conference cn : contacts) {
                    String startDate = cn.getStartDate();
                    String endDate = cn.getEndDate();
                    String hostPhoneNumber = cn.getHostPhoneNo();
                    String guestPhoneNumber = cn.getGuestPhoneNo();
                    String subject = cn.getSubject();
                    String uniqueId = cn.getUniqueId();
                    String conferencePhoneNo = cn.getConferencePhoneNo();


                    listConferenceBook.add(new ConferenceBook(subject,
                            startDate, endDate, hostPhoneNumber, guestPhoneNumber, uniqueId, conferencePhoneNo));
                }

                ConferenceBookAdapter adapter = new ConferenceBookAdapter(getActivity(), listConferenceBook);
                litstView_conference.setAdapter(adapter);
                litstView_conference.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                        String textView_conference_guestPhoneNos = ((TextView) view.findViewById(R.id.textView_conference_guestPhoneNos)).getText().toString();
                        String textView_conference_conferencePhoneNo = ((TextView) view.findViewById(R.id.textView_conference_conferencePhoneNo)).getText().toString();
                        String textView_conference_hostNo = ((TextView) view.findViewById(R.id.textView_conference_hostNo)).getText().toString();
                        String textView_conference_title = ((TextView) view.findViewById(R.id.textView_conference_title)).getText().toString();
                        String textView_conference_uniqueId = ((TextView) view.findViewById(R.id.textView_conference_uniqueId)).getText().toString();


                        final EditText input2 = new EditText(getActivity());
                        input2.setText(((TextView) view.findViewById(R.id.textView_conference_title)).getText().toString());
                        input2.setInputType(InputType.TYPE_CLASS_TEXT);
                        AlertDialog.Builder builderTitle = new AlertDialog.Builder(getActivity()).setTitle("Enter Room")
                                .setView(input2);
                        builderTitle.setPositiveButton("ENTER", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(getActivity(), VoiceCallActivity.class);
                                intent.setData(Uri.fromParts("tel", textView_conference_conferencePhoneNo, null));
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                getActivity().startActivity(intent);
                            }
                        }).setNeutralButton("DELETE", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                HttpUtils.deleteConference(Config.getToken(), textView_conference_uniqueId, textView_conference_hostNo, new AsyncResponse() {
                                    @Override
                                    public void processFinish(String rawFormat, boolean result, Vector<String> values) {
                                        if (result) {
                                            //방을 지운후 새롭게 갱신한다. Refresh room list after getting deleted.
                                            Toast.makeText(getActivity(), "Successfully deleted", Toast.LENGTH_SHORT).show();

                                            {
                                                ConferenceSQLiteDatabaseHelper mDbHelper = new ConferenceSQLiteDatabaseHelper(getActivity());
                                                mDbHelper.deleteByUniqueId(textView_conference_uniqueId);
                                                mDbHelper.close();
                                            }
// Reload current fragment
                                            Logger.e(TAG, rawFormat);
                                            refreshFragment();

                                        } else {
                                            try {
                                                //방이 서버에 존재하지 않으므로, DB에서도 날리도록 한다.
                                                // The room does not exist, purge in db as well.
                                                if (values.get(1).equals("0408")) {
                                                    ConferenceSQLiteDatabaseHelper mDbHelper = new ConferenceSQLiteDatabaseHelper(getActivity());
                                                    mDbHelper.deleteByUniqueId(textView_conference_uniqueId);
                                                    mDbHelper.close();
                                                    refreshFragment();
                                                }
                                            } catch (Exception e) {
                                                Logger.e(TAG, e);
                                            }
                                        }
                                    }
                                });

                            }
                        }).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        }).show();


                    }
                });
                db.close();

            }
        });


    }
}

