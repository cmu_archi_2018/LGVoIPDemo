package lg.dplakosh.lgvoipdemo.ui;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Vector;

import lg.dplakosh.lgvoipdemo.R;
import lg.dplakosh.lgvoipdemo.etc.AsteriskPasswordTransformationMethod;
import lg.dplakosh.lgvoipdemo.net.AsyncResponse;
import lg.dplakosh.lgvoipdemo.net.HttpUtils;
import lg.dplakosh.lgvoipdemo.utils.Config;

public class ChangeUserInfoActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_user_info);

        // Example of a call to a native method
        final EditText editText_address = findViewById(R.id.editText_change_userInfo_Address);
        final EditText editText_date = findViewById(R.id.editText_change_userInfo_date);
        final EditText editText_cardNo = findViewById(R.id.editText_change_userInfo_cardNum);
        final EditText editText_CVC = findViewById(R.id.editText_change_userInfo_CVC);

        final TextView textView_address = findViewById(R.id.textView_change_userInfo_Address);
        final TextView textView_date = findViewById(R.id.textView_change_userInfo_date);
        final TextView textView_cardNo = findViewById(R.id.textView_change_userInfo_cardNum);
        final TextView textView_CVC = findViewById(R.id.textView_change_userInfo_CVC);

        //============================
        // 정보가 있으면 자동으로 채운다.
        //============================
        if (!Config.getAddress().isEmpty()) {
            editText_address.setText(Config.getAddress());
        }
        if (!Config.getCreditcardExpirDate().isEmpty()) {
            editText_date.setText(Config.getCreditcardExpirDate());
        }
        if (!Config.getCreditcardNo().isEmpty()) {
            editText_cardNo.setText(Config.getCreditcardNo());
        }
        if (!Config.getCVC().isEmpty()) {
            editText_CVC.setText(Config.getCVC());
        }

        findViewById(R.id.button_change_userInfo_confirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String address = editText_address.getText().toString();
                final String date = editText_date.getText().toString();
                final String cardNo = editText_cardNo.getText().toString();
                final String CVC = editText_CVC.getText().toString();

                if (address.isEmpty() || date.isEmpty() || cardNo.isEmpty() || CVC.isEmpty()) {
                    Toast.makeText(ChangeUserInfoActivity.this, "invalid input. please verify values", Toast.LENGTH_SHORT).show();
                    return;
                }
                String string = HttpUtils.changeUserInfo(Config.getToken(), Config.getEmail(), address, cardNo, date, CVC, ChangeUserInfoActivity.this, new AsyncResponse() {
                    @Override
                    public void processFinish(String rawFormat, boolean result, Vector<String> values) {
                        Toast.makeText(ChangeUserInfoActivity.this, rawFormat, Toast.LENGTH_SHORT).show();

                        if (result) {
                            //성공시 변경된 정보를 저장한다.
                            //TODO SECURE?
                            Config.setAddress(address);
                            Config.setCreditcardExpirDate(date);
                            Config.setCreditcardNo(cardNo);
                            Config.setCVC(CVC);
                            finish(); //이전 액티비티로 돌아간다.
                        } else {

                        }
                    }
                });
            }
        });

        editText_address.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = editText_address.getText().toString();
                int num = text.length();
                textView_address.setText(Integer.toString(num) + "/150");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editText_date.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = editText_date.getText().toString();
                int num = text.length();
                textView_date.setText(Integer.toString(num) + "/150");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editText_cardNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = editText_cardNo.getText().toString();
                int num = text.length();
                textView_cardNo.setText(Integer.toString(num) + "/150");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editText_CVC.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = editText_CVC.getText().toString();
                int num = text.length();
                textView_CVC.setText(Integer.toString(num) + "/150");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editText_CVC.setTransformationMethod(new AsteriskPasswordTransformationMethod());
    }
}