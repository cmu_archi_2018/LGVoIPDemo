package lg.dplakosh.lgvoipdemo.ui;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import lg.dplakosh.lgvoipdemo.R;
import lg.dplakosh.lgvoipdemo.db.contact.Contact;
import lg.dplakosh.lgvoipdemo.db.contact.ContactHandler;
import lg.dplakosh.lgvoipdemo.etc.EtcChoi;

public class AddContactActivity extends Activity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addcontact);

        Button Button_Act;
        Button_Act = (Button) findViewById(R.id.AddContact);

        final EditText editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        final TextView textViewEmail = (TextView) findViewById(R.id.textViewEmail);

        final EditText editTextPhonenumber = (EditText) findViewById(R.id.editTextPhonenumber);
        final TextView textViewPhonenumber = (TextView) findViewById(R.id.textViewPhonenumber);

        final EditText editTextName = (EditText) findViewById(R.id.editTextName);
        final TextView textViewName = (TextView) findViewById(R.id.textViewName);

        Button_Act.setOnClickListener(new Button.OnClickListener(){
            @Override
            public void onClick(View v) {

                String textEmail = editTextEmail.getText().toString();
                String textPhonenumber = editTextPhonenumber.getText().toString();
                String textName = editTextName.getText().toString();
                if (EtcChoi.checkEmail(textEmail)&&EtcChoi.checkPhoneNum(textPhonenumber)) {
                    ContactHandler db = new ContactHandler(getApplicationContext());

                    db.addContact(new Contact(textName, textPhonenumber, textEmail));

                    //db.addContact(new Contact("Alex Park", "234-567-8901", "abc@def.net"));

                    //db.getAllContacts();

                    db.close();

                    ContactFragment.allow=true;
                    Intent myintent = new Intent(AddContactActivity.this, MainTabActivity.class);
                    myintent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(myintent);
                    finish();
                }
                else{
                    Toast.makeText(AddContactActivity.this, "Format was wrong!!! Please check your information", Toast.LENGTH_SHORT).show();
                }
            }
        });

        editTextEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = editTextEmail.getText().toString();
                int num = text.length();
                textViewEmail.setText(Integer.toString(num) + "/150");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editTextName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = editTextName.getText().toString();
                int num = text.length();
                textViewName.setText(Integer.toString(num) + "/150");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editTextPhonenumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String text = editTextPhonenumber.getText().toString();
                int num = text.length();
                textViewPhonenumber.setText(Integer.toString(num) + "/150");
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

    }
}
