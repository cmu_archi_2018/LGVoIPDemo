package lg.dplakosh.lgvoipdemo.net;

import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

import lg.dplakosh.lgvoipdemo.VoiceCallApp;
import lg.dplakosh.lgvoipdemo.db.conference.ConferenceDBSchema;
import lg.dplakosh.lgvoipdemo.db.conference.ConferenceSQLiteDatabaseHelper;
import lg.dplakosh.lgvoipdemo.db.sms.SMSDBSchema;
import lg.dplakosh.lgvoipdemo.db.sms.SMSSQLiteDatabaseHelper;
import lg.dplakosh.lgvoipdemo.utils.Config;
import lg.dplakosh.lgvoipdemo.utils.Helper;
import lg.dplakosh.lgvoipdemo.utils.Logger;


/**
 * client library for https://docs.google.com/presentation/d/1XOZ5WR9N_2OgOtWfLrBSnXZwsjqJEdATYsVuIDSwmSs/edit#slide=id.p
 */
class HttpPostAsyncTask extends AsyncTask<String, Void, String> {

    private final static String TAG = HttpPostAsyncTask.class.getSimpleName();
    private ProgressDialog mDiaglog;
    private AsyncResponse delegate = null;
    // This is the JSON body of the post
    private Map<String, String> mMapHeader = null;
    private JSONObject mJsonBody = null;


    private String mURIType;
    private String methodType;
    private final Context mContext;

    private static String convertStreamToString(InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    @Override
    protected void onPreExecute() {
        if (mDiaglog != null) {
            try {
                mDiaglog.getWindow().setType(WindowManager.LayoutParams.TYPE_APPLICATION_PANEL);
                mDiaglog.setMessage("Progressing... ");
                mDiaglog.show();
            } catch (Exception e) {
                Logger.e(TAG, e);
            }

        }
    }

    // This is a constructor that allows you to pass in the JSON body
    public HttpPostAsyncTask(Map<String, String> mapHeader, JSONObject jsonBody, AsyncResponse delegate, Context context) {
        mContext = context;

        if (mContext != null) {
            mDiaglog = new ProgressDialog(mContext);
        }
        try {
            if (mapHeader != null) {
                this.mMapHeader = new HashMap<>(mapHeader);
            } else {
                Log.e(TAG, "header is null");
            }
            if (jsonBody != null) {
                this.mJsonBody = new JSONObject(jsonBody.toString());
                Log.i(TAG, this.mJsonBody.toString() + "length:" + this.mJsonBody.toString().length());
            } else {
                Log.e(TAG, "body is null");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        this.delegate = delegate;
    }

    // This is a function that we are overriding from AsyncTask. It takes Strings as parameters because that is what we defined for the parameters of our async task
    @Override
    protected String doInBackground(String... params) {
        String response = "";
        try {

            methodType = params[0];
            mURIType = params[2]; //GET, POST방식에 따라 나뉘는 경우가 있으므로
            StringBuilder fullUrl = new StringBuilder();

            for (int i = 1; i < params.length; i++) {
                fullUrl.append(params[i]);
            }

            // This is getting the url from the string we passed in
            Log.i(TAG, "url: " + fullUrl);
            URL url = new URL(fullUrl.toString());
            // Create the urlConnection
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setDoInput(true);

            //in GET request, no set to setDoOutput!!!
            //http://malchooni.name/httpurlconnection%EC%9D%84-%EC%9D%B4%EC%9A%A9%ED%95%9C-http-%ED%81%B4%EB%9D%BC%EC%9D%B4%EC%96%B8%ED%8A%B8/?ckattempt=1
            if (!methodType.equals("GET")) {
                urlConnection.setDoOutput(true);
            }
            urlConnection.setRequestMethod(methodType);
            Log.i(TAG, "method: " + urlConnection.getRequestMethod());

            //TODO 파라미터로 빼도될듯.
//            urlConnection.setRequestProperty("Content-Type", "application/json");
            //────────────────────────────
            // 추가 헤더를 세팅한다.
            //────────────────────────────
            if (this.mMapHeader != null) {
                for (Map.Entry<String, String> entry : mMapHeader.entrySet()) {
                    String key = entry.getKey();
                    String value = entry.getValue();
                    Log.i(TAG, "header(" + key + "):(" + value + ")");
                    urlConnection.setRequestProperty(key, value);
                }
            }
            if (this.mJsonBody != null) {
                urlConnection.setRequestProperty("Content-Length", "" + this.mJsonBody.toString().length());
            }
            //────────────────────────────
            // POST, GET, ....
            //────────────────────────────


//            Log.i(TAG, urlConnection.getURL().toString());
//            Log.i(TAG, urlConnection.getHeaderFields().toString());
//            Log.i(TAG, urlConnection.getRequestProperties().toString());

            if (this.mJsonBody != null) {
                DataOutputStream os = new DataOutputStream(urlConnection.getOutputStream());
                os.writeBytes(this.mJsonBody.toString());
                os.flush();
                Log.i(TAG, "body: " + this.mJsonBody.toString());
            } else {
                Log.e(TAG, "mJsonBody is NULL!!!!");
            }
            int statusCode = urlConnection.getResponseCode();
            String responseMsg = urlConnection.getResponseMessage();

            if (statusCode == HttpURLConnection.HTTP_OK) {
                InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
                response = convertStreamToString(inputStream);
            } else {
                // Status code is not 200
                // Do something to handle the error
                Log.e(TAG, "not 200 status code is " + statusCode + " " + responseMsg);
                InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
                response = "" + statusCode + " " + responseMsg + convertStreamToString(inputStream);
            }
        } catch (Exception e) {
            Logger.e(TAG, e);
        }
        return response;
    }

    @Override
    protected void onPostExecute(String result) {
        if (mDiaglog != null) {
            if (mDiaglog.isShowing()) {
                mDiaglog.dismiss();
            }
        }
        boolean bret = false;
        try {
            Log.i("response result", result);
            JSONObject json = new JSONObject(result);
            if ("0000".equals(json.getString("code"))) {
                bret = true;
            }
            if (bret) {
                switch (mURIType) {


                    case HttpUtils.URI_AUTH_RECOVER: {
                        Vector<String> a = new Vector<>();
                        a.add(json.toString());
                        delegate.processFinish(result, bret, a);
                    }
                    case HttpUtils.URI_SERVICE_CONFERENCE: {
                        switch (methodType) {
                            case "POST": {
                                Vector<String> a = new Vector<>();
                                a.add(json.toString());
                                delegate.processFinish(result, bret, a);
                            }
                            break;
                            case "GET": {
                                JSONArray jsonArray = json.getJSONArray("conference");
                                Log.i("conference|response", json.toString());

                                Log.i("conference|response", jsonArray.toString());
                                Log.i("conference|response", "length: " + jsonArray.length());

                                if (jsonArray.length() != 0) {
                                    if (mContext != null) {
                                        Log.i(TAG, "start write db");
                                        ConferenceSQLiteDatabaseHelper mDbHelper = new ConferenceSQLiteDatabaseHelper(mContext);
                                        SQLiteDatabase db = mDbHelper.getWritableDatabase();
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            JSONObject jsonObj = jsonArray.getJSONObject(i);
                                            Log.i(TAG, "jsonObj(" + i + ")" + jsonObj.toString());
                                            ContentValues values = new ContentValues();
                                            values.put(ConferenceDBSchema.FeedEntry.COLUMN_NAME_UNIQUEID, jsonObj.getString("_id"));
                                            values.put(ConferenceDBSchema.FeedEntry.COLUMN_NAME_conferencePhoneNo, jsonObj.getString("conferencePhoneNo"));
                                            values.put(ConferenceDBSchema.FeedEntry.COLUMN_NAME_hostPhoneNo, jsonObj.getString("hostPhoneNo"));
                                            values.put(ConferenceDBSchema.FeedEntry.COLUMN_NAME_subject, jsonObj.getString("subject"));
                                            values.put(ConferenceDBSchema.FeedEntry.COLUMN_NAME_startDate, jsonObj.getString("startDate"));
                                            values.put(ConferenceDBSchema.FeedEntry.COLUMN_NAME_endDate, jsonObj.getString("endDate"));

                                            //TODO 이거 어떻게 처리할지 고민
                                            values.put(ConferenceDBSchema.FeedEntry.COLUMN_NAME_guestPhoneNo, jsonObj.getJSONArray("guestPhoneNo").toString());

                                            values.put(ConferenceDBSchema.FeedEntry.COLUMN_NAME_requestDate, jsonObj.getString("requestDate"));

                                            try {
                                                long newRowId = db.insert(ConferenceDBSchema.FeedEntry.TABLE_NAME, null, values);
                                            } catch(Exception e)
                                            {
                                                Logger.e(TAG,e);
                                            }

                                        }
                                        Log.i(TAG, "end write db");
                                        mDbHelper.close();
                                        Vector<String> strings = new Vector<>();
                                        strings.add(json.getString("message"));
                                        delegate.processFinish(result, bret, strings);
                                    } else {

                                        Log.e(TAG, "context is null");
                                        Log.e(TAG, "context is null");
                                        Log.e(TAG, "context is null");
                                        Log.e(TAG, "context is null");
                                        Log.e(TAG, "context is null");

                                        Vector<String> strings = new Vector<>();
                                        strings.add(json.toString());
                                        delegate.processFinish(result, false, strings);
                                    }
                                }
                            }
                            break;

                            case "PATCH": {
                                Vector<String> a = new Vector<>();
                                a.add(json.toString());
                                delegate.processFinish(result, bret, a);
                            }
                            break;

                            case "DELETE": {
                                Vector<String> a = new Vector<>();
                                a.add(json.toString());
                                delegate.processFinish(result, bret, a);
                            }
                            break;
                            default:
                                Log.e("CONFERENCE", "UNKNOWN method Type: " + methodType);
                                Vector<String> a = new Vector<>();
                                a.add(json.toString());
                                delegate.processFinish(result, bret, a);
                                break;
                        }
                    }

                    break;

                    case HttpUtils.URI_USER_CHANGEPASSWORD: {
                        Vector<String> strings = new Vector<>();
                        strings.add(json.getString("message"));
                        delegate.processFinish(result, bret, strings);
                    }
                    break;


                    case HttpUtils.URI_USER_PROFILE: {
                        Vector<String> a = new Vector<>();
                        a.add(json.toString());
                        delegate.processFinish(result, bret, a);
                    }
                    break;

                    case HttpUtils.URI_AUTH_LOGIN: {
                        Vector<String> strings = new Vector<>();
                        strings.add(json.getString("token"));
                        strings.add("" + json.getJSONObject("user").getString("phoneNo"));

                        delegate.processFinish(result, bret, strings);
                    }
                    break;
                    case HttpUtils.URI_AUTH_REGISTRATION: {
                        Vector<String> strings = new Vector<>();
                        strings.add(json.getJSONObject("user").getString("phoneNo"));
                        delegate.processFinish(result, bret, strings);
                    }
                    break;
                    case HttpUtils.URI_DIRECTORY_STATUS:
                        if (methodType.equals("POST")) {
                            JSONArray jsonArray = json.getJSONArray("textMessages");

                            Log.i("SMS", json.toString());
                            if (jsonArray.length() != 0) {
                                if (mContext != null) {

                                    SMSSQLiteDatabaseHelper mDbHelper = new SMSSQLiteDatabaseHelper(mContext);
                                    SQLiteDatabase db = mDbHelper.getWritableDatabase();
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObj = jsonArray.getJSONObject(i);
                                        //    public String insertEntry(String MESSAGE, String DATE, String phoneNo, String READORNOT) {\
                                        ContentValues values = new ContentValues();
                                        values.put(SMSDBSchema.FeedEntry.COLUMN_NAME_DATE, jsonObj.getString("requestDate"));
                                        values.put(SMSDBSchema.FeedEntry.COLUMN_NAME_PHONENO, jsonObj.getString("callerPhoneNo"));
                                        values.put(SMSDBSchema.FeedEntry.COLUMN_NAME_MESSAGE, jsonObj.getString("textMessage"));
                                        //values.put(SMSDBSchema.FeedEntry.COLUMN_NAME_READORNOT, jsonObj.getString("textMessage"));
                                        values.put(SMSDBSchema.FeedEntry.COLUMN_NAME_READORNOT, "NOTREAD");
                                        long newRowId = db.insert(SMSDBSchema.FeedEntry.TABLE_NAME, null, values);
                                    }
                                    mDbHelper.close();
                                }

                            }

                            Vector<String> strings = new Vector<>();
                            strings.add(json.getString("message"));
                            delegate.processFinish(result, bret, strings);
                        } else /*GET*/ {
//                            json.getJSONArray("textMessages")
//                            delegate.processFinish(result, bret,  json.getJSONObject("info").getString("phoneNo"));

                        }
                        break;
                    case HttpUtils.URI_USER_LIST:

                        break;

                    case HttpUtils.URI_SERVICE_MESSAGE:
                        Vector<String> strings = new Vector<>();
                        strings.add(json.getString("message"));
                        delegate.processFinish(result, bret, strings);

                        break;
                }
            } else {
                Vector<String> strings = new Vector<>();
                strings.add(json.getString("message"));
                strings.add(json.getString("code"));

                delegate.processFinish(result, bret, strings);
            }
        } catch (JSONException e) {
            Logger.e(TAG, e);

            Vector<String> strings = new Vector<>();
            strings.add("json exception error");
            delegate.processFinish(result, bret, strings);
        }

    }
}


public class HttpUtils {
    private static final String TAG = HttpUtils.class.getSimpleName();

    public static String getServerUrl() {
        if (Config.getServerUrl().isEmpty()) {
            String strUrl = VoiceCallApp.local ? "http://10.0.3.2:8080" : "https://cmu-2018-webservice.herokuapp.com";
            Toast.makeText(VoiceCallApp.mContext, "Please set Server url in settings. default set to " + strUrl, Toast.LENGTH_SHORT).show();
            return strUrl;
        } else {
            return Config.getServerUrl();
        }
    }


    //로그인 (Login)
    public static final String URI_AUTH_LOGIN = "/api/auth/login";

    //유저등록 (Sign Up)
    public static final String URI_AUTH_REGISTRATION = "/api/auth/register";

    //유저 조회하기 (Retrieve user list)
    public static final String URI_USER_LIST = "/api/user/list";

    //비밀번호 변경
    public static final String URI_USER_CHANGEPASSWORD = "/api/user/password/"; //:email";

    //디렉토리서비스, heartbeat
    public static final String URI_DIRECTORY_STATUS = "/api/directory/status/"; /* +"phonenumber"*/

    //문자보내기(POST),
    public static final String URI_SERVICE_MESSAGE = "/api/service/message";


    //컨퍼런스콜 관련 ( CRUD )
    public static final String URI_SERVICE_CONFERENCE = "/api/service/conference"; /* + hostPhoneNo */


    //send email for recovering password
    public static final String URI_AUTH_RECOVER = "/api/auth/recover"; /* + ?:email&:token */


    public static final String URI_USER_PROFILE = "/api/user/profile/"; //:email :POST"

    /**
     * @param email            이메일주소(ID)
     * @param password         비밀번호
     * @param address          집주소(?)
     * @param cardNo           신용카드 번호
     * @param expirationDate   신용카드 만료일
     * @param verificationCode 신용카드 확인번호
     * @param context
     * @param response         결과를 리턴할 콜백을 넣어준다. 성공시 values[0]: 서버에서발급한 전화번호, 실패시 서버의 실패 메시지
     * @return
     * @brief 사용자 등록을 한다. 내부적으로 AsyncTask로 수행되므로 마지막 파라미터에 콜백을 세팅해주고 그것을 통해 리턴을 받아야한다.
     * @See
     */
    public static String requestRegister(String email, String password, String address, String cardNo, String expirationDate, String verificationCode, Context context, AsyncResponse response) {
        String ret = "";
        JSONObject jsonParam = new JSONObject();
        try {


            //------
            // Header
            //------
            Map<String, String> map = new HashMap<>();
            map.put("Content-Type", "application/json");

            jsonParam.put("email", email);
            jsonParam.put("password", password);
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonPayment = new JSONObject();
            jsonPayment.put("cardNo", cardNo);
            jsonPayment.put("expirationDate", expirationDate);
            jsonPayment.put("verificationCode", verificationCode);
            jsonArray.put(jsonPayment);
            jsonParam.put("payment", jsonArray);
            HttpPostAsyncTask asyncTask = (HttpPostAsyncTask) new HttpPostAsyncTask(map, jsonParam, response, context).execute("POST", getServerUrl(), URI_AUTH_REGISTRATION);
            ret = "see AsyncResponse for a result";
        } catch (JSONException e) {
            Logger.e(TAG, e);
        }
        return ret;
    }


    /**
     * @param email    이메일주소(ID)
     * @param password 비밀번호
     * @param context
     * @param response 결과를 리턴할 콜백을 넣어준다. 성공시 values[0]: 토큰, 실패시 서버의 실패 메시지
     * @return
     * @brief 로그인을 한다. 로그인후 token을 받는다.
     */
    public static String requestLogin(final String email, final String password, Context context, AsyncResponse response) {
        String ret = "";
        JSONObject jsonParam = new JSONObject();
        try {
            //------
            // Header
            //------
            Map<String, String> map = new HashMap<>();
            map.put("Content-Type", "application/json");

            jsonParam.put("email", email);
            jsonParam.put("password", password);
            HttpPostAsyncTask asyncTask = (HttpPostAsyncTask) new HttpPostAsyncTask(map, jsonParam, response, context).execute("POST", getServerUrl(), URI_AUTH_LOGIN);
            ret = "see AsyncResponse for a result";
        } catch (JSONException e) {
            Logger.e(TAG, e);
        }
        return ret;
    }

    /**
     * @param x_access_token 로그인시 받는 token
     * @param response       결과를 리턴할 콜백을 넣어준다. 성공시 values[0]: 토큰, 실패시 서버의 실패 메시지
     * @return
     * @brief 엑세스토큰이 valid 한지 검사한다.
     */
    public static String requestCheckTokenIsValid(String x_access_token, AsyncResponse response) {
        String ret = "";
        JSONObject jsonParam = new JSONObject();
        try {
            Map<String, String> map = new HashMap<>();
            map.put("x-access-token", x_access_token);
            HttpPostAsyncTask asyncTask = (HttpPostAsyncTask) new HttpPostAsyncTask(map, null, response, null).execute("GET", getServerUrl(), URI_AUTH_LOGIN);
            ret = "see AsyncResponse for a result";
        } catch (Exception e) {
            Logger.e(TAG, e);
        }
        return ret;
    }


    /**
     * 유저 목록을 조회한다.
     *
     * @param x_access_token 로그인시 받는 token
     * @param response       결과를 리턴할 콜백을 넣어준다. new AsyncResponse()
     * @return
     */
    public static String requestUserList(String x_access_token, AsyncResponse response) {
        String ret = "";
        JSONObject jsonParam = new JSONObject();
        try {
            Map<String, String> map = new HashMap<>();
            map.put("x-access-token", x_access_token);

            HttpPostAsyncTask asyncTask = (HttpPostAsyncTask) new HttpPostAsyncTask(map, null, response, null).execute("GET", getServerUrl(), URI_USER_LIST);
            ret = "see AsyncResponse for a result";
        } catch (Exception e) {
            Logger.e(TAG, e);
        }
        return ret;
    }


    /**
     * 전화번호를 IP Address로 변환한다.
     *
     * @param x_access_token 로그인시 받는 token
     * @param response       결과를 리턴할 콜백을 넣어준다. 성공시 values[0]: 토큰, 실패시 서버의 실패 메시지
     * @return
     */
    public static String requestIpAddressOfPhoneNumber(String x_access_token, String phoneNumber, Context context, AsyncResponse response) {
        String ret = "";
        JSONObject jsonParam = new JSONObject();
        try {
            Map<String, String> map = new HashMap<>();
            map.put("x-access-token", x_access_token);
            map.put("Content-Type", "application/json");

            HttpPostAsyncTask asyncTask = (HttpPostAsyncTask) new HttpPostAsyncTask(map, null, response, context).execute("GET", getServerUrl(), URI_DIRECTORY_STATUS, phoneNumber);
            ret = "see AsyncResponse for a result";
        } catch (Exception e) {
            Logger.e(TAG, e);
        }
        return ret;
    }

    /**
     * 단문메시지 전송
     *
     * @param x_access_token          로그인시 받는 token
     * @param senderPhone             송신자(본인) 폰번호
     * @param vecReceiverPhoneNumbers 수신자(들) 번호
     * @param msg                     보낼 메시지
     * @param response                values[0]
     * @return
     */
    public static String sendSMSMessage(String x_access_token, String senderPhone, Vector<String> vecReceiverPhoneNumbers, String msg, AsyncResponse response) {
        String ret = "";
        JSONObject jsonParam = new JSONObject();
        try {
            Map<String, String> map = new HashMap<>();
            map.put("x-access-token", x_access_token);
            map.put("Content-Type", "application/json");


            //송신자 번호
            jsonParam.put("phoneNo", senderPhone);

            //수신자(들) 번호
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonPayment = new JSONObject();
            for (String receiverPhoneNumber : vecReceiverPhoneNumbers) {
                jsonArray.put(receiverPhoneNumber);
            }
            jsonParam.put("receiveArray", jsonArray);

            //메시지
            jsonParam.put("message", msg);

            HttpPostAsyncTask asyncTask = (HttpPostAsyncTask) new HttpPostAsyncTask(map, jsonParam, response, null).execute("POST", getServerUrl(), URI_SERVICE_MESSAGE);
            ret = "see AsyncResponse for a result";
        } catch (Exception e) {
            Logger.e(TAG, e);
        }
        return ret;
    }


    public static String sendHeartBeatAndGetMessage(String x_access_token, String myPhoneNo, Context context, AsyncResponse response) {
        String ret = "";
        JSONObject jsonParam = new JSONObject();
        try {
            Map<String, String> map = new HashMap<>();
            map.put("x-access-token", x_access_token);
            map.put("Content-Type", "application/json");

            jsonParam.put("ipAddress", Helper.getLocalIP(context));
            HttpPostAsyncTask asyncTask = (HttpPostAsyncTask) new HttpPostAsyncTask(map, jsonParam, response, context).execute("POST", getServerUrl(), URI_DIRECTORY_STATUS, myPhoneNo);
            ret = "see AsyncResponse for a result";
        } catch (Exception e) {
            Logger.e(TAG, e);
        }
        return ret;
    }

    /**
     * 컨퍼런스 방을 개설한다.
     *
     * @param x_access_token 로그인시 받는 token
     * @param hostPhoneNo    나의 폰 번호
     * @param subject        방 제목
     * @param startDate      "2018-06-30T20:43:14.537Z" 와 같은 형식의 Date
     * @param endDate        "2018-06-30T20:43:14.537Z" 와 같은 형식의 Date
     * @param guestPhoneNo   초대할 사람의 번호(들)
     * @param response
     * @return
     */
    public static String registerConference(String x_access_token, String hostPhoneNo, String subject, String startDate, String endDate, Vector<String> guestPhoneNo, Context context, AsyncResponse response) {
        String ret = "";
        JSONObject jsonParam = new JSONObject();
        try {
            Map<String, String> map = new HashMap<>();
            map.put("x-access-token", x_access_token);
            map.put("Content-Type", "application/json");

            //메시지
            jsonParam.put("hostPhoneNo", hostPhoneNo);
            jsonParam.put("subject", subject);
            jsonParam.put("startDate", startDate);
            jsonParam.put("endDate", endDate);

            //수신자(들) 번호
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonPayment = new JSONObject();
            for (String phoneNo : guestPhoneNo) {
                jsonArray.put(new JSONObject().put("phoneNo", Integer.parseInt(phoneNo)));
            }
            jsonParam.put("guestPhoneNo", jsonArray);
            HttpPostAsyncTask asyncTask = (HttpPostAsyncTask) new HttpPostAsyncTask(map, jsonParam, response, null).execute("POST", getServerUrl(), URI_SERVICE_CONFERENCE);
            ret = "see AsyncResponse for a result";
        } catch (Exception e) {
            Logger.e(TAG, e);
        }
        return ret;
    }

    /**
     * 컨퍼런스 방의 정보를 변경한다.
     *
     * @param _id            방의 고유 id
     * @param x_access_token 로그인시 받는 token
     * @param hostPhoneNo    나의 폰 번호
     * @param subject        변경할 방 제목
     * @param startDate      "2018-06-30T20:43:14.537Z" 와 같은 형식의 Date
     * @param endDate        "2018-06-30T20:43:14.537Z" 와 같은 형식의 Date
     * @param guestPhoneNo   변경할 초대할 사람의 번호(들)
     * @return
     */
    public static String updateConference(String x_access_token, String _id, String hostPhoneNo, String subject, String startDate, String endDate, Vector<String> guestPhoneNo, AsyncResponse response) {
        String ret = "";
        JSONObject jsonParam = new JSONObject();
        try {
            Map<String, String> map = new HashMap<>();
            map.put("x-access-token", x_access_token);
            map.put("Content-Type", "application/json");

            //메시지
            jsonParam.put("_id", _id);
            jsonParam.put("hostPhoneNo", Integer.parseInt(hostPhoneNo));
            jsonParam.put("subject", subject);
            jsonParam.put("startDate", startDate);
            jsonParam.put("endDate", endDate);


            //수신자(들) 번호
            JSONArray jsonArray = new JSONArray();
            JSONObject jsonPayment = new JSONObject();
            for (String phoneNo : guestPhoneNo) {
                jsonArray.put(Integer.parseInt(phoneNo));
            }
            jsonParam.put("guestPhoneNo", jsonArray);
            HttpPostAsyncTask asyncTask = (HttpPostAsyncTask) new HttpPostAsyncTask(map, jsonParam, response, null).execute("PATCH", getServerUrl(), URI_SERVICE_CONFERENCE, hostPhoneNo);
            ret = "see AsyncResponse for a result";
        } catch (Exception e) {
            Logger.e(TAG, e);
        }
        return ret;
    }

    /**
     * 개설된 방을 조회한다.
     *
     * @param x_access_token
     * @param hostPhoneNo
     * @param response
     * @return
     */
    public static String retrieveConference(String x_access_token, String hostPhoneNo, Context context, AsyncResponse response) {
        String ret = "";
        try {

            Map<String, String> map = new HashMap<>();
            map.put("x-access-token", x_access_token);

            HttpPostAsyncTask asyncTask = (HttpPostAsyncTask) new HttpPostAsyncTask(map, null, response, context).execute("GET", getServerUrl(), URI_SERVICE_CONFERENCE, "/", hostPhoneNo);
            ret = "see AsyncResponse for a result";
        } catch (Exception e) {
            Logger.e(TAG, e);
        }
        return ret;
    }

    /**
     * 개설한 방을 삭제한다.
     *
     * @param x_access_token
     * @param _id
     * @param hostPhoneNo
     * @param response
     * @return
     */
    public static String deleteConference(String x_access_token, String _id, String hostPhoneNo, AsyncResponse response) {
        String ret = "";
        JSONObject jsonParam = new JSONObject();
        try {
            Map<String, String> map = new HashMap<>();
            map.put("x-access-token", x_access_token);
            map.put("Content-Type", "application/json");

            jsonParam.put("_id", _id);
            jsonParam.put("hostPhoneNo", hostPhoneNo);


            HttpPostAsyncTask asyncTask = (HttpPostAsyncTask) new HttpPostAsyncTask(map, jsonParam, response, null).execute("DELETE", getServerUrl(), URI_SERVICE_CONFERENCE);
            ret = "see AsyncResponse for a result";
        } catch (Exception e) {
            Logger.e(TAG, e);
        }
        return ret;
    }


    /**
     * 암호를 복구한다.
     *
     * @param userId
     * @param context
     * @param response
     * @return
     */
    public static String recoverPassword(String userId, Context context, AsyncResponse response) {
        String ret = "";
        JSONObject jsonParam = new JSONObject();
        try {

            //------
            // Header
            //------
            Map<String, String> map = new HashMap<>();
            map.put("Content-Type", "application/json");


            jsonParam.put("email", userId);
            HttpPostAsyncTask asyncTask = (HttpPostAsyncTask) new HttpPostAsyncTask(map, jsonParam, response, context).execute("POST", getServerUrl(), URI_AUTH_RECOVER);
            ret = "see AsyncResponse for a result";
        } catch (Exception e) {
            Logger.e(TAG, e);
        }
        return ret;
    }


    /**
     * 사용자 password를 변경한다.
     *
     * @param x_access_token 로그인시 주어진 토큰
     * @param oldPassword    기존 패스워드
     * @param newPassword    신규 패스워드
     * @param response
     * @return
     */
    public static String changeUserPassword(String x_access_token, String userId, String oldPassword, String newPassword, Context context, AsyncResponse response) {
        String ret = "";
        JSONObject jsonParam = new JSONObject();
        try {
            //-----------------------------------
            // 헤더
            //-----------------------------------
            Map<String, String> map = new HashMap<>();
            map.put("x-access-token", x_access_token);
            map.put("Content-Type", "application/json");

            //-----------------------------------
            // 바디
            //-----------------------------------
            jsonParam.put("oldPassword", oldPassword);
            jsonParam.put("newPassword", newPassword);
            HttpPostAsyncTask asyncTask = (HttpPostAsyncTask) new HttpPostAsyncTask(map, jsonParam, response, context).execute("POST", getServerUrl(), URI_USER_CHANGEPASSWORD, userId);
            ret = "see AsyncResponse for a result";
        } catch (Exception e) {
            Logger.e(TAG, e);
        }
        return ret;
    }


    public static String changeUserInfo(String x_access_token, String email, String address, String cardNo, String expirationDate, String verificationCode, Context context, AsyncResponse response) {
        String ret = "";
        JSONObject jsonParam = new JSONObject();
        try {
            //-----------------------------------
            // 헤더
            //-----------------------------------
            Map<String, String> map = new HashMap<>();
            map.put("x-access-token", x_access_token);
            map.put("Content-Type", "application/json");

            //-----------------------------------
            // 바디
            //-----------------------------------
            JSONObject jsonPayment = new JSONObject();
            jsonPayment.put("cardNo", cardNo);
            jsonPayment.put("expirationDate", expirationDate);
            jsonPayment.put("verificationCode", verificationCode);
            JSONArray jsonArry = new JSONArray();
            jsonArry.put(jsonPayment);
            JSONObject json = new JSONObject();
            jsonParam.put("payment", jsonArry);
            jsonParam.put("address", address);
            HttpPostAsyncTask asyncTask = (HttpPostAsyncTask) new HttpPostAsyncTask(map, jsonParam, response, context).execute("POST", getServerUrl(), URI_USER_PROFILE, email);
            ret = "see AsyncResponse for a result";
        } catch (Exception e) {
            Logger.e(TAG, e);
        }
        return ret;
    }

}
