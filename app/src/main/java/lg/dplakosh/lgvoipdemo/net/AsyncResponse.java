package lg.dplakosh.lgvoipdemo.net;

import java.util.Vector;

public interface AsyncResponse {
    void processFinish(String rawFormat, boolean result, Vector<String> values);
}
