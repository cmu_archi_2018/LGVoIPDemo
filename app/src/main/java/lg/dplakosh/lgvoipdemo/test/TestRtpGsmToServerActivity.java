package lg.dplakosh.lgvoipdemo.test;

import android.app.Activity;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Locale;

import lg.dplakosh.lgvoipdemo.R;

public class TestRtpGsmToServerActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //xml로 만들어진 뷰를 컨텐트뷰로 설정한다.
        setContentView(R.layout.test_activity_call);
        //───────────────────────────────────────────
        // 상대방 아이피주소
        //───────────────────────────────────────────
        final EditText edittext_ipaddress = findViewById(R.id.editText_ipaddress);
        //───────────────────────────────────────────
        // 상대방 포트설정
        //───────────────────────────────────────────
        final EditText edittext_port = findViewById(R.id.editText_port);


        final TestRtpTransceiver rtpTransceiver = new TestRtpTransceiver(getApplicationContext());
        Button btn_test_userRegister = findViewById(R.id.btn_test_call);
        btn_test_userRegister.setText("전화걸기");
        btn_test_userRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rtpTransceiver.call(String.valueOf(edittext_ipaddress.getText()), Integer.parseInt(String.valueOf(edittext_port.getText())) );
            }
        });

        TextView textMyIp = findViewById(R.id.textView_localip);
        textMyIp.setText(getLocalIP());
        ((TextView)findViewById(R.id.textView_localport)).setText(String.format("%d",rtpTransceiver.getLocalPort()));
    }


    private String getLocalIP()
    {
        int LocalIpAddressBin = 0;
        WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
        if (wifiManager != null) {
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            LocalIpAddressBin = wifiInfo.getIpAddress();
           return  String.format(Locale.US, "%d.%d.%d.%d", (LocalIpAddressBin & 0xff), (LocalIpAddressBin >> 8 & 0xff), (LocalIpAddressBin >> 16 & 0xff), (LocalIpAddressBin >> 24 & 0xff));
        }
        return "NO ADDRESS";
    }


}
