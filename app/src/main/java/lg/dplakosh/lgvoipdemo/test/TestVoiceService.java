package lg.dplakosh.lgvoipdemo.test;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.rtp.AudioCodec;
import android.net.rtp.AudioGroup;
import android.net.rtp.AudioStream;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.StrictMode;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

import lg.dplakosh.lgvoipdemo.utils.Helper;


/**
 *
 */

public class TestVoiceService extends Service {

    private final IBinder mBinder = new VoiceBinder();

    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    public class VoiceBinder extends Binder {
        public TestVoiceService getService() {
            return TestVoiceService.this;
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        startLocalBroadcastManager();

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (!newCallListenerWorking) {
            new Thread(new NewCallListener()).start();
        }
        Log.i("TestVoiceService", "Received start id " + startId + ": " + intent);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        stopLocalBroadcastManager();
        Log.i("TestVoiceService", "Service destroyed");
    }


    /**
     * Client functions to manipulate the AudioGroup and AudioStream.
     */

    private AudioGroup audioGroup;
    private AudioStream audioStream;
    private boolean inCall, newCallListenerWorking;
    private InetAddress localInetAddress, remoteInetAddress;
    private int localAudioPort, remoteAudioPort;
    private static final int NEW_CALL_PORT = 8237;
    public static final int SIGNAL_RECEIVE_PORT = 8236;
    public static final int SIGNAL_SEND_PORT = 8235;

    /**
     * Starts an AudioStream on localInetAddress on a random port(in accordance with the RFC).
     */
    private void startAudioStream() {
//        localInetAddress = Helper.getLocalIpAddress();
        try {
            localInetAddress = Helper.getLocalIpAddress();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            audioStream = new AudioStream(localInetAddress);
        } catch (SocketException e) {
            e.printStackTrace();
        }
        localAudioPort = audioStream.getLocalPort();
//        Log.i("TestVoiceService", "audioStream started on " + localInetAddress.getHostName() + " on port " + localAudioPort);
    }

    /**
     * To be called when the following variables are set:
     * remoteInetAddress, remoteAudioPort
     * localInetAddress, localAudioPort
     * When both users run this function, full-duplex audio conversation starts.
     */
    private void setStreams() {
        AudioCodec localAudioCodec = AudioCodec.AMR;
        audioGroup = new AudioGroup();
        audioGroup.setMode(AudioGroup.MODE_NORMAL);

        audioStream.associate(remoteInetAddress, remoteAudioPort);
        audioStream.setCodec(localAudioCodec);
        audioStream.join(audioGroup);
        Log.i("TestVoiceService", "audioStream associated with remote peer.");

        Intent intent = new Intent(ACTION_REMOTE_READY);
        mLocalBroadcastManager.sendBroadcast(intent);
    }

    public void holdGroup(boolean hold) {
        if (hold) {
            audioGroup.setMode(AudioGroup.MODE_ON_HOLD);
            Log.i("TestVoiceService", "Call on hold. Microphone and Speaker disabled.");
        } else {
            audioGroup.setMode(AudioGroup.MODE_NORMAL);
            Log.i("TestVoiceService", "Call off hold. Microphone and Speaker enabled.");
        }
    }

    public void muteGroup(boolean mute) {
        if (mute) {
            audioGroup.setMode(AudioGroup.MODE_MUTED);
            Log.i("TestVoiceService", "Microphone muted.");
        } else {
            audioGroup.setMode(AudioGroup.MODE_NORMAL);
            Log.i("TestVoiceService", "Microphone unmuted.");
        }
    }

    public int getAudioGroupMode() {
        return audioGroup.getMode();
    }

    public int getAudioStreamMode() {
        return audioStream.getMode();
    }

    public boolean isAudioGroupSet() {
        return audioGroup != null;
    }

    public boolean isAudioStreamSet() {
        return audioStream != null;
    }

    /**
     * Resets all data of the Service
     */
    private void closeAll() {
        audioStream.join(null);
        audioGroup = null;
        audioStream = null;
        remoteAudioPort = 0;
        localAudioPort = 0;
        remoteInetAddress = null;
        localInetAddress = null;
        Log.i("TestVoiceService", "Resources reset.");
    }

    private void newCall() {
        Intent newCallIntent = new Intent(this, TestNewCallCommingActivity.class);
        newCallIntent.putExtra("target", remoteInetAddress.getHostName());
        newCallIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getApplication().startActivity(newCallIntent);
    }

    /**
     * Called when a new call is made and when a call is accepted.
     */
    private void startCall() {
        //localInetAddress = Helper.getLocalIpAddress();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                send(localAudioPort, SIGNAL_RECEIVE_PORT, SIGNAL_SEND_PORT);
            }
        }, 1000);

        //https://stackoverflow.com/questions/25093546/android-os-networkonmainthreadexception-at-android-os-strictmodeandroidblockgua
        //[minq.park] 이거안하면 서버가 죽는다. -_- ui 스레드에서 network 관련 써도 안죽도록 정책을 변경한다.
        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
            //your codes here
            Intent intent = new Intent(this, TestInCallActivity.class);
            intent.putExtra("target", remoteInetAddress.getHostName());
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    public void endCall() {
        inCall = false;
        closeAll();
    }

    /**
     * Parses the incoming message and takes action.
     */
    private void parseSignal(int message) {
        if (message > 1000) {
            if (!isAudioGroupSet() && localAudioPort != 0)
                remoteAudioPort = message;
            Log.i("TestVoiceService", "Remote Audio Port is " + remoteAudioPort);
            setStreams();
        } else {
            Intent intent = new Intent("");
            switch (message) {
                case HOLD:
                    intent = new Intent(ACTION_REMOTE_HOLD);
                    break;
                case UNHOLD:
                    intent = new Intent(ACTION_REMOTE_UNHOLD);
                    break;
                case MUTE:
                    intent = new Intent(ACTION_REMOTE_MUTE);
                    break;
                case UNMUTE:
                    intent = new Intent(ACTION_REMOTE_UNMUTE);
                    break;
                case END:
                    intent = new Intent(ACTION_REMOTE_END);
                        endCall();
                    break;
                case REJECT:
                    intent = new Intent(ACTION_REMOTE_REJECT);
                    endCall();
                    break;
            }
            mLocalBroadcastManager.sendBroadcast(intent);
        }
    }

    /**
     * LocalBroadcastManager and BroadcastReceiver to handle intents from activities.
     */
    private LocalBroadcastManager mLocalBroadcastManager;
    private BroadcastReceiver mReceiver;

    public static final String ACTION_REMOTE_READY = "edu.cmu.sevenhours.voice.REMOTE_READY";
    public static final String ACTION_REMOTE_HOLD = "edu.cmu.sevenhours.voice.REMOTE_HOLD";
    public static final String ACTION_REMOTE_UNHOLD = "edu.cmu.sevenhours.voice.REMOTE_UNHOLD";
    public static final String ACTION_REMOTE_MUTE = "edu.cmu.sevenhours.voice.REMOTE_MUTE";
    public static final String ACTION_REMOTE_UNMUTE = "edu.cmu.sevenhours.voice.REMOTE_UNMUTE";
    public static final String ACTION_REMOTE_END = "edu.cmu.sevenhours.voice.REMOTE_END";
    public static final String ACTION_REMOTE_REJECT = "edu.cmu.sevenhours.voice.REMOTE_REJECT";

    public static final String ACTION_LOCAL_READY = "edu.cmu.sevenhours.voice.LOCAL_READY";
    public static final String ACTION_LOCAL_HOLD = "edu.cmu.sevenhours.voice.LOCAL_HOLD";
    public static final String ACTION_LOCAL_UNHOLD = "edu.cmu.sevenhours.voice.LOCAL_UNHOLD";
    public static final String ACTION_LOCAL_MUTE = "edu.cmu.sevenhours.voice.LOCAL_MUTE";
    public static final String ACTION_LOCAL_UNMUTE = "edu.cmu.sevenhours.voice.LOCAL_UNMUTE";
    public static final String ACTION_LOCAL_END = "edu.cmu.sevenhours.voice.LOCAL_END";
    public static final String ACTION_LOCAL_REJECT = "edu.cmu.sevenhours.voice.LOCAL_REJECT";

    private void startLocalBroadcastManager() {
        mLocalBroadcastManager = LocalBroadcastManager.getInstance(this);
        mReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String currentAction = intent.getAction();
                Log.i("Service", "intent recieved" + currentAction);
                if (currentAction.equals(TestNewCallCommingActivity.ACTION_ACCEPTED)) {
                    Log.i("Service", "intent recieved" + currentAction);
                    startCall();
                } else if (currentAction.equals(TestNewCallCommingActivity.ACTION_REJECTED)) {
                    send(REJECT, SIGNAL_RECEIVE_PORT, SIGNAL_SEND_PORT);
                    inCall = false;
                    closeAll();
                } else if (currentAction.equals(TestDialerActivity.ACTION_MAKE_CALL)) {
                    Bundle localBundle = intent.getExtras();
                    String target = ((String) localBundle.get("target"));
                    remoteInetAddress = Helper.getTargetInetaddress(target);
                    localInetAddress = Helper.getLocalIpAddress();
                    inCall = true;
                    send(START, NEW_CALL_PORT, SIGNAL_SEND_PORT);
                    receive(SIGNAL_RECEIVE_PORT);
                    startAudioStream();
                    startCall();
                } else if (currentAction.equals(TestVoiceService.NEW_SIGNAL)) {
                    Bundle localBundle = intent.getExtras();
                    int signal = (Integer) localBundle.get("signal");
                    parseSignal(signal);
                } else if (currentAction.equals(TestVoiceService.NEW_CALL)) {
                    newCall();
                } else {
                    Log.e("Service", "UNDEFINED action!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" + currentAction);
                }
            }
        };
        IntentFilter filter = new IntentFilter();
        filter.addAction(TestNewCallCommingActivity.ACTION_ACCEPTED);
        filter.addAction(TestNewCallCommingActivity.ACTION_REJECTED);
        filter.addAction(TestDialerActivity.ACTION_MAKE_CALL);
        filter.addAction(TestVoiceService.NEW_SIGNAL);
        filter.addAction(TestVoiceService.NEW_CALL);
        mLocalBroadcastManager.registerReceiver(mReceiver, filter);
    }

    private void stopLocalBroadcastManager() {
        mLocalBroadcastManager.unregisterReceiver(mReceiver);
    }


    /**
     * Threads that enable sending and receiving of signalling data.
     * Signal sending on SIGNAL_SEND_PORT.
     * Signal receiving on SHORT_SIGNAL_RECEIVE_PORT.
     */

    private static final int START = 100;
    //public static final int READY = 101;
    public static final int HOLD = 102;
    public static final int UNHOLD = 103;
    public static final int MUTE = 104;
    public static final int UNMUTE = 105;
    public static final int END = 106;
    private static final int REJECT = 107;

    public void send(int message, int remotePort, int localPort) {
        new Thread(new Send(message, remotePort, localPort)).start();
    }

    private void receive(int localPort) {
        new Thread(new SignalListener(localPort)).start();
    }

    //TODO: create persistent socket to send. More efficient.
    private class Send implements Runnable {

        final int toSend;
        final int remoteSignalPort;
        final int localSignalPort;

        Send(int message, int remotePort, int localPort) {
            toSend = message;
            remoteSignalPort = remotePort;
            localSignalPort = localPort;
        }

        public void run() {
            Socket socket = null;
            DataOutputStream dataOutputStream = null;
            try {
                //socket = new Socket(remoteInetAddress.getHostName(), remoteSignalPort, localInetAddress, localSignalPort);
                socket = new Socket(remoteInetAddress, remoteSignalPort);
                dataOutputStream = new DataOutputStream(socket.getOutputStream());
                dataOutputStream.writeInt(toSend);
                Log.i("TestVoiceService", "Sent " + toSend + " to " + remoteInetAddress.getHostName() + " port " + remoteSignalPort);
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            } finally {
                if (socket != null) {
                    try {
                        socket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (dataOutputStream != null) {
                    try {
                        dataOutputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private static final String NEW_SIGNAL = "edu.cmu.sevenhours.voice.NEW_SIGNAL";
    private static final String NEW_CALL = "edu.cmu.sevenhours.voice.NEW_CALL";

    private class SignalListener implements Runnable {

        final int localSignalPort;

        SignalListener(int localPort) {
            localSignalPort = localPort;
        }

        @Override
        public void run() {
            int message;
            ServerSocket serverSocket = null;
            Socket socket = null;
            DataInputStream dataInputStream = null;
            boolean flag = true;
            try {
                serverSocket = new ServerSocket(localSignalPort);
            } catch (IOException e) {
                e.printStackTrace();
                flag = false;
            }
            while (flag && inCall) {
                try {
                    Log.i("TestVoiceService", "Listening for Signals on port " + localSignalPort);
                    socket = serverSocket.accept();
                    dataInputStream = new DataInputStream(socket.getInputStream());
                    message = dataInputStream.readInt();
                    if (remoteInetAddress.equals(socket.getInetAddress())) {
                        Log.i("TestVoiceService", "Received " + message + " from " + remoteInetAddress.getHostName() + " on port " + localSignalPort);
                        //parseSignal(message);
                        Intent intent = new Intent(TestVoiceService.NEW_SIGNAL);
                        intent.putExtra("signal", message);
                        mLocalBroadcastManager.sendBroadcast(intent);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (socket != null) {
                        try {
                            socket.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if (dataInputStream != null) {
                        try {
                            dataInputStream.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            if (!inCall && serverSocket != null) {
                try {
                    serverSocket.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class NewCallListener implements Runnable {

        final int localSignalPort;

        NewCallListener() {
            localSignalPort = TestVoiceService.NEW_CALL_PORT;
        }

        @Override
        public void run() {
            int message;
            ServerSocket serverSocket = null;
            Socket socket = null;
            DataInputStream dataInputStream = null;
            boolean flag = true;
            try {
                serverSocket = new ServerSocket(localSignalPort);
            } catch (IOException e) {
                e.printStackTrace();
                flag = false;
                newCallListenerWorking = false;
            }
            while (flag) {
                try {
                    newCallListenerWorking = true;
                    Log.i("TestVoiceService", "Listening for new calls on port " + localSignalPort);
                    socket = serverSocket.accept();
                    dataInputStream = new DataInputStream(socket.getInputStream());
                    message = dataInputStream.readInt();
                    if (message == 100 && !inCall) {
                        remoteInetAddress = socket.getInetAddress();
                        localInetAddress = Helper.getLocalIpAddress();
                        Log.i("TestVoiceService", "Received " + message + " from " + remoteInetAddress.getHostName() + " on port " + localSignalPort);
                        inCall = true;
                        receive(SIGNAL_RECEIVE_PORT);
                        startAudioStream();
                        // start TestNewCallCommingActivity
                        Intent intent = new Intent(TestVoiceService.NEW_CALL);
                        mLocalBroadcastManager.sendBroadcast(intent);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (socket != null) {
                        try {
                            socket.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    if (dataInputStream != null) {
                        try {
                            dataInputStream.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            newCallListenerWorking = false;
        }
    }
}
