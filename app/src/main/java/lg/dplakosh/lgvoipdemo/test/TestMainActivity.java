package lg.dplakosh.lgvoipdemo.test;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Vector;

import lg.dplakosh.lgvoipdemo.R;
import lg.dplakosh.lgvoipdemo.net.AsyncResponse;
import lg.dplakosh.lgvoipdemo.net.HttpUtils;
import lg.dplakosh.lgvoipdemo.ui.WebViewBilling;
import lg.dplakosh.lgvoipdemo.utils.Config;


public class TestMainActivity extends Activity {

    private static final String LOG_TAG = TestMainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //xml로 만들어진 뷰를 컨텐트뷰로 설정한다.
        setContentView(R.layout.test_activity_main);
        //─────────── ────────────────────────────────
        // 유저 등록 테스트
        //───────────────────────────────────────────
        Button btn_test_userRegister = findViewById(R.id.btn_test_user_register);
        btn_test_userRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TestMainActivity.this, TestRegisterActivity.class));
            }
        });

        //───────────────────────────────────────────
        // 토큰 체크 테스트 (로그인후 받은 토큰값이 만료되었는지 확인)
        //───────────────────────────────────────────
        Button btn_test_token_check = findViewById(R.id.btn_test_token_check);
        btn_test_token_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String string = HttpUtils.requestCheckTokenIsValid(Config.getToken(), new AsyncResponse() {
                    @Override
                    public void processFinish(String rawFormat, boolean result, Vector<String> values) {
                        Toast.makeText(TestMainActivity.this, rawFormat, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        Button btn_test_webview = findViewById(R.id.button_webviewtest);
        btn_test_webview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TestMainActivity.this, WebViewBilling.class));

            }
        });


        //───────────────────────────────────────────
        // 폰번호를 ipaddress로 변경한다.
        //───────────────────────────────────────────
        final EditText editText_phone_to_ipaddress = findViewById(R.id.editText_test_phone_to_ipaddress);

        if (!Config.getPhoneNo().isEmpty()) {
            editText_phone_to_ipaddress.setText(Config.getPhoneNo());
        }


        Button btn_test_phone_to_ipaddress = findViewById(R.id.btn_test_phone_to_ipaddress);
        btn_test_token_check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String string = HttpUtils.requestIpAddressOfPhoneNumber(Config.getToken(), String.valueOf(editText_phone_to_ipaddress.getText()), TestMainActivity.this, new AsyncResponse() {
                    @Override
                    public void processFinish(String rawFormat, boolean result, Vector<String> values) {
                        Toast.makeText(TestMainActivity.this, rawFormat, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });


        //───────────────────────────────────────────
        // 화면이동
        //───────────────────────────────────────────
        //로그인
        findViewById(R.id.btn_goto_login).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TestMainActivity.this, TestLoginActivity.class));

            }
        });

        //RTP+GSM <-> Server (Mr.cho's)
        findViewById(R.id.btn_goto_rtptest).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TestMainActivity.this, TestRtpGsmToServerActivity.class));

            }
        });

        //다이얼링
        findViewById(R.id.btn_goto_dialler).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(TestMainActivity.this, TestDialerActivity.class));
            }
        });

        //------------------------------------------------
        //  녹음기능에 대한 권한을 얻는다.
        //------------------------------------------------
        if (checkSelfPermission(Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {
            permissionToRecordAccepted = true;
            Log.e(LOG_TAG, "Permission To Record Audio Granted");
        } else {
            ActivityCompat.requestPermissions(this, permissions, REQUEST_RECORD_AUDIO_PERMISSION);
        }


        //------------------------------------------------
        //  OPEN API 관련 IP세팅
        //------------------------------------------------
        final EditText editTextIpAddress = findViewById(R.id.editText_ipaddress_to_change);

        findViewById(R.id.button_ipChange_to_local).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                editTextIpAddress.setText("http://10.0.3.2:8080");
                Config.setServerUrl(String.valueOf(editTextIpAddress.getText()));
            }
        });

        findViewById(R.id.button_ipChange_to_cloud).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editTextIpAddress.setText("https://cmu-2018-webservice.herokuapp.com");
                Config.setServerUrl(String.valueOf(editTextIpAddress.getText()));
            }
        });


        editTextIpAddress.setText(Config.getServerUrl());
        findViewById(R.id.button_ipChange_apply).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Config.setServerUrl(String.valueOf(editTextIpAddress.getText()));
            }
        });


        //---------------------------------------------------
        // SMS보내기 테스트
        //---------------------------------------------------
        Button buttonSMS = findViewById(R.id.btn_goto_sendsms);
        buttonSMS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(TestMainActivity.this, TestSendSMSMessageActivity.class);
                startActivity(intent1);
            }
        });


        //---------------------------------------------------
        // 패스워드 변경 테스트. (패스워드 변경전 로그인이 되어있어야한다.)
        //---------------------------------------------------
        Button btn_changePassword = findViewById(R.id.button_changePassword);
        btn_changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(TestMainActivity.this, TestChangePassword.class);
                startActivity(intent1);
            }
        });


    }

    // Requesting permission to RECORD_AUDIO
    private boolean permissionToRecordAccepted = false;
    private final String[] permissions = {Manifest.permission.RECORD_AUDIO};
    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case REQUEST_RECORD_AUDIO_PERMISSION:
                if (grantResults.length > 0 && permissions.length == grantResults.length)
                    permissionToRecordAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                Log.e(LOG_TAG, "Request for Permission To Record Audio Granted");
                break;
        }
        if (!permissionToRecordAccepted) {
            Log.e(LOG_TAG, "Request for Permission To Record Audio Not Granted");
            finish();
        }
    }


}
