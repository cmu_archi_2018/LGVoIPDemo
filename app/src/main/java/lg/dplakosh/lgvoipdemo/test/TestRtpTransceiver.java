package lg.dplakosh.lgvoipdemo.test;

import android.content.Context;
import android.media.AudioManager;
import android.net.rtp.AudioCodec;
import android.net.rtp.AudioGroup;
import android.net.rtp.AudioStream;
import android.net.rtp.RtpStream;
import android.os.StrictMode;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;

import lg.dplakosh.lgvoipdemo.utils.Logger;

/**
 * 참고: https://stackoverflow.com/questions/30948346/voip-rtp-streaming-from-to-server-in-java-to-from-android and so on
 */

class TestRtpTransceiver {

    private final static String TAG = TestRtpGsmToServerActivity.class
            .getSimpleName();
    private AudioStream audioStream;
    private final AudioGroup audioGroup;

    private int localPort;

    public int getLocalPort() {
        return localPort;
    }

    /**
     * @param context 안드로이드의 어플리케이션 문맥
     * @brief
     */
    public TestRtpTransceiver(Context context) {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitNetwork().build();
        StrictMode.setThreadPolicy(policy);
        AudioManager audio = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
        audio.setMode(AudioManager.MODE_IN_COMMUNICATION);
        audio.setSpeakerphoneOn(true);
        audio.setMicrophoneMute(false);
        audioGroup = new AudioGroup();
        audioGroup.setMode(AudioGroup.MODE_NORMAL);
        InetAddress inetAddress;
        try {
            audioStream = new AudioStream(InetAddress.getByAddress(getLocalIPAddress()));
            audioStream.setCodec(AudioCodec.GSM);
            audioStream.setMode(RtpStream.MODE_NORMAL);
        } catch (SocketException e) {
            Logger.e(TAG, e);
        } catch (UnknownHostException e) {
            Logger.e(TAG, e);
        } catch (Exception e) {
            Logger.e(TAG, e);
        }
    }

    /**
     * 해당 ipaddress:port 로 전화를 건다.
     *
     * @param ipaddress
     * @param port
     */
    public void call(String ipaddress, int port) {
        try {
            InetAddress inetAddressRemote = InetAddress.getByName(ipaddress);
            audioStream.associate(inetAddressRemote, port);
            audioStream.join(audioGroup);

            localPort = audioStream.getLocalPort();
        } catch (UnknownHostException e) {
            Logger.e(TAG, e);
        } catch (Exception e) {
            Logger.e(TAG, e);
        }
    }

    /**
     * 로컬 ip 어드레스를 반환해준다.
     *
     * @return
     */
    private static byte[] getLocalIPAddress() {
        byte ip[] = null;
        try {
            for (Enumeration en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements(); ) {
                NetworkInterface intf = (NetworkInterface) en.nextElement();
                for (Enumeration enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements(); ) {
                    InetAddress inetAddress = (InetAddress) enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        ip = inetAddress.getAddress();
                    }
                }
            }
        } catch (Exception ex) {
            Logger.e(TAG, ex);

        }
        return ip;
    }
}
