package lg.dplakosh.lgvoipdemo.test;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Vector;

import lg.dplakosh.lgvoipdemo.R;
import lg.dplakosh.lgvoipdemo.net.AsyncResponse;
import lg.dplakosh.lgvoipdemo.net.HttpUtils;
import lg.dplakosh.lgvoipdemo.utils.Config;

public class TestRegisterActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_activity_register);

        // Example of a call to a native method
        final EditText editText_email = findViewById(R.id.editText_email);
        final EditText editText_password = findViewById(R.id.editText_password);
        final EditText editText_cardNo = findViewById(R.id.editText_cardNo);
        final EditText editText_expiration = findViewById(R.id.editText_expiration);
        final EditText editText_address = findViewById(R.id.editText_address);
        final EditText editText_verificationCode = findViewById(R.id.editText_verficiationCode);
        Button button_signup = findViewById(R.id.button_sign_up);
        button_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String email = editText_email.getText().toString();
                final String plainPassword = editText_password.getText().toString();
                final String verificationCode = editText_verificationCode.getText().toString();
                final String address = editText_address.getText().toString();
                final String cardNo = editText_cardNo.getText().toString();
                final String expirationDate = editText_expiration.getText().toString();

                if (email.isEmpty() || plainPassword.isEmpty()) {
                    Toast.makeText(TestRegisterActivity.this, "Please input valid email or password", Toast.LENGTH_SHORT).show();
                }

                //─────────────────────────────────────────
                //UserScenario 1-1. 사용자 등록을 한다.
                //─────────────────────────────────────────
                String string = HttpUtils.requestRegister(email, plainPassword, address, cardNo, expirationDate, verificationCode, TestRegisterActivity.this, new AsyncResponse() {
                    @Override
                    public void processFinish(String rawFormat, boolean result, Vector<String> values) {
                        Toast.makeText(TestRegisterActivity.this, rawFormat, Toast.LENGTH_SHORT).show();

                        if(result) //사용자 등록 성공
                        {
                            Config.setPhoneNo(values.elementAt(0));
                            Config.setEmail(email);
                            Config.setPassword(plainPassword);
                            Intent intent = new Intent(TestRegisterActivity.this, TestLoginActivity.class);
                            startActivity(intent);
                        }
                        else
                        {
                            Toast.makeText(TestRegisterActivity.this, values.elementAt(0), Toast.LENGTH_SHORT).show();
                        }
                    }
                });


            }
        });
    }
}
