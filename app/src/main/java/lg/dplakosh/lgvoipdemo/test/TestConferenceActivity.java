package lg.dplakosh.lgvoipdemo.test;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Vector;

import lg.dplakosh.lgvoipdemo.R;
import lg.dplakosh.lgvoipdemo.net.AsyncResponse;
import lg.dplakosh.lgvoipdemo.net.HttpUtils;
import lg.dplakosh.lgvoipdemo.utils.Config;

public class TestConferenceActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_activity_change_password);

        // Example of a call to a native method
        final EditText editText_oldPassword = findViewById(R.id.editText_oldPassword);
        final EditText editText_newPassword = findViewById(R.id.editText_newPassword);
        final EditText editText_confirmPassword= findViewById(R.id.editText_confirm_newPassword);

        Button button_login = findViewById(R.id.button_confirm_change_password);
        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String oldPassword = editText_oldPassword.getText().toString();
                final String newPassword = editText_newPassword.getText().toString();
                final String confirmPassword = editText_confirmPassword.getText().toString();


                if(!newPassword.equals(confirmPassword))
                {
                    Toast.makeText(TestConferenceActivity.this, "Confirm your new password again.", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (oldPassword.isEmpty() || newPassword.isEmpty()) {
                    Toast.makeText(TestConferenceActivity.this, "invalid password", Toast.LENGTH_SHORT).show();
                    return;
                }
                String string = HttpUtils.changeUserPassword(Config.getToken(), Config.getEmail(), oldPassword, newPassword, TestConferenceActivity.this, new AsyncResponse() {
                    @Override
                    public void processFinish(String rawFormat, boolean result, Vector<String> values) {
                        Toast.makeText(TestConferenceActivity.this, rawFormat, Toast.LENGTH_SHORT).show();

                        if (result) {
                            //성공시 새로운 패스워드를 저장한다.
                            Config.setPassword(newPassword);
                            Intent intent = new Intent(TestConferenceActivity.this, TestDialerActivity.class);
                            startActivity(intent);
                        } else {
                            //로그인실패할경우
                        }
                    }
                });
            }
        });
    }
}