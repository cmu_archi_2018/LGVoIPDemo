package lg.dplakosh.lgvoipdemo.test;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.widget.TextView;

import lg.dplakosh.lgvoipdemo.R;


public class TestNewCallCommingActivity extends Activity implements View.OnClickListener {
    private LocalBroadcastManager mLocalBroadcastManager;
    public static final String ACTION_ACCEPTED = "edu.cmu.sevenhours.voice.ACCEPTED";
    public static final String ACTION_REJECTED = "edu.cmu.sevenhours.voice.REJECTED";

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mLocalBroadcastManager = LocalBroadcastManager.getInstance(this);
        setContentView(R.layout.test_new_incoming_activity);

        Bundle localBundle = getIntent().getExtras();
        String target = ((String)localBundle.get("target"));
        ((TextView)findViewById(R.id.new_call_ip)).setText(target);

        findViewById(R.id.accept).setOnClickListener(this);
        findViewById(R.id.reject).setOnClickListener(this);
    }

    public void onClick(View view) {
        Intent intent = new Intent("");
        switch(view.getId()) {
            case R.id.accept:
                intent = new Intent(ACTION_ACCEPTED);
                break;
            case R.id.reject:
                intent = new Intent(ACTION_REJECTED);
                break;
        }
        mLocalBroadcastManager.sendBroadcast(intent);
        finish();
    }

}
