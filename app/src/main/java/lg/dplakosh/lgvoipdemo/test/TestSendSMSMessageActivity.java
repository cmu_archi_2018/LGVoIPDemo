package lg.dplakosh.lgvoipdemo.test;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;
import java.util.Vector;

import lg.dplakosh.lgvoipdemo.R;
import lg.dplakosh.lgvoipdemo.net.AsyncResponse;
import lg.dplakosh.lgvoipdemo.net.HttpUtils;
import lg.dplakosh.lgvoipdemo.utils.Config;


public class TestSendSMSMessageActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_activity_sms);

        TextView textView_myPhoneNo = findViewById(R.id.textView_myphonenumber);
        textView_myPhoneNo.setText(Config.getPhoneNo());

        // Example of a call to a native method
        final EditText editText_send_phoneno = findViewById(R.id.editText_send_phoneno);

        final EditText editText_send_sms = findViewById(R.id.editText_send_sms);
        Button button_signup = findViewById(R.id.button_send_sms);
        button_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String phonNumbers = String.valueOf(editText_send_phoneno.getText());
                String[] items = phonNumbers.split(",");
                Vector<String> vecPhoneNumbers = new Vector<>(Arrays.asList(items));
                HttpUtils.sendSMSMessage(Config.getToken(), Config.getPhoneNo(), vecPhoneNumbers, String.valueOf(editText_send_sms.getText()), new AsyncResponse() {
                    @Override
                    public void processFinish(String rawFormat, boolean result, Vector<String> values) {
                        if (result) {
                            editText_send_sms.setText("");
                        }
                        Toast.makeText(TestSendSMSMessageActivity.this, values.elementAt(0), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}
