package lg.dplakosh.lgvoipdemo.test;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.security.NoSuchAlgorithmException;
import java.util.Vector;

import lg.dplakosh.lgvoipdemo.R;
import lg.dplakosh.lgvoipdemo.net.AsyncResponse;
import lg.dplakosh.lgvoipdemo.net.HttpUtils;
import lg.dplakosh.lgvoipdemo.utils.Config;
import lg.dplakosh.lgvoipdemo.utils.Helper;

public class TestLoginActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_activity_login);

        // Example of a call to a native method
        final EditText editText_email = findViewById(R.id.editText_email);
        final EditText editText_password = findViewById(R.id.editText_password);

        //이미 계정정보가 있으면 자동채우기 한다.
        editText_email.setText(Config.getEmail());
        editText_password.setText(Config.getPassword());

        Button button_login = findViewById(R.id.button_login);
        button_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String email = editText_email.getText().toString();
                final String plainPassword = editText_password.getText().toString();

                if (email.isEmpty() || plainPassword.isEmpty()) {
                    Toast.makeText(TestLoginActivity.this, "Please input valid email or password", Toast.LENGTH_SHORT).show();
                }

                String dig="";
                try {
                    dig = Helper.SHA256(plainPassword);
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                }
                final String digestedPassword = dig;
                //─────────────────────────────────────────
                //UserScenario 1-1. 로그인을 한다.
                //─────────────────────────────────────────
                String string = HttpUtils.requestLogin(email, digestedPassword, TestLoginActivity.this , new AsyncResponse() {
                    @Override
                    public void processFinish(String rawFormat, boolean result, Vector<String> values) {
                        //─────────────────────────────────────────
                        //1-2. 로그인 결과에 따라 화면을 넘기거나, 등록되지 않은 아이디, 잘못된 아이디 또는 패스워드등의 경고문구를 띄운다.
                        //─────────────────────────────────────────
                        Toast.makeText(TestLoginActivity.this, rawFormat, Toast.LENGTH_SHORT).show();
                        //─────────────────────────────────────────
                        //1-3. 로그인이 성공하면 SharedPreference등으로 기기 내부에 저장한다. (다음로그인시 자동으로 로그인되게끔)
                        //─────────────────────────────────────────
                        if(result)
                        {
                            //─────────────────────────────────────────
                            //1-4. 다이얼화면으로 이동한다.
                            //─────────────────────────────────────────

                            //토큰을 저장하고
                            Config.setToken(values.elementAt(0));
                            Config.setEmail(email);
                            Config.setPassword(plainPassword);
                            Config.setPhoneNo(values.elementAt(1));
                            //다른화면으로 이동
                            Intent intent = new Intent(TestLoginActivity.this, TestDialerActivity.class);
                            startActivity(intent);
                        }
                        else
                        {
                            //로그인실패할경우
                            Toast.makeText(TestLoginActivity.this, values.elementAt(0), Toast.LENGTH_SHORT).show();
                        }

                    }
                });
            }
        });

        Button button_signup = findViewById(R.id.button_sign_up);

        button_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TestLoginActivity.this, TestRegisterActivity.class);
                startActivity(intent);
            }
        });


        //─────────────────────────────────────────
        //UserScenario 2. 이미 로그인한 이력이 있으면 SharedPreference등을 읽어서 로그인을 시도한다.
        //─────────────────────────────────────────
        if (Config.isRegistered()) {

            String email = Config.getEmail();
            editText_email.setText(email);

            if (!Config.getPassword().isEmpty()) {
                String maybeDigestedPassword = Config.getPassword();
                editText_password.setText(maybeDigestedPassword);

                String string = HttpUtils.requestLogin(email, maybeDigestedPassword, TestLoginActivity.this , new AsyncResponse() {
                    @Override
                    public void processFinish(String rawFormat, boolean result, Vector<String> values) {
                        //─────────────────────────────────────────
                        //1-2. 로그인 결과에 따라 화면을 넘기거나, 등록되지 않은 아이디, 잘못된 아이디 또는 패스워드등의 경고문구를 띄운다.
                        //─────────────────────────────────────────
                        Toast.makeText(TestLoginActivity.this, rawFormat, Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(TestLoginActivity.this, TestDialerActivity.class);
                        startActivity(intent);
                    }
                });

            }
        }
//        tv.setText(stringFromJNI());
    }

}
