package lg.dplakosh.lgvoipdemo.db.contact;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class ContactHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "contactsManager";
    private static final String TABLE_CONTACTS = "contacts";
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_PH_NO = "phone_number";
    private static final String KEY_EMAIL = "email";


    public ContactHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //3rd argument to be passed is CursorFactory instance
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + this.TABLE_CONTACTS + "("
                + this.KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + this.KEY_NAME + " TEXT,"
                + this.KEY_PH_NO + " TEXT,"
                + this.KEY_EMAIL + " TEXT" + ");";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + this.TABLE_CONTACTS);

        // Create tables again
        onCreate(db);
    }

    // code to add the new contact
    public void addContact(Contact contact) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(this.KEY_NAME, contact.getName()); // Contact Name
        values.put(this.KEY_PH_NO, contact.getPhoneNumber()); // Contact Phone
        values.put(this.KEY_EMAIL, contact.getEmail()); // Contact Phone

        // Inserting Row
        db.insert(this.TABLE_CONTACTS, null, values);
        //2nd argument is String containing nullColumnHack
        db.close(); // Closing database connection
    }

    // code to get the single contact
    public Contact getContact(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(this.TABLE_CONTACTS, new String[] { this.KEY_ID,
                        this.KEY_NAME, this.KEY_PH_NO, this.KEY_EMAIL }, this.KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        // return contact
        return new Contact(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2), cursor.getString(3));
    }

    // code to get all contacts in a list view
    public List<Contact> getAllContacts() {
        List<Contact> contactList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + this.TABLE_CONTACTS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        DatabaseUtils.dumpCursorToString(cursor);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                try {
                    Contact contact = new Contact();
                    contact.setID(Integer.parseInt(cursor.getString(0)));
                    contact.setName(cursor.getString(1));
                    contact.setPhoneNumber(cursor.getString(2));
                    contact.setEmail(cursor.getString(3));
                    // Adding contact to list
                    contactList.add(contact);
                }
                catch(Exception e) {
                    Log.e("CHOI",Log.getStackTraceString(e));
                }
            } while (cursor.moveToNext());
        }
        // return contact list
        return contactList;
    }

    // code to update the single contact
    public int updateContact(Contact contact) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(this.KEY_NAME, contact.getName());
        values.put(this.KEY_PH_NO, contact.getPhoneNumber());
        values.put(this.KEY_EMAIL, contact.getEmail());

        // updating row
        return db.update(this.TABLE_CONTACTS, values, this.KEY_ID + " = ?",
                new String[] { String.valueOf(contact.getID()) });
    }

    // Deleting single contact
    public void deleteContact(Contact contact) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(this.TABLE_CONTACTS, this.KEY_ID + " = ?",
                new String[] { String.valueOf(contact.getID()) });
        db.close();
    }

    // Deleting single contact
    public void deleteContactUsingEmail(Contact contact) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(this.TABLE_CONTACTS, this.KEY_EMAIL + " = ?",
                new String[] { String.valueOf(contact.getEmail()) });
        db.close();
    }

    public void deleteDatabase(){
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            //db.execSQL("delete from " + this.DATABASE_NAME);
            db.delete(TABLE_CONTACTS, null, null);
            db.close();
        }
        catch(Exception e){
            Log.e("CHOI",Log.getStackTraceString(e));
        }
    }

    // Getting contacts Count
    public int getContactsCount() {
        String countQuery = "SELECT  * FROM " + this.TABLE_CONTACTS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }

    public long getPrimaryid(String name, String phone, String email){
        // Select All Query
        String selectQuery = "SELECT  * FROM " + this.TABLE_CONTACTS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        DatabaseUtils.dumpCursorToString(cursor);

        long returnid=0;

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                try {
                    Contact contact = new Contact();
                    contact.setID(Integer.parseInt(cursor.getString(0)));
                    contact.setName(cursor.getString(1));
                    contact.setPhoneNumber(cursor.getString(2));
                    contact.setEmail(cursor.getString(3));
                    String tmp_name = contact.getName();
                    String tmp_phone = contact.getPhoneNumber();
                    String tmp_email = contact.getEmail();

                    if((tmp_name.equals(name))
                            && (tmp_phone.equals(phone))
                            && (tmp_email.equals(email))){
                        returnid = contact.getID();
                        break;
                    }
                }
                catch(Exception e) {
                    Log.e("CHOI",Log.getStackTraceString(e));
                }
            } while (cursor.moveToNext());
        }
        // return contact ID
        return returnid;
    }

}
