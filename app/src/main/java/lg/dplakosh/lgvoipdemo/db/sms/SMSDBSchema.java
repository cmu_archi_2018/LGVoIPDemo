package lg.dplakosh.lgvoipdemo.db.sms;

import android.net.Uri;
import android.provider.BaseColumns;

public final class SMSDBSchema {
    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private SMSDBSchema() {}
    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + FeedEntry.TABLE_NAME + " (" +
                    FeedEntry._ID + " INTEGER PRIMARY KEY," +
                    FeedEntry.COLUMN_NAME_MESSAGE + " TEXT," +
                    FeedEntry.COLUMN_NAME_DATE + " TEXT," +
                    FeedEntry.COLUMN_NAME_PHONENO + " TEXT," +
                    FeedEntry.COLUMN_NAME_READORNOT + " TEXT);";

    public static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + FeedEntry.TABLE_NAME;

    /* Inner class that defines the table contents */
    public static class FeedEntry implements BaseColumns {
        public static final String TABLE_NAME = "SMSDATA";
        public static final String COLUMN_NAME_MESSAGE = "MESSAGE";
        public static final String COLUMN_NAME_DATE = "DATE";
        public static final String COLUMN_NAME_PHONENO = "PHONENO";
        public static final String COLUMN_NAME_READORNOT = "READORNOT"; //value: "READ" "NOTREAD"
    }

    // Provider specific constants
    public static final String AUTHORITY = "lg.dplakosh.lgvoipdemo.db.sms.SmsContentProvider";
    public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + FeedEntry.TABLE_NAME);
    public static final int MSG_ITEM = 1;
    public static final int MSG_DIR = 2;
    public static final String MSG_TYPE_ITEM =
            "vnd.android.cursor.item/lg.dplakosh.lgvoipdemo.db.sms.provider.msg";
    public static final String MSG_TYPE_DIR =
            "vnd.android.cursor.dir/lg.dplakosh.lgvoipdemo.db.sms.provider.msg";
}