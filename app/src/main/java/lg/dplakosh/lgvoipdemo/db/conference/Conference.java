package lg.dplakosh.lgvoipdemo.db.conference;

public class Conference {
    private int _id; //db자동증가

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getConferencePhoneNo() {
        return conferencePhoneNo;
    }

    public void setConferencePhoneNo(String conferencePhoneNo) {
        this.conferencePhoneNo = conferencePhoneNo;
    }

    public String getHostPhoneNo() {
        return hostPhoneNo;
    }

    public void setHostPhoneNo(String COLUMN_NAME_hostPhoneNo) {
        this.hostPhoneNo = COLUMN_NAME_hostPhoneNo;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    private String uniqueId;
    private String conferencePhoneNo;
    private String hostPhoneNo;
    private String subject;
    private String startDate;
    private String endDate;
    private String guestPhoneNo;
    private String requestDate;

    public String getRequestDate() {
        return requestDate;
    }

    public void setRequestDate(String requestDate) {
        this.requestDate = requestDate;
    }


    public String getGuestPhoneNo() {
        return guestPhoneNo;
    }

    public void setGuestPhoneNo(String guestPhoneNo) {
        this.guestPhoneNo = guestPhoneNo;
    }

    public Conference() {
    }

    public Conference(int _id, String COLUMN_NAME_UNIQUEID, String COLUMN_NAME_conferencePhoneNo, String COLUMN_NAME_hostPhoneNo, String COLUMN_NAME_subject, String COLUMN_NAME_startDate, String COLUMN_NAME_endDate, String COLUMN_NAME_guestPhoneNo, String requestDate) {
        this._id = _id;
        this.uniqueId = COLUMN_NAME_UNIQUEID;
        this.conferencePhoneNo = COLUMN_NAME_conferencePhoneNo;
        this.hostPhoneNo = COLUMN_NAME_hostPhoneNo;
        this.subject = COLUMN_NAME_subject;
        this.startDate = COLUMN_NAME_startDate;
        this.guestPhoneNo = COLUMN_NAME_guestPhoneNo;
        this.requestDate = requestDate;

    }


}
