package lg.dplakosh.lgvoipdemo.db.conference;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class ConferenceSQLiteDatabaseHelper extends SQLiteOpenHelper {
    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 4;
    private static final String DATABASE_NAME = "conference.db";

    public ConferenceSQLiteDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        db.execSQL(ConferenceDBSchema.SQL_CREATE_ENTRIES);
    }

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(ConferenceDBSchema.SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    // code to get all contacts in a list view
    public List<Conference> getAll() {
        List<Conference> contactList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + ConferenceDBSchema.FeedEntry.TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Conference contact = new Conference();
                int i = 0;
                contact.set_id(Integer.parseInt(cursor.getString(i++)));
                contact.setUniqueId(cursor.getString(i++));
                contact.setConferencePhoneNo(cursor.getString(i++));
                contact.setHostPhoneNo(cursor.getString(i++));
                contact.setSubject(cursor.getString(i++));
                contact.setStartDate(cursor.getString(i++));
                contact.setEndDate(cursor.getString(i++));
                contact.setGuestPhoneNo(cursor.getString(i++));
                contact.setRequestDate(cursor.getString(i++));

                // Adding contact to list
                contactList.add(contact);
            } while (cursor.moveToNext());
        }
        // return contact list
        return contactList;
    }


    // Deleting single contact
    public void deleteByUniqueId(String uniqueId) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(ConferenceDBSchema.FeedEntry.TABLE_NAME, ConferenceDBSchema.FeedEntry.COLUMN_NAME_UNIQUEID + " = ?",
                new String[] { uniqueId });
        db.close();
    }

}