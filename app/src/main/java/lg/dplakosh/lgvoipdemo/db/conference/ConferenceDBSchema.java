package lg.dplakosh.lgvoipdemo.db.conference;

import android.provider.BaseColumns;

public final class ConferenceDBSchema {
    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private ConferenceDBSchema() {
    }

    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + FeedEntry.TABLE_NAME + " (" +
                    FeedEntry._ID + " INTEGER PRIMARY KEY," +
                    FeedEntry.COLUMN_NAME_UNIQUEID + " TEXT unique," +
                    FeedEntry.COLUMN_NAME_conferencePhoneNo + " TEXT," +
                    FeedEntry.COLUMN_NAME_hostPhoneNo + " TEXT," +
                    FeedEntry.COLUMN_NAME_subject + " TEXT," +
                    FeedEntry.COLUMN_NAME_startDate + " TEXT," +
                    FeedEntry.COLUMN_NAME_endDate + " TEXT," +
                    FeedEntry.COLUMN_NAME_guestPhoneNo + " TEXT," +
                    FeedEntry.COLUMN_NAME_requestDate + " TEXT)";

    public static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + FeedEntry.TABLE_NAME;

    /* Inner class that defines the table contents */
    public static class FeedEntry implements BaseColumns {
        public static final String TABLE_NAME = "CONFERENCEDATA";
        public static final String COLUMN_NAME_UNIQUEID = "UNIQUEID";
        public static final String COLUMN_NAME_conferencePhoneNo = "CONFERENCEPHONENO";
        public static final String COLUMN_NAME_hostPhoneNo = "HOSTPHONENO";
        public static final String COLUMN_NAME_subject = "SUBJECT";
        public static final String COLUMN_NAME_startDate = "STARTDATE";
        public static final String COLUMN_NAME_endDate = "ENDDATE";
        public static final String COLUMN_NAME_guestPhoneNo = "GUESTPHONENO";
        public static final String COLUMN_NAME_requestDate = "REQUESTDATE";


    }

}