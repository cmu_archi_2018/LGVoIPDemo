package lg.dplakosh.lgvoipdemo.db.sms;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

public class SmsContentProvider extends ContentProvider {

    private static final String TAG = "voip|SmsContentProvider";

    private SMSSQLiteDatabaseHelper dbHelper;
    private static final UriMatcher sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        sURIMatcher.addURI(SMSDBSchema.AUTHORITY, SMSDBSchema.FeedEntry.TABLE_NAME,
                SMSDBSchema.MSG_DIR);
        sURIMatcher.addURI(SMSDBSchema.AUTHORITY, SMSDBSchema.FeedEntry.TABLE_NAME
                + "/#", SMSDBSchema.MSG_ITEM);
    }
    @Override
    public boolean onCreate() {
        dbHelper = new SMSSQLiteDatabaseHelper(getContext());
        Log.d(TAG, "onCreated");
        return true;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        switch (sURIMatcher.match(uri)) {
            case SMSDBSchema.MSG_DIR:
                return SMSDBSchema.MSG_TYPE_DIR;
            case SMSDBSchema.MSG_ITEM:
                return SMSDBSchema.MSG_TYPE_ITEM;
            default:
                throw new IllegalArgumentException("Illegal URI: " + uri);
        }
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        Uri ret = null;

        if (sURIMatcher.match(uri) != SMSDBSchema.MSG_DIR) {
            throw new IllegalArgumentException("Illegal uri: " + uri);
        }

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        long rowId = db.insertWithOnConflict(SMSDBSchema.FeedEntry.TABLE_NAME, null,
                values, SQLiteDatabase.CONFLICT_IGNORE);

        if (rowId != -1) {
            ret = ContentUris.withAppendedId(uri, rowId);
            Log.d(TAG, "inserted uri: " + ret);

            getContext().getContentResolver().notifyChange(uri, null);
        }
        return ret;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }
}
