package lg.dplakosh.lgvoipdemo.db.sms;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class SMSSQLiteDatabaseHelper extends SQLiteOpenHelper {
    // If you change the database schema, you must increment the database version.
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "smsdata.db";
    public SMSSQLiteDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SMSDBSchema.SQL_CREATE_ENTRIES);
    }
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SMSDBSchema.SQL_DELETE_ENTRIES);
        onCreate(db);
    }
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    // code to get all contacts in a list view
    public List<SMS> getAllSMSs() {
        List<SMS> smsList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + SMSDBSchema.FeedEntry.TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                try {
                    SMS sms = new SMS();
                    sms.set_id(Integer.parseInt(cursor.getString(0)));
                    sms.set_message(cursor.getString(1));
                    sms.set_date(cursor.getString(2));
                    sms.set_phone_number(cursor.getString(3));
                    sms.set_readornot(cursor.getString(4));
                    // Adding contact to list
                    smsList.add(sms);
                }
                catch(Exception e){
                        Log.e("CHOI",Log.getStackTraceString(e));
                }
            } while (cursor.moveToNext());
        }
        // return contact list
        return smsList;
    }

    // Getting contacts Count
    public int getSMSsCount() {
        String countQuery = "SELECT  * FROM " + SMSDBSchema.FeedEntry.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();

        // return count
        return cursor.getCount();
    }

    public int updateSMS(SMS sms) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(SMSDBSchema.FeedEntry.COLUMN_NAME_MESSAGE, sms.get_message());
        values.put(SMSDBSchema.FeedEntry.COLUMN_NAME_DATE, sms.get_date());
        values.put(SMSDBSchema.FeedEntry.COLUMN_NAME_PHONENO, sms.get_phone_number());
        values.put(SMSDBSchema.FeedEntry.COLUMN_NAME_READORNOT, sms.get_readornot());

        // updating row
        return db.update(SMSDBSchema.FeedEntry.TABLE_NAME, values, SMSDBSchema.FeedEntry._ID + " = ?",
                new String[] { String.valueOf(sms.get_id()) });
    }

    // Deleting single contact
    public void deleteSMS(SMS sms) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(SMSDBSchema.FeedEntry.TABLE_NAME, SMSDBSchema.FeedEntry._ID + " = ?",
                new String[] { String.valueOf(sms.get_id()) });
        db.close();
    }

    public void deleteDatabase() {
        try{
        SQLiteDatabase db = this.getWritableDatabase();
        //db.execSQL("delete from " + DATABASE_NAME);
        db.delete(SMSDBSchema.FeedEntry.TABLE_NAME, null, null);
        db.close();
        }
        catch(Exception e){
            Log.e("CHOI",Log.getStackTraceString(e));
        }
    }

    // Deleting single contact
    public void deleteSMSUsingInfo(SMS sms) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(SMSDBSchema.FeedEntry.TABLE_NAME, SMSDBSchema.FeedEntry.COLUMN_NAME_DATE + " = ?",
                new String[] { String.valueOf(sms.get_date()) });
        db.close();
    }

    // code to get the single contact
    public SMS getSMS(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(SMSDBSchema.FeedEntry.TABLE_NAME, new String[] { SMSDBSchema.FeedEntry._ID,
                        SMSDBSchema.FeedEntry.COLUMN_NAME_MESSAGE,SMSDBSchema.FeedEntry.COLUMN_NAME_DATE,SMSDBSchema.FeedEntry.COLUMN_NAME_PHONENO,SMSDBSchema.FeedEntry.COLUMN_NAME_READORNOT }, SMSDBSchema.FeedEntry._ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        // return contact
        return new SMS(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4));
    }

    // code to add the new contact
    public void addSMS(SMS sms) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(SMSDBSchema.FeedEntry.COLUMN_NAME_MESSAGE, sms.get_message()); // Contact Phone
        values.put(SMSDBSchema.FeedEntry.COLUMN_NAME_DATE, sms.get_date()); // Contact Name
        values.put(SMSDBSchema.FeedEntry.COLUMN_NAME_PHONENO, sms.get_phone_number()); // Contact Phone
        values.put(SMSDBSchema.FeedEntry.COLUMN_NAME_READORNOT, sms.get_readornot()); // Contact Phone

        // Inserting Row
        db.insert(SMSDBSchema.FeedEntry.TABLE_NAME, null, values);
        //2nd argument is String containing nullColumnHack
        db.close(); // Closing database connection
    }


    public long getPrimaryid(String date, String phone, String message, String readornot){
        // Select All Query
        String selectQuery = "SELECT  * FROM " + SMSDBSchema.FeedEntry.TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        DatabaseUtils.dumpCursorToString(cursor);

        long returnid=0;

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                try {
                    SMS sms = new SMS();
                    sms.set_id(Integer.parseInt(cursor.getString(0)));
                    sms.set_message(cursor.getString(1));
                    sms.set_date(cursor.getString(2));
                    sms.set_phone_number(cursor.getString(3));
                    sms.set_readornot(cursor.getString(4));
                    String tmp_date = sms.get_date();
                    String tmp_phone = sms.get_phone_number();
                    String tmp_message = sms.get_message();
                    String tmp_readornot = sms.get_readornot();

                    if((tmp_date.equals(date))
                            && (tmp_phone.equals(phone))
                            && (tmp_message.equals(message))){
                        returnid = sms.get_id();
                        break;
                    }
                }
                catch(Exception e) {
                    Log.e("CHOI",Log.getStackTraceString(e));
                }
            } while (cursor.moveToNext());
        }
        // return contact ID
        return returnid;
    }

}