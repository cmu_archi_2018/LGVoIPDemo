package lg.dplakosh.lgvoipdemo.db.sms;

public class SMS {
    private int _id;
    private String _date;
    private String _phone_number;
    private String _message;
    private String _readornot;
    public SMS(){   }
    public SMS(int id, String _date, String _phone_number, String _message, String _readornot){
        this._id = id;
        this._date = _date;
        this._phone_number = _phone_number;
        this._message = _message;
        this._readornot = _readornot;
    }

    public SMS(String _date, String _phone_number, String _message, String _readornot){
        this._date = _date;
        this._phone_number = _phone_number;
        this._message = _message;
        this._readornot = _readornot;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public void set_date(String _date) {
        this._date = _date;
    }

    public void set_phone_number(String _phone_number) {
        this._phone_number = _phone_number;
    }

    public void set_message(String _message) {
        this._message = _message;
    }

    public void set_readornot(String _readornot) {
        this._readornot = _readornot;
    }

    public int get_id() {
        return _id;
    }

    public String get_date() {
        return _date;
    }

    public String get_phone_number() {
        return _phone_number;
    }

    public String get_message() {
        return _message;
    }

    public String get_readornot() {
        return _readornot;
    }
}
