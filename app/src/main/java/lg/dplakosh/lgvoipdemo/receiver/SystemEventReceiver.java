package lg.dplakosh.lgvoipdemo.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import lg.dplakosh.lgvoipdemo.ui.VoipPhoneNotifier;
import lg.dplakosh.lgvoipdemo.utils.Config;

public class SystemEventReceiver extends BroadcastReceiver {

    private static final String TAG = "voip|SystemEventRv";

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive() - " + intent.getAction());

        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)
                || intent.getAction().equals(Intent.ACTION_MY_PACKAGE_REPLACED)) {

            Config.init(context.getApplicationContext());
            VoipPhoneNotifier.init(context.getApplicationContext());
            VoipPhoneNotifier.getInstance().updateVoipPhoneNotification();
        }
    }
}
