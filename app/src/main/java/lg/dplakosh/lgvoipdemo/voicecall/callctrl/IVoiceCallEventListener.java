package lg.dplakosh.lgvoipdemo.voicecall.callctrl;

public interface IVoiceCallEventListener {

    void started();
    void startFailed(int reason, int code);
    void progressing();
    void terminated(int reason);

    void serviceNotReady();
    void serviceReady();
}
