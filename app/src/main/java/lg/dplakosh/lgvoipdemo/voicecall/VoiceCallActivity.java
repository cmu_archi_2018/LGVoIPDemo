package lg.dplakosh.lgvoipdemo.voicecall;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.KeyguardManager;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.annotation.UiThread;
import android.telephony.PhoneNumberUtils;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import lg.dplakosh.lgvoipdemo.R;
import lg.dplakosh.lgvoipdemo.VoiceCallApp;
import lg.dplakosh.lgvoipdemo.db.contact.Contact;
import lg.dplakosh.lgvoipdemo.db.contact.ContactHandler;
import lg.dplakosh.lgvoipdemo.utils.Config;
import lg.dplakosh.lgvoipdemo.utils.Logger;
import lg.dplakosh.lgvoipdemo.voicecall.callctrl.CallTerminateReason;
import lg.dplakosh.lgvoipdemo.voicecall.callctrl.IVoiceCall;
import lg.dplakosh.lgvoipdemo.voicecall.callctrl.IVoiceCallEventListener;
import lg.dplakosh.lgvoipdemo.voicecall.callctrl.VoiceCallControl;

//https://stackoverflow.com/questions/30246425/turning-on-screen-from-receiver-service/30365638#30365638


//TODO 근접센서 스크린 온오프 관련하여 이걸로 바꿔라
// https://android.googlesource.com/platform/packages/services/Telecomm/+/lollipop-dev/src/com/android/server/telecom/ProximitySensorManager.java

public class VoiceCallActivity extends Activity implements IVoiceCallEventListener, VoiceCallTimer.OnTickListener, SensorEventListener {

    private static final String TAG = "voip|VoiceCallActivity";
    private static final String EXTRA_INCOMING_CALL = "com.lge.callsvc.extra.INCOMING_CALL";
    private static final String EXTRA_SESSION_KEY = "com.lge.callsvc.extra.SESSION_KEY";

    private TextView nameTextView;
    private TextView phoneNumerTextView;
    private TextView elapsedTimeTextView;
    private TextView callStateLabelTextView;
    private VoiceCallTouchUi mVoiceCallTouchUi;

    private IVoiceCall mCall;
    private VoiceCallTimer mCallTimer;
    private long mPrevDuration = 0;
    private boolean mIsIncomingCall;

    // Screen-of by Proximity
    private SensorManager mSensorManager;
    private Sensor mProximity;
    private static final int SENSOR_SENSITIVITY = 4;
    private MediaPlayer mMediaPlayer;

    private PowerManager mPowerManager;
    private PowerManager.WakeLock mProximityWakeLock;
    private AudioManager mAudioManager;

    public void turnOffScreen() {
        // turn on screen
        Log.v("ProximityActivity", "ON!");

        if (mProximityWakeLock == null) {
            return;
        }
        if (!mProximityWakeLock.isHeld()) {
            Log.i(TAG, "Acquiring proximity wake lock");
            mProximityWakeLock.acquire();
        } else {
            Log.i(TAG, "Proximity wake lock already acquired");
        }

    }

    @TargetApi(21) //Suppress lint error for PROXIMITY_SCREEN_OFF_WAKE_LOCK
    public void turnOnScreen() {
        // turn off screen
        Log.v("ProximityActivity", "OFF!");
        if (mProximityWakeLock == null) {
            return;
        }
        if (mProximityWakeLock.isHeld()) {
            Log.i(TAG, "Releasing proximity wake lock");
            int flags =
                    PowerManager.RELEASE_FLAG_WAIT_FOR_NO_PROXIMITY;
            mProximityWakeLock.release(flags);
        } else {
            Log.i(TAG, "Proximity wake lock already released");
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPowerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        PowerManager pm = (PowerManager) getApplicationContext().getSystemService(Context.POWER_SERVICE);
        if (pm.isWakeLockLevelSupported(PowerManager.PROXIMITY_SCREEN_OFF_WAKE_LOCK)) {
            mProximityWakeLock = pm.newWakeLock(
                    PowerManager.PROXIMITY_SCREEN_OFF_WAKE_LOCK, TAG);
        } else {
            mProximityWakeLock = null;
        }

        String number = PhoneNumberUtils.getNumberFromIntent(getIntent(), getApplicationContext());
        boolean isIncomingCall = getIntent().getBooleanExtra(EXTRA_INCOMING_CALL, false);
        int sessionKey = getIntent().getIntExtra(EXTRA_SESSION_KEY, 0);

        Log.d(TAG, "number - " + number);
        Log.d(TAG, "onCreate - " + isIncomingCall);
        Log.d(TAG, "sessionKey - " + sessionKey);

        setContentView(R.layout.activity_voicecall);

        nameTextView = findViewById(R.id.name);
        phoneNumerTextView = findViewById(R.id.phoneNumber);
        elapsedTimeTextView = findViewById(R.id.elapsedTime);
        callStateLabelTextView = findViewById(R.id.callStateLabel);

        if (!TextUtils.isEmpty(number)) {
            nameTextView.setVisibility(View.VISIBLE);
            searchContactName(number);
        }
        else {
            nameTextView.setVisibility(View.GONE);
        }

        phoneNumerTextView.setText(number);
        elapsedTimeTextView.setText("00:00");

        callStateLabelTextView.setVisibility(View.VISIBLE);
        callStateLabelTextView.setText(isIncomingCall ? "Incoming call" : "Dialing");

        mVoiceCallTouchUi = findViewById(R.id.CallTouchUi);

        mIsIncomingCall = isIncomingCall;

        if (isIncomingCall) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
                setShowWhenLocked(true);
                setTurnScreenOn(true);
                KeyguardManager keyguardManager = (KeyguardManager) getSystemService(Context.KEYGUARD_SERVICE);
                keyguardManager.requestDismissKeyguard(this, null);
            } else {
                getWindow().addFlags(WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                        WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                        WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
            }

            mVoiceCallTouchUi.showIncomingCallButtons();
            mVoiceCallTouchUi.StartRinger();

            //if (mCall == null) {
            if (VoiceCallControl.getInstance().attachCall(sessionKey, this)) {
                mCall = VoiceCallControl.getInstance().getForegroundCall();
                mCallTimer = new VoiceCallTimer(mCall, this);
            }
            //}
        } else {
            Log.d(TAG, "onCreate - startCall... - isIncoming : " + isIncomingCall);

            mVoiceCallTouchUi.showIncallButtons();

            //if (mCall == null) {
            if (VoiceCallControl.getInstance().startCall(number, this)) {
                mCall = VoiceCallControl.getInstance().getForegroundCall();
                mCallTimer = new VoiceCallTimer(mCall, this);

                Log.d(TAG, "onCreate - startCall ok");
            } else {
                // TODO
                Log.d(TAG, "onCreate - startCall fail");
                finish();
            }
            //}
        }

        mAudioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mProximity = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
    }

    private ContactHandler contactDB;

    boolean searchContactName(String number) {

        if (TextUtils.isEmpty(number)) {
            Logger.d(TAG, "searchContactName number is empty");
            return false;
        }

        if (contactDB == null) {
            contactDB = new ContactHandler(this);
        }
        contactDB.getReadableDatabase();
        List<Contact> contacts = contactDB.getAllContacts();
        contactDB.getReadableDatabase();

        Logger.d(TAG, "searchContactName - " + number + ", contactDB count :" + contacts.size());

        for (Contact cn : contacts) {

            Logger.d(TAG, "  -> " + cn.getPhoneNumber() + ", " + cn.getName());

            if (cn.getPhoneNumber().equals(number) && !TextUtils.isEmpty(cn.getName())) {
                nameTextView.setText(cn.getName());
                Logger.d(TAG, "searchContactName - found!!");
                return true;
            }
        }
        Logger.d(TAG, "searchContactName - not found !!");

        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Logger.d(TAG, "onResume");
        mSensorManager.registerListener(this, mProximity, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {

        Logger.d(TAG, "onPause");
        super.onPause();

        ActivityManager activityManager = (ActivityManager) getApplicationContext()
                .getSystemService(Context.ACTIVITY_SERVICE);

        activityManager.moveTaskToFront(getTaskId(), 0);

        mSensorManager.unregisterListener(this);


        turnOnScreen();
    }

    @Override
    protected void onStop() {

        super.onStop();
        Logger.d(TAG, "onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Logger.d(TAG, "onDestroy");

        if (mCallTimer != null && mCallTimer.isRunning())
            mCallTimer.cancelTimer();

        playRingbackTone(false);

        mAudioManager.setMode(AudioManager.MODE_NORMAL);

        mProximityWakeLock = null;

    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_HOME)
            return false;

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void started() {
        Log.d(TAG, "started - E");
        updateScreen();
        playRingbackTone(false);
        mAudioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
        mVoiceCallTouchUi.updateAudioButtons();
    }

    @UiThread
    private void updateScreen() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mCallTimer.startTimer();
                Log.d(TAG, "updateScreen - E");
                callStateLabelTextView.setText("Connected");
                mVoiceCallTouchUi.showIncallButtons();
                Log.d(TAG, "updateScreen - X");
            }
        });
    }

    @Override
    public void startFailed(int reason, int code) {
        Log.d(TAG, "startFailed");
        playRingbackTone(false);
        mAudioManager.setMode(AudioManager.MODE_NORMAL);

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (mIsIncomingCall) {
                    mVoiceCallTouchUi.disableIncominButton();
                } else {
                    mVoiceCallTouchUi.disableIncallButton();
                }

                if (reason == CallTerminateReason.PEER_BUSY) {
                    Toast.makeText(getApplication(), "Peer is busy now.", Toast.LENGTH_LONG).show();
                }
            }
        });

        finish();
    }

    @Override
    public void progressing() {
        Log.d(TAG, "progressing");
        playRingbackTone(true);
    }

    private void playRingbackTone(boolean playTone) {

        Uri uri = Settings.System.DEFAULT_RINGTONE_URI;

        Log.d(TAG, "playRingbackTone - : " + playTone);

        if (playTone == true) {
            if (mMediaPlayer == null) {
                mMediaPlayer = new MediaPlayer();
            }
            if (mMediaPlayer != null) {
                try {
                    mMediaPlayer.setDataSource(this, uri);
                    mMediaPlayer.setAudioStreamType(AudioManager.STREAM_VOICE_CALL);
                    mMediaPlayer.setVolume(1.0f, 1.0f);
                    mMediaPlayer.prepare();
                    mMediaPlayer.setLooping(true);
                    mMediaPlayer.start();
                } catch (Exception ex) {
                    Log.d(TAG, "playRingbackTone - err : " + ex.getMessage());
                }
            }
        } else {
            if (mMediaPlayer != null) {
                mMediaPlayer.stop();
                mMediaPlayer.reset();
                mMediaPlayer.release();
                mMediaPlayer = null;
            }
        }
    }

    @Override
    public void terminated(int reason) {
        Log.d(TAG, "terminated");
        playRingbackTone(false);
        mAudioManager.setMode(AudioManager.MODE_NORMAL);

        mVoiceCallTouchUi.EndRinger();
        if (mCallTimer != null && mCallTimer.isRunning())
            mCallTimer.cancelTimer();

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (reason == CallTerminateReason.PEER_BUSY) {
                    Toast.makeText(getApplication(), "Peer is busy now.", Toast.LENGTH_LONG).show();
                }
            }
        });

        finish();
    }

    @Override
    public void serviceNotReady() {

    }

    @Override
    public void serviceReady() {

    }

    @Override
    public void onTickForCallTimeElapsed(long timeElapsed) {
        long duration = mCall.getDurationMillis();

        //Log.d(TAG, "onTickForCallTimeElapsed - " + mPrevDuration + " => " + duration);

        if (mPrevDuration != duration) {
            elapsedTimeTextView.setText(DateUtils.formatElapsedTime(duration / 1000));
            elapsedTimeTextView.invalidate();
            mPrevDuration = duration;
        }
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_PROXIMITY) {
            WindowManager.LayoutParams params = this.getWindow().getAttributes();

            if (event.values[0] >= -SENSOR_SENSITIVITY && event.values[0] <= SENSOR_SENSITIVITY) {
                if (VoiceCallApp.dev) {
                    //near
//                    Toast.makeText(getApplicationContext(), "near", Toast.LENGTH_SHORT).show();
//                    params.flags |= WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
//                    params.screenBrightness = 0;
//                    getWindow().setAttributes(params);
                    turnOnScreen();
                }
            } else {
                //far
                if (VoiceCallApp.dev) {
//                    Toast.makeText(getApplicationContext(), "far", Toast.LENGTH_SHORT).show();
//                    params.flags |= WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
//                    params.screenBrightness = -1f;
//                    getWindow().setAttributes(params);
                    turnOffScreen();
                }
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

}
