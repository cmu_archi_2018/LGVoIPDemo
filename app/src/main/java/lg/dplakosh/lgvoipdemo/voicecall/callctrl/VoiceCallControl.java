package lg.dplakosh.lgvoipdemo.voicecall.callctrl;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import lg.dplakosh.cif.ICallService;
import lg.dplakosh.cif.ICallServiceListener;
import lg.dplakosh.cif.ICallSession;
import lg.dplakosh.lgvoipdemo.utils.Logger;

public class VoiceCallControl implements ServiceConnection, CallServiceListener.CallServiceCallback {

    private static final String TAG = "voip|VoiceCallControl";
    private static String Action_CallService = "com.lge.callsvc.bind_request";

    private static final VoiceCallControl sMe = new VoiceCallControl();
    private static Context mContext;

    private ICallService mCallService = null;
    private ICallServiceListener mCallServiceListener = null;

    private VoiceCall mForegroundCall;

    public static VoiceCallControl getInstance() {
        return sMe;
    }

    private VoiceCallControl() {
    }

    private List<ICommand> mPendingTasks = new ArrayList<>();

    interface ICommand {
        void execute();
    }

    class StartCallCommand implements ICommand {
        private String phoneNumber;
        private IVoiceCallEventListener listener;

        StartCallCommand(String phoneNumber, IVoiceCallEventListener listener) {
            this.phoneNumber = phoneNumber;
            this.listener = listener;
        }

        @Override
        public void execute() {
            ICallSession session = null;

            try {
                session = mCallService.openSession();

                if (session != null) {

                    mForegroundCall = new VoiceCall(session);

                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        Logger.e(TAG, e);
                    }

                    mForegroundCall.setVoiceCallEventListener(listener);
                    mForegroundCall.start(phoneNumber);

                    Log.d(TAG, "StartCallCommand - ok");
                }
                else {
                    Logger.e(TAG, "StartCallCommand - openSession failure");
                    listener.startFailed(CallTerminateReason.BACKEND_NOT_READY, 0);
                }
            } catch (RemoteException e) {
                Logger.e(TAG, e);
                listener.startFailed(CallTerminateReason.INTERNAL_EXCEPTION, 0);
            }
        }
    }

    class AttachCallCommand implements ICommand {

        private int sessionKey;
        private IVoiceCallEventListener listener;

        AttachCallCommand(int sessionKey, IVoiceCallEventListener listener) {
            this.sessionKey = sessionKey;
            this.listener = listener;
        }

        @Override
        public void execute() {
            ICallSession session = null;

            try {
                session = mCallService.attachSession(sessionKey);

                if (session != null) {

                    mForegroundCall = new VoiceCall(session);
                    mForegroundCall.setVoiceCallEventListener(listener);

                    Log.d(TAG, "AttachCallCommand - ok");

                } else {
                    Logger.e(TAG,"AttachCallCommand - attachSession failure");
                }
            } catch (RemoteException e) {
                Logger.e(TAG,e);
                listener.terminated(CallTerminateReason.BACKEND_NOT_READY);
            }
        }
    }

    class SetLoginResultCommand implements ICommand {

        private boolean result;
        private String myNumber;
        private String accessToken;

        SetLoginResultCommand(boolean result, String myNumber, String accessToken) {
            this.result = result;
            this.myNumber = myNumber;
            this.accessToken = accessToken;
        }

        @Override
        public void execute() {
            try {
                mCallService.setLoginResult(result, myNumber, accessToken);
            } catch (RemoteException e) {
                // just logging here
                // handle call failure in back end
                Logger.e(TAG, e);
            }
        }
    }

    class SendTextMessageCommand implements ICommand {

        private String number;
        private String message;

        SendTextMessageCommand(String number, String message) {
            this.number = number;
            this.message = message;
        }

        @Override
        public void execute() {
            try {
                mCallService.sendTextMessage(number, message);
            } catch (RemoteException e) {
                Logger.e(TAG, e);
            }
        }
    }

    private void drainPendingTasks() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                synchronized (mPendingTasks) {
                    try {
                        if (!mPendingTasks.isEmpty()) {
                            for (ICommand task : mPendingTasks) {
                                task.execute();
                            }
                            mPendingTasks.clear();
                        }
                    } catch (Exception e) {
                        Log.d(TAG, "drainPendingTasks - exception - " + e.getMessage());
                    }
                }
            }
        }).start();
    }

    public void initCallService(Context context) {
        mContext = context;
        connectToBackEndService();
    }

    private void connectToBackEndService() {
        Intent intent = new Intent(Action_CallService);
        intent.setPackage("lg.dplakosh.lgvoipdemo.backend");
        boolean result = mContext.bindService(intent, this, Context.BIND_AUTO_CREATE);

        Log.d(TAG, "initCallService - connectToBackEndService : " + result + ", id : " + Thread.currentThread().getId());
    }

    public void terminateService() {
        Log.d(TAG, "terminateService - unbindService");
        mContext.unbindService(this);
        mCallService = null;
        mCallServiceListener = null;
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {

        mCallService = ICallService.Stub.asInterface(service);

        Log.d(TAG, "onServiceConnected - " + Thread.currentThread().getId());

        try {
            mCallServiceListener = new CallServiceListener(this);
            mCallService.setListener(mCallServiceListener);

            drainPendingTasks();
        } catch (Exception e) {
            Logger.e(TAG, e);
        }
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        Log.d(TAG, "onServiceDisconnected");

        synchronized (mPendingTasks) {
            if (!mPendingTasks.isEmpty()) {
                mPendingTasks.clear();
            }
        }

        terminateService();

        // TODO
        // notify ServiceDisconnection of VoiceCall
        // Terminate Call if any
        if (mForegroundCall != null && mForegroundCall.getCallState().isAlive()) {
            mForegroundCall.terminated(101);
        } else {
            Logger.e(TAG, "mForegroundCall is null or !isAlive()");
        }
    }

    public boolean startCall(String phoneNumber, IVoiceCallEventListener listener) {

        if (mForegroundCall != null && mForegroundCall.getCallState().isAlive()) {
            Log.d(TAG, "startCall fail - fg call is used - " + mForegroundCall.getCallState());
            return false;
        }

        StartCallCommand command = new StartCallCommand(phoneNumber, listener);

        if (mCallService == null) {
            synchronized (mPendingTasks) {
                mPendingTasks.add(command);
            }
            connectToBackEndService();
        } else {
            command.execute();
        }

        return true;
    }

    public IVoiceCall getForegroundCall() {
        return mForegroundCall;
    }

    public boolean attachCall(int sessionKey, IVoiceCallEventListener listener) {

        AttachCallCommand command = new AttachCallCommand(sessionKey, listener);

        if (mCallService == null) {
            synchronized (mPendingTasks) {
                mPendingTasks.add(command);
            }
            connectToBackEndService();
        } else {
            command.execute();
        }

        return true;
    }

    @Override
    public void onRegistrationStatus(int status) {
        Logger.e(TAG, "onRegistrationStatus>" + status);
    }

    @Override
    public void onSendMessageResult(int status, int reason) {
        Logger.e(TAG, "onSendMessageResult>" + status);
    }

    public boolean setLoginResult(boolean result, String myNumber, String accessToken) {

        Log.d(TAG, "setLoginResult  - result : " + result + ", myNumber : " + myNumber);

        SetLoginResultCommand command = new SetLoginResultCommand(result, myNumber, accessToken);

        if (mCallService == null) {
            synchronized (mPendingTasks) {
                mPendingTasks.add(command);
            }
            connectToBackEndService();
        } else {
            command.execute();
        }

        return true;
    }

    public boolean sendTextMessage(String number, String message) {

        Log.d(TAG, "sendTextMessage  - number : " + number + ", msg : " + message);

        SendTextMessageCommand command = new SendTextMessageCommand(number, message);

        if (mCallService == null) {
            synchronized (mPendingTasks) {
                mPendingTasks.add(command);
            }
            connectToBackEndService();
        } else {
            command.execute();
        }

        return true;
    }
}
