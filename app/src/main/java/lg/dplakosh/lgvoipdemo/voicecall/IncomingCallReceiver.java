package lg.dplakosh.lgvoipdemo.voicecall;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.widget.Toast;

import lg.dplakosh.lgvoipdemo.ui.LoginActivity;
import lg.dplakosh.lgvoipdemo.ui.MainTabActivity;
import lg.dplakosh.lgvoipdemo.ui.VoipPhoneNotifier;
import lg.dplakosh.lgvoipdemo.utils.Config;

public class IncomingCallReceiver extends BroadcastReceiver {

    private static final String TAG = "voip|IncomingCallRecv";
    private static final String ACTION_VOIP_INCOMING = "com.lge.callsvc.action.VOICE_INVITATION";
    private static final String EXTRA_SESSION_KEY = "com.lge.callsvc.extra.SESSION_KEY";
    private static final String EXTRA_INCOMING_CALL = "com.lge.callsvc.extra.INCOMING_CALL";

    private static final String ACTION_NEW_MESSAGE = "android.intent.category.DEFAULT";

    private static final String ACTION_START_UI = "lg.dplakosh.lgvoipdemo.ACTION_START_UI";
    private static final String ACTION_LOGOUT = "lg.dplakosh.lgvoipdemo.ACTION_NOTIFY_LOGOUT";

    @Override
    public void onReceive(Context context, Intent brintent) {
        Log.d(TAG, "onReceive - " + brintent.getAction());

        if (brintent.getAction().equals(ACTION_VOIP_INCOMING)) {

            int sessionKey = brintent.getIntExtra(EXTRA_SESSION_KEY, 0);

            Intent intent = new Intent(context, VoiceCallActivity.class);
            //intent.setData(brintent.getData());
            intent.setData(Uri.fromParts("tel", PhoneNumberUtils.getNumberFromIntent(brintent, context), null));
            intent.putExtra(EXTRA_SESSION_KEY, sessionKey);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(EXTRA_INCOMING_CALL, true);
            context.startActivity(intent);
        }
        else if (brintent.getAction().equals(ACTION_NEW_MESSAGE)) {
            Toast.makeText(context.getApplicationContext(), "Got Text Message", Toast.LENGTH_SHORT).show();
        }
        else if (brintent.getAction().equals(ACTION_START_UI)) {
            Intent intent = new Intent(context, MainTabActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
        else if (brintent.getAction().equals(ACTION_LOGOUT)) {
            Config.setLoginStatus(Config.STATUS_LOGOUT);
            VoipPhoneNotifier.getInstance().updateVoipPhoneNotification();
            Intent intent = new Intent(context, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent);
        }
    }
}
