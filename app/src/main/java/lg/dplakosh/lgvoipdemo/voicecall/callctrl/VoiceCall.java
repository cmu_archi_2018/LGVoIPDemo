package lg.dplakosh.lgvoipdemo.voicecall.callctrl;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.RemoteException;
import android.os.StrictMode;
import android.os.SystemClock;
import android.util.Log;

import lg.dplakosh.cif.ICallSession;
import lg.dplakosh.lgvoipdemo.VoiceCallApp;
import lg.dplakosh.lgvoipdemo.utils.Logger;

class VoiceCall implements CallSessionListener.CallSessionCallback, IVoiceCall {

    private static final String TAG = "voip|VoiceCall";

    private final ICallSession mSession;
    private VoiceCallState mCallState = VoiceCallState.IDLE;
    private CallSessionListener mListener;
    private long mCallDuration = 0;
    private long mCallStartTime = 0;
    private long mCallEndTime = 0;

    private IVoiceCallEventListener mVoiceCallEventListener;

    public VoiceCall(ICallSession callSession) {

        //TODO GUI 쓰레드에서 돌지 않게 수정이 불가한지 ...?
        int SDK_INT = android.os.Build.VERSION.SDK_INT;
        if (SDK_INT > 8) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
                    .permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        mCallDuration = 0;
        mCallStartTime = 0;
        mCallEndTime = 0;

        mSession = callSession;
        try {

            mListener = new CallSessionListener(this);
            mSession.setListener(mListener);
        } catch (RemoteException e) {
            Logger.e(TAG, e);
        }
    }

    public void start(String phoneNumber) {
        if (mSession != null) {
            try {
                mSession.start(phoneNumber);
                mCallState = VoiceCallState.DIALING;
            } catch (RemoteException e) {
                Logger.e(TAG, e);
            }

            Log.d(TAG, "start ok - " + mCallState);

            // Loopback
            /*try {
                mListener.started();
            } catch (RemoteException e) {
                e.printStackTrace();
            }*/
        }
    }

    @Override
    public void accept() {
        if (mSession != null) {
            try {
                mSession.accept();
            } catch (RemoteException e) {
                Logger.e(TAG, e);
                applicationRestart();


            }
        }
    }

    private void applicationRestart() {
        //WORKAROUND android.os.DeadObjectException 가 뜨면 노답이다. 앱을 재실행하자.
        Intent mStartActivity = new Intent(VoiceCallApp.mContext, VoiceCall.class);
        int mPendingIntentId = 123456;
        PendingIntent mPendingIntent = PendingIntent.getActivity(VoiceCallApp.mContext, mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mgr = (AlarmManager) VoiceCallApp.mContext.getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
        System.exit(0);
    }

    @Override
    public void reject(int reason) {
        if (mSession != null) {
            try {
                mSession.reject(reason);
            } catch (RemoteException e) {
                Logger.e(TAG, e);
                applicationRestart();

            }
            mCallState = VoiceCallState.DISCONNECTING;
        }
    }

    @Override
    public void terminate(int reason) {
        if (mSession != null) {
            try {
                mSession.terminate(reason);
            } catch (RemoteException e) {
                Logger.e(TAG, e);
                applicationRestart();

            }
            mCallState = VoiceCallState.DISCONNECTING;
        }
    }

    @Override
    public VoiceCallState getCallState() {
        return mCallState;
    }

    @Override
    public long getDurationMillis() {
        if (mCallStartTime == 0) {
            Log.d(TAG, "getDurationMillis - mCallStartTime is 0");
            return 0;
        }

        mCallDuration = SystemClock.elapsedRealtime() - mCallStartTime;

        return mCallDuration;
    }

    @Override
    public void setVoiceCallEventListener(IVoiceCallEventListener listener) {
        mVoiceCallEventListener = listener;
    }

    @Override
    public void progressing() {
        Log.i(TAG, "progressing");
        mVoiceCallEventListener.progressing();
    }

    @Override
    public void started() {
        mCallState = VoiceCallState.ACTIVE;

        Log.d(TAG, "started in voicecall - " + mVoiceCallEventListener);
        if (mVoiceCallEventListener != null) {
            mCallStartTime = SystemClock.elapsedRealtime();
            mVoiceCallEventListener.started();
        } else {
            Logger.e(TAG, "started> mVoiceCallEventListenr is null");
        }
    }

    @Override
    public void startFailed(int reason, int code) {
        mCallState = VoiceCallState.DISCONNECTED;
        if (mVoiceCallEventListener != null) {
            mVoiceCallEventListener.startFailed(reason, code);
        } else {
            Logger.e(TAG, "startFailed> mVoiceCallEventListenr is null");
        }
    }

    @Override
    public void terminated(int reason) {
        mCallState = VoiceCallState.DISCONNECTED;

        mCallEndTime = SystemClock.elapsedRealtime();
        if (0 == mCallStartTime) {
            mCallDuration = 0;
        } else {
            mCallDuration = (mCallEndTime - mCallStartTime);
        }
        if (mVoiceCallEventListener != null) {
            mVoiceCallEventListener.terminated(reason);
        } else {
            Logger.e(TAG, "terminated> mVoiceCallEventListener is null");
        }
    }
}
