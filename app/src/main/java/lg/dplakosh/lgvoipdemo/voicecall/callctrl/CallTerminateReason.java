package lg.dplakosh.lgvoipdemo.voicecall.callctrl;

public class CallTerminateReason {

    public static final int NONE = 0;
    public static final int TERMINATE_REASON_USER = 1;
    public static final int BUSY = 2;
    public static final int PEER_INVALID_MEDIA_PARAM  = 10;

    public static final int PEER_BUSY = 100;

    // From 8000... internal reason
    public static final int BACKEND_NOT_READY  = 8000;
    public static final int INTERNAL_EXCEPTION  = 8001;
}
