package lg.dplakosh.lgvoipdemo.voicecall;

import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;

import lg.dplakosh.lgvoipdemo.voicecall.callctrl.IVoiceCall;

class VoiceCallTimer extends Handler {
    private static final long TIME_INTERVAL = 1000;
    private long mLastReportedTime;
    private boolean mTimerRunning = true;
    private final PeriodicTimerCallback mTimerCallback;
    private final OnTickListener mListener;
    private final IVoiceCall mCall;

    public interface OnTickListener {
        void onTickForCallTimeElapsed(long timeElapsed);
    }

    public VoiceCallTimer(IVoiceCall call, OnTickListener listener) {
        mCall = call;
        mListener = listener;
        mTimerCallback = new PeriodicTimerCallback();
    }

    public void startTimer() {
        Log.d("voip|CallTimer", "startTimer");
        mTimerRunning = false;
        periodicUpdateTimer();
    }

    public void cancelTimer() {
        Log.d("voip|CallTimer", "cancelTimer");
        removeCallbacks(mTimerCallback);
        mTimerRunning = true;
    }

    private class PeriodicTimerCallback implements Runnable {
        PeriodicTimerCallback() {
        }

        public void run() {
            mTimerRunning = false;
            periodicUpdateTimer();
        }
    }

    private void periodicUpdateTimer() {
        if (!mTimerRunning) {
            mTimerRunning = true;
            long now = SystemClock.uptimeMillis();
            long nextReport = mLastReportedTime + TIME_INTERVAL;
            while (now >= nextReport) {
                nextReport += TIME_INTERVAL;
            }
            postAtTime(mTimerCallback, nextReport);
            mLastReportedTime = nextReport;
            updateElapsedTime();
        }
    }

    private void updateElapsedTime() {
        if (mListener != null && mCall != null) {
            long mDuration = mCall.getDurationMillis();
            mListener.onTickForCallTimeElapsed(mDuration / 1000);
        }
    }

    public boolean isRunning() {
        return mTimerRunning;
    }
}
