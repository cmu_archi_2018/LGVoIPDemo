package lg.dplakosh.lgvoipdemo.voicecall.callctrl;

import android.os.RemoteException;

import lg.dplakosh.lgvoipdemo.utils.Logger;

class CallServiceListener extends lg.dplakosh.cif.ICallServiceListener.Stub {
    private final static String TAG = CallServiceListener.class.getSimpleName();

    public interface CallServiceCallback {
        void onRegistrationStatus(int status);
        void onSendMessageResult(int status, int reason);
    }

    private final CallServiceCallback mCallback;

    public CallServiceListener(CallServiceCallback callback) {
        mCallback = callback;
    }

    @Override
    public void onRegistrationStatus(int status) throws RemoteException {
        if (mCallback != null) {
            mCallback.onRegistrationStatus(status);
        } else {
            //https://stackoverflow.com/questions/442747/getting-the-name-of-the-currently-executing-method <-- too heavy
            Logger.e(TAG, "onSendMessageResult> mCallback is null");
        }
    }

    @Override
    public void onSendMessageResult(int status, int reason) throws RemoteException {
        if (mCallback != null) {
            mCallback.onSendMessageResult(status, reason);
        } else {
            Logger.e(TAG, "onSendMessageResult> mCallback is null");
        }
    }
}
