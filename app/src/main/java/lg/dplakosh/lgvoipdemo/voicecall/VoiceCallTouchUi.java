package lg.dplakosh.lgvoipdemo.voicecall;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ToggleButton;

import lg.dplakosh.lgvoipdemo.R;
import lg.dplakosh.lgvoipdemo.utils.Logger;
import lg.dplakosh.lgvoipdemo.voicecall.callctrl.CallTerminateReason;
import lg.dplakosh.lgvoipdemo.voicecall.callctrl.IVoiceCall;
import lg.dplakosh.lgvoipdemo.voicecall.callctrl.VoiceCallControl;

public class VoiceCallTouchUi extends LinearLayout implements View.OnClickListener {

    private final static String TAG = VoiceCallTouchUi.class.getSimpleName();

    private View mInCallButtons;
    private Button mEndButton;
    private ToggleButton mMuteButton;
    private ToggleButton mSpeakerButton;
    private ToggleButton mBluetoothButton;

    private View mIncomingCallButton;
    private Button mAnswerButton;
    private Button mRefuseButton;

    private AudioManager audioManager;
    private MediaPlayer ring;
    private Vibrator vibrator;
    private final long[] vibratorPattern = {0, 200, 800};

    public VoiceCallTouchUi(@NonNull Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        audioManager = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);
        vibrator = (Vibrator) getContext().getSystemService(Context.VIBRATOR_SERVICE);

        mInCallButtons = findViewById(R.id.InCallButtons);
        mInCallButtons.setVisibility(View.INVISIBLE);

        mEndButton = mInCallButtons.findViewById(R.id.endButton);
        mEndButton.setOnClickListener(this);

        mMuteButton = mInCallButtons.findViewById(R.id.muteButton);
        mMuteButton.setOnClickListener(this);

        mSpeakerButton = mInCallButtons.findViewById(R.id.speakerButton);
        mSpeakerButton.setOnClickListener(this);

        mBluetoothButton = mInCallButtons.findViewById(R.id.bluetoothButton);
        mBluetoothButton.setOnClickListener(this);

        mIncomingCallButton = findViewById(R.id.IncomingCallButton);
        mIncomingCallButton.setVisibility(View.INVISIBLE);

        mAnswerButton = mIncomingCallButton.findViewById(R.id.answerButton);
        mAnswerButton.setOnClickListener(this);

        mRefuseButton = mIncomingCallButton.findViewById(R.id.refuseButton);
        mRefuseButton.setOnClickListener(this);

        updateAudioButtons();
    }

    public void updateAudioButtons() {
        mMuteButton.setChecked(audioManager.isMicrophoneMute());
        mSpeakerButton.setChecked(audioManager.isSpeakerphoneOn());
        mBluetoothButton.setChecked(audioManager.isBluetoothScoOn());
    }

    public void showIncomingCallButtons() {
        mAnswerButton.setEnabled(true);
        mRefuseButton.setEnabled(true);
        mIncomingCallButton.setVisibility(View.VISIBLE);
        mInCallButtons.setVisibility(View.GONE);
    }

    public void showIncallButtons() {
        mEndButton.setEnabled(true);
        mIncomingCallButton.setVisibility(View.GONE);
        mInCallButtons.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.answerButton:
                answerCall();
                break;

            case R.id.refuseButton:
                refuseCall();
                break;

            case R.id.endButton:
                endCall();
                break;

            case R.id.muteButton:
                setMute();
                break;

            case R.id.speakerButton:
                setSpeaker();
                break;

            case R.id.bluetoothButton:
                setBluetooth();
                break;

            default:
                Logger.e(TAG, "wrong R.id.x");
                break;
        }
    }

    private void endCall() {
        IVoiceCall call = VoiceCallControl.getInstance().getForegroundCall();
        if (call != null)
            call.terminate(CallTerminateReason.TERMINATE_REASON_USER);
    }

    private void setMute() {
        if (mMuteButton.isChecked()) {
            audioManager.setMicrophoneMute(true);
        } else {
            audioManager.setMicrophoneMute(false);
        }
    }

    private void setSpeaker() {
        if (mSpeakerButton.isChecked()) {
            mBluetoothButton.setEnabled(false);
            audioManager.setSpeakerphoneOn(true);
        } else {
            mBluetoothButton.setEnabled(true);
            audioManager.setSpeakerphoneOn(false);
        }
    }

    private void setBluetooth() {
        if (mBluetoothButton.isChecked()) {
            mSpeakerButton.setEnabled(false);
            audioManager.setBluetoothScoOn(true);
            audioManager.startBluetoothSco();
        } else {
            mSpeakerButton.setEnabled(true);
            audioManager.setBluetoothScoOn(false);
            audioManager.stopBluetoothSco();
        }
    }

    private void answerCall() {
        EndRinger();
        IVoiceCall call = VoiceCallControl.getInstance().getForegroundCall();
        if (call != null)
            call.accept();
    }

    private void refuseCall() {
        EndRinger();
        IVoiceCall call = VoiceCallControl.getInstance().getForegroundCall();
        if (call != null) {
            call.reject(CallTerminateReason.TERMINATE_REASON_USER);
        }
    }

    public void StartRinger() {
        switch( audioManager.getRingerMode() ){
            case AudioManager.RINGER_MODE_NORMAL:
                if (ring == null) {
                    ring = MediaPlayer.create(getContext(), R.raw.ring);
                    ring.setLooping(true);
                    ring.start();
                }
                break;
            case AudioManager.RINGER_MODE_SILENT:
                break;
            case AudioManager.RINGER_MODE_VIBRATE:
                vibrator.vibrate(vibratorPattern, 0);
                break;
        }
    }

    public void EndRinger() {
        if (ring != null) {
            ring.stop();
            ring.release();
            ring = null;
        }
        vibrator.cancel();
    }

    public void disableIncominButton() {
        mAnswerButton.setEnabled(false);
        mRefuseButton.setEnabled(false);
    }

    public void disableIncallButton() {
        mEndButton.setEnabled(false);
    }
}
