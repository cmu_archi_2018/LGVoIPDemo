package lg.dplakosh.lgvoipdemo.voicecall.callctrl;

public interface IVoiceCall {
    void accept();
    void reject(int reason);
    void terminate(int reason);
    VoiceCallState getCallState();
    void setVoiceCallEventListener(IVoiceCallEventListener listener);
    long getDurationMillis() ;
}
