package lg.dplakosh.lgvoipdemo.voicecall.callctrl;

public enum  VoiceCallState {

    IDLE, ACTIVE, DIALING, ALERTING, INCOMING, DISCONNECTED, DISCONNECTING;

    public boolean isAlive() {
        return !(this == IDLE || this == DISCONNECTED || this == DISCONNECTING);
    }

    public boolean isRinging() {
        return (this == INCOMING);
    }

    public boolean isDialing() {
        return (this == DIALING || this == ALERTING);
    }

    public boolean isIdle() {
        return (this == IDLE);
    }
}
