package lg.dplakosh.lgvoipdemo.voicecall.callctrl;

import android.os.RemoteException;
import android.util.Log;

import lg.dplakosh.cif.ICallSessionListener;
import lg.dplakosh.lgvoipdemo.utils.Logger;

class CallSessionListener extends ICallSessionListener.Stub {

    private final static String TAG = CallSessionCallback.class.getSimpleName();

    private CallSessionCallback mCallback = null;

    public interface CallSessionCallback {
        void progressing();

        void started();

        void startFailed(int reason, int code);

        void terminated(int reason);
    }

    public CallSessionListener(CallSessionCallback callback) {
        mCallback = callback;
    }

    @Override
    public void progressing() throws RemoteException {
        if (mCallback != null) {
            mCallback.progressing();
        } else {
            Logger.e(TAG, "progressing> mCallback is null");
        }
    }

    @Override
    public void started() throws RemoteException {
        Log.d("yong", "started - mCallback : " + mCallback);
        if (mCallback != null) {
            mCallback.started();
        } else {
            Logger.e(TAG, "started> mCallback is null");
        }
    }

    @Override
    public void startFailed(int reason, int code) throws RemoteException {
        if (mCallback != null) {
            mCallback.startFailed(reason, code);
        }
    }

    @Override
    public void terminated(int reason) throws RemoteException {
        if (mCallback != null) {
            mCallback.terminated(reason);
        }
    }
}
