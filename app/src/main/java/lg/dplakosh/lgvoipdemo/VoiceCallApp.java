package lg.dplakosh.lgvoipdemo;

import android.app.Application;
import android.content.Context;

import lg.dplakosh.lgvoipdemo.ui.VoipPhoneNotifier;
import lg.dplakosh.lgvoipdemo.utils.Config;
import lg.dplakosh.lgvoipdemo.voicecall.callctrl.VoiceCallControl;

public class VoiceCallApp extends Application {

    public static boolean dev = true;
    public static boolean local = true;


    public static int voiceDetectionPeriodMilliSec = 1000000;
    public static Context mContext;

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
        Config.init(mContext);
        VoiceCallControl.getInstance().initCallService(getApplicationContext());
        VoipPhoneNotifier.init(getApplicationContext());
    }

    @Override
    public void onTerminate() {
        super.onTerminate();

        //see https://developer.android.com/reference/android/app/Application
        VoiceCallControl.getInstance().terminateService();

        VoipPhoneNotifier.getInstance().destroy();
    }
}
