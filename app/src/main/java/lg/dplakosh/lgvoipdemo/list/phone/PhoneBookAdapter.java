package lg.dplakosh.lgvoipdemo.list.phone;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;

import java.util.List;

import android.content.Context;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import lg.dplakosh.lgvoipdemo.R;
import lg.dplakosh.lgvoipdemo.db.contact.Contact;
import lg.dplakosh.lgvoipdemo.db.contact.ContactHandler;
import lg.dplakosh.lgvoipdemo.ui.ContactFragment;
import lg.dplakosh.lgvoipdemo.ui.EditSmsActivity;
import lg.dplakosh.lgvoipdemo.ui.MainTabActivity;
import lg.dplakosh.lgvoipdemo.ui.ModifyContactActivity;
import lg.dplakosh.lgvoipdemo.voicecall.VoiceCallActivity;

import android.support.v7.app.AlertDialog;

public class PhoneBookAdapter extends BaseAdapter {
    private Context mContext;
    private List<PhoneBook> listPhoneBook;

    public PhoneBookAdapter(Context context, List<PhoneBook> list) {
        mContext = context;
        listPhoneBook = list;
    }

    @Override
    public int getCount() {
        return listPhoneBook.size();
    }

    @Override
    public Object getItem(int pos) {
        return listPhoneBook.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        // get selected entry
        final PhoneBook entry = listPhoneBook.get(pos);
        final ContactHandler db = new ContactHandler(mContext);

        // inflating list view layout if null
        if(convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.list_item, null);
        }

        Button button_send = (Button)convertView.findViewById(R.id.button_send);
        button_send.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                Log.d("send","send");
                String str_name = entry.getName();
                String str_phone = entry.getPhone();
                String str_email = entry.getEmail();
                Intent myIntent = new Intent(mContext, EditSmsActivity.class);
                myIntent.putExtra("Name",str_name);
                myIntent.putExtra("Phone",str_phone);
                myIntent.putExtra("Email",str_email);
                mContext.startActivity(myIntent);
            }
        });

        Button button_call = (Button)convertView.findViewById(R.id.button_call);
        button_call.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                Log.d("call","call");
                String str_name = entry.getName();
                String str_phone = entry.getPhone();
                String str_email = entry.getEmail();
                Intent intent = new Intent(mContext, VoiceCallActivity.class);
                intent.setData(Uri.fromParts("tel", str_phone, null));
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        });

        Button button_modify = (Button)convertView.findViewById(R.id.button_modify);
        button_modify.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                Log.d("modify","modify");
                String str_name = entry.getName();
                String str_phone = entry.getPhone();
                String str_email = entry.getEmail();
                Intent myIntent = new Intent(mContext, ModifyContactActivity.class);
                myIntent.putExtra("Name",str_name);
                myIntent.putExtra("Phone",str_phone);
                myIntent.putExtra("Email",str_email);
                mContext.startActivity(myIntent);
            }
        });


        Button button_delete = (Button)convertView.findViewById(R.id.button_delete);
        button_delete.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {

                ContactFragment.allow=true;
                AlertDialog.Builder alert = new AlertDialog.Builder(mContext);
                alert.setTitle("Alert!!");
                alert.setMessage("Are you sure to delete contact?");
                alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //do your work here
                        String str_name = entry.getName();
                        String str_phone = entry.getPhone();
                        String str_email = entry.getEmail();
                        Contact contact = new Contact(str_name,str_phone,str_email);
                        db.deleteContactUsingEmail(contact);
                        Log.d("delete","delete");
                        dialog.dismiss();
                        Intent myIntent = new Intent(mContext, MainTabActivity.class);
                        mContext.startActivity(myIntent);

                    }
                });
                alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });

                alert.show();


            }
        });

        // set name
        TextView tvName = (TextView)convertView.findViewById(R.id.tvName);
        tvName.setText(entry.getName());

        // set phone
        TextView tvPhone = (TextView)convertView.findViewById(R.id.tvPhone);
        tvPhone.setText(entry.getPhone());

        // set email
        TextView tvEmail = (TextView)convertView.findViewById(R.id.tvEmail);
        tvEmail.setText(entry.getEmail());

        return convertView;
    }

}
