package lg.dplakosh.lgvoipdemo.list.smslist;

import java.util.List;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import lg.dplakosh.lgvoipdemo.R;
import lg.dplakosh.lgvoipdemo.db.contact.ContactHandler;
import lg.dplakosh.lgvoipdemo.db.sms.SMS;
import lg.dplakosh.lgvoipdemo.db.sms.SMSSQLiteDatabaseHelper;
import lg.dplakosh.lgvoipdemo.ui.EditSmsActivity;
import lg.dplakosh.lgvoipdemo.ui.MainTabActivity;
import lg.dplakosh.lgvoipdemo.ui.ReadSmsActivity;
import lg.dplakosh.lgvoipdemo.ui.SMSFragment;

public class SMSBookAdapter extends BaseAdapter {
    private final Context mContext;
    private final List<SMSBook> listSMSBook;

    public SMSBookAdapter(Context context, List<SMSBook> list) {
        mContext = context;
        listSMSBook = list;
    }

    @Override
    public int getCount() {
        return listSMSBook.size();
    }

    @Override
    public Object getItem(int pos) {
        return listSMSBook.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        // get selected entry
        final SMSBook entry = listSMSBook.get(pos);
        final SMSSQLiteDatabaseHelper message_db = new SMSSQLiteDatabaseHelper(mContext);


        // inflating list view layout if null
        if(convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.list_sms_item, null);
        }

        Button button_read = (Button)convertView.findViewById(R.id.button_read);
        button_read.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                Log.d("read","read");
                String str_date = entry.getDate();
                String str_phone = entry.getPhonenumber();
                String str_message = entry.getSMSMessage();
                String str_readornot = entry.getReadorno();
                Intent myIntent = new Intent(mContext, ReadSmsActivity.class);
                myIntent.putExtra("Date",str_date);
                myIntent.putExtra("Phone",str_phone);
                myIntent.putExtra("Message",str_message);
                myIntent.putExtra("Readornot",str_readornot);
                mContext.startActivity(myIntent);
            }
        });

        Button button_modify = (Button)convertView.findViewById(R.id.button_delete);
        button_modify.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v)
            {
                SMSFragment.allow=true;
                AlertDialog.Builder alert = new AlertDialog.Builder(mContext);
                alert.setTitle("Alert!!");
                alert.setMessage("Are you sure to delete sms?");
                alert.setPositiveButton("YES", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //do your work here
                        String str_date = entry.getDate();
                        String str_phone = entry.getPhonenumber();
                        String str_message = entry.getSMSMessage();
                        String str_readornot = entry.getReadorno();
                        SMS sms = new SMS(str_date,str_phone,str_message,str_readornot);
                        message_db.deleteSMSUsingInfo(sms);
                        Log.d("delete","delete");
                        dialog.dismiss();
                        Intent myIntent = new Intent(mContext, MainTabActivity.class);
                        mContext.startActivity(myIntent);

                    }
                });
                alert.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });

                alert.show();
            }
        });

        // set name
        TextView tvDate = convertView.findViewById(R.id.tvDate);
        tvDate.setText(entry.getDate());

        // set phone
        TextView tvPhonenumber = convertView.findViewById(R.id.tvPhonenumber);
        tvPhonenumber.setText(entry.getPhonenumber());

        // set email
        TextView tvMessage = convertView.findViewById(R.id.tvMessage);
        tvMessage.setText(entry.getSMSMessage());

        // set email
        TextView tvReadorno = convertView.findViewById(R.id.tvReadorno);
        tvReadorno.setText(entry.getReadorno());

        return convertView;
    }

}
