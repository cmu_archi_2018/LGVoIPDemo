package lg.dplakosh.lgvoipdemo.list.conference;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import lg.dplakosh.lgvoipdemo.R;

public class ConferenceBookAdapter extends BaseAdapter {
    private final Context mContext;
    private final List<ConferenceBook> listConferenceBook;

    public ConferenceBookAdapter(Context context, List<ConferenceBook> list) {
        mContext = context;
        listConferenceBook = list;
    }

    @Override
    public int getCount() {
        return listConferenceBook.size();
    }

    @Override
    public Object getItem(int pos) {
        return listConferenceBook.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @Override
    public View getView(int pos, View convertView, ViewGroup parent) {
        // get selected entry
        ConferenceBook entry = listConferenceBook.get(pos);

        // inflating list view layout if null
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.list_conference_item, null);
        }

        ((TextView) convertView.findViewById(R.id.textView_conference_title)).setText(entry.getmStrTitle());
        ((TextView) convertView.findViewById(R.id.textView_conference_startDate)).setText(entry.getmStrStartDate());
        ((TextView) convertView.findViewById(R.id.textView_conference_endDate)).setText(entry.getmStrEndDate());
        ((TextView) convertView.findViewById(R.id.textView_conference_hostNo)).setText(entry.getmStrHostPhoneNo());
        ((TextView) convertView.findViewById(R.id.textView_conference_uniqueId)).setText(entry.getmStrUniqueId());
        ((TextView) convertView.findViewById(R.id.textView_conference_guestPhoneNos)).setText(entry.getmStrGuestPhoneNos());
        ((TextView) convertView.findViewById(R.id.textView_conference_conferencePhoneNo)).setText(entry.getmStrConferencePhoneNo());

        return convertView;
    }

}
