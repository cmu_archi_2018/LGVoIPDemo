package lg.dplakosh.lgvoipdemo.list.conference;

public class ConferenceBook {

    public String getmStrTitle() {
        return mStrTitle;
    }

    public void setmStrTitle(String mStrTitle) {
        this.mStrTitle = mStrTitle;
    }

    public String getmStrStartDate() {
        return mStrStartDate;
    }

    public void setmStrStartDate(String mStrStartDate) {
        this.mStrStartDate = mStrStartDate;
    }

    public String getmStrEndDate() {
        return mStrEndDate;
    }

    public void setmStrEndDate(String mStrEndDate) {
        this.mStrEndDate = mStrEndDate;
    }

    public String getmStrHostPhoneNo() {
        return mStrHostPhoneNo;
    }

    public void setmStrHostPhoneNo(String mStrHostPhoneNo) {
        this.mStrHostPhoneNo = mStrHostPhoneNo;
    }

    private String mStrTitle;
    private String mStrStartDate;
    private String mStrEndDate;
    private String mStrHostPhoneNo;

    public String getmStrConferencePhoneNo() {
        return mStrConferencePhoneNo;
    }

    public void setmStrConferencePhoneNo(String mStrConferencePhoneNo) {
        this.mStrConferencePhoneNo = mStrConferencePhoneNo;
    }

    private String mStrConferencePhoneNo;

    public String getmStrGuestPhoneNos() {
        return mStrGuestPhoneNos;
    }

    public void setmStrGuestPhoneNos(String mStrGuestPhoneNos) {
        this.mStrGuestPhoneNos = mStrGuestPhoneNos;
    }

    private String mStrGuestPhoneNos;

    public String getmStrUniqueId() {
        return mStrUniqueId;
    }

    public void setmStrUniqueId(String mStrUniqueId) {
        this.mStrUniqueId = mStrUniqueId;
    }

    private String mStrUniqueId;

    public ConferenceBook(String strTitle, String strStartDate, String strEndDate, String strHostPhoneNo, String strGuestPhoneNos, String strUniqueId, String strConferencePhoneNo) {
        mStrTitle = strTitle;
        mStrStartDate = strStartDate;
        mStrEndDate = strEndDate;
        mStrHostPhoneNo = strHostPhoneNo;
        mStrUniqueId = strUniqueId;
        mStrGuestPhoneNos = strGuestPhoneNos;
        mStrConferencePhoneNo = strConferencePhoneNo;
    }

}