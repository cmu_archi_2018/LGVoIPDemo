package lg.dplakosh.lgvoipdemo.list.smslist;

import android.graphics.Bitmap;

public class SMSBook {
    private Bitmap uAvatar;
    private String uDate;
    private String uNumber;
    private String uMessage;
    private String uReadorno;

    public SMSBook(Bitmap avatar, String date, String phonenumber, String message, String Readorno) {
        uAvatar = avatar;
        uDate = date;
        uNumber = phonenumber;
        uMessage = message;
        uReadorno = Readorno;
    }

    public void setAvatar(Bitmap avatar) {
        uAvatar = avatar;
    }
    public Bitmap getAvatar() {
        return uAvatar;
    }

    public void setDate(String date) {
        uDate = date;
    }
    public String getDate() {
        return uDate;
    }

    public void setPhonenumber(String phonenumber) {
        uNumber = phonenumber;
    }
    public String getPhonenumber() {
        return uNumber;
    }

    public void setSMSMessage(String message) {
        uMessage = message;
    }
    public String getSMSMessage() {
        return uMessage;
    }

    public void setReadorno(String Readorno) {
        uReadorno = Readorno;
    }
    public String getReadorno() {
        return uReadorno;
    }
}