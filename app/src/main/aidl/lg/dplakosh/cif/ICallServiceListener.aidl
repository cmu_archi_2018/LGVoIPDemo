package lg.dplakosh.cif;

interface ICallServiceListener
{
    void onRegistrationStatus(int status);
	void onSendMessageResult(int status, int reason);
}

