package lg.dplakosh.cif;

import lg.dplakosh.cif.ICallSessionListener;

interface ICallSession
{
	void start(String phoneNumber);
	void accept();
	void reject(int reason);
	void terminate(int reason);

	void setListener(ICallSessionListener listener);

	int getProperty(int item);
}

