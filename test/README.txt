폰 볼더에 있는 파일들은 
https://canvas.cmu.edu/courses/4742/files/folder/Studio%20Projects 에서 가져온 자료입니다.


[컴퓨터]----[마이크로5핀USB]---[라즈베리파이]


1. 라즈베리파이에 전원을 공급한다. ( USB micro 5pin cable 연결)
2. LG-VoIP-Test-Team-4-AP-1
   LG-VoIP-Test-Team-4-AP-2
   두개의 ssid가 검색된다.
   
3. LG-VoIP-Test-Team-4-AP-2 에 접속
4. putty와 같은 터미널 프로그램에서 10.0.2.1:22 로 접속한다.
5. id: pi
   password: LgVoipProject1!
