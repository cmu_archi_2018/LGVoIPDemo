
# 7-Hours VoIP Application for Android

Welcome to the 7-Hours VoIP Application

## Summary

7-Hours Voip Application for android consists of two independent applications, front-end and back-end, which communicate with each other via a binder to conserve battery power.

SIP is made up of our own independent protocol. See https://goo.gl/PovP7z


## Dependency

* When creating a conference room, the following library is used to select the time.

```
implementation 'com.github.drawers:SpinnerDatePicker:1.0.6'
```

* This program uses RTP of the Android framework for media transmission. https://developer.android.com/reference/android/net/rtp/package-summary
See below for supported codecs. https://developer.android.com/reference/android/net/rtp/AudioCodec

## Project Compilation

- pre-requisites

1.	JDK v1.8 or higher  
2.	SDK v2.2.1 or higher  
3.	NDK  r10d or higher (Note: remember to export NDK's path) 
4.	Android Studio (with SDK) 1.2.1 or higher
5.  Gradle 3.1.3

- Summary of set up:

1.	Get the source code.[Git] (https://gitlab.com/cmu_archi_2018/LGVoIPDemo.git)  
2.	Open it in Android Studio, and modify the path of SDK and NDK in the file "local.properties"
3.	Compile and run.  

- Testing Hints:

1. For high testability, if you want to use a peer-to-peer mode without a server, you can set the ip and port of the other mobile phone to the ip of the Comm server in the settings of the backend. (However, there is a restriction that the billing function and the message function can not be used.)
2. Recommend to use a real Android device instead of Android virtual device. Some times AVD has no sound system supports.
3. Project has set minSdk for Android 6.0 (API 23) due to lack of smartphone, but we recommend using API 24 or higher. (!)
4. For high modifiability, the program supports changing the decorable at runtime. This can also be changed in the settings menu of the backend.


## 7-Hours VoIP Project Reference ###

1. Android Applicaton (https://gitlab.com/cmu_archi_2018/LGVoIPDemo.git)

2. Communication Server (https://gitlab.com/cmu_archi_2018/voipserver_backend.git)

3. Web Service Server (https://gitlab.com/cmu_archi_2018/webservice.git)

## Licence

Project uses [Apache 2.0 License](LICENSE)
